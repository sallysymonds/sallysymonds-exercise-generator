<?php

/**
 * Migrator abstract class
 *
 * @author OptimizePress <info@optimizepress.com>
 */
abstract class Op_Migrator
{
	/**
	 * Number of replacements done
	 * @var integer
	 */
	public $replacement_number = 0;

	/**
	 * Class constructor with fluent interface
	 * @param string $old_domain
	 * @param string $new_domain
	 * @return Op_Migrator returns itself, fluent interface
	 */
	public function __construct($old_domain, $new_domain)
	{
		$this->old_domain = $old_domain;
		$this->new_domain = $new_domain;

		return $this;
	}

	/**
	 * Replaces self::$old_domain with self::$new_domain
	 * @return void
	 */
	abstract public function switch_domains();

	/**
	 * Unserializes and replaces data and does so in recursion if it finds an array,
	 * depending on second parameter ($serialized) it returns serialized data
	 * @param  mixed $data
	 * @param  boolean $serialized
	 * @return string
	 */
	protected function recursive_unserialize_replace($data, $serialized = false)
	{
		try {
			$unserialized = @unserialize($data);
			if (is_string($data) && $unserialized !== false) {
				$data = $this->recursive_unserialize_replace($unserialized, true);
			} else if (is_array($data)) {
				$temp = array();
				foreach ($data as $key => $value) {
					$temp[$key] = $this->recursive_unserialize_replace($value, false);
				}
				$data = $temp;
				unset($temp);
			} else {
				$count = 0;
				$data = str_replace($this->old_domain, $this->new_domain, $data, $count);
				$this->replacement_number += $count;
			}
		} catch (Exception $e) {
			error_log($e->getMesasge());
		}

		if ($serialized) {
			return serialize($data);
		} else {
			return $data;
		}
	}
}