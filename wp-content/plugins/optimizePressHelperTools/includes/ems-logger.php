<?php

require_once OP_MOD . 'email/ProviderInterface.php';

class OptimizePress_Modules_Email_Logger_File implements OptimizePress_Modules_Email_LoggerInterface
{
    protected $file;

    public function __construct($file)
    {
        $this->file = $file;
    }

    public function log($level, $message)
    {
        switch ($level) {
            case 'emergency':
            case 'alert':
            case 'critical':
            case 'error':
            case 'warning':
            case 'notice':
            case 'info':
            case 'debug':
            default:
                $this->info($message);
                break;
        }
    }

    public function emergency($message)
    {
        $this->info($message);
    }

    public function alert($message)
    {
        $this->info($message);
    }

    public function critical($message)
    {
        $this->info($message);
    }

    public function error($message)
    {
        $this->info($message);
    }

    public function warning($message)
    {
        $this->info($message);
    }

    public function notice($message)
    {
        $this->info($message);
    }

    public function debug($message)
    {
        $this->info($message);
    }

    public function info($message)
    {
        $this->write($message);
    }

    protected function write($message)
    {
        if (is_array($message)) {
            $message = print_r($message, true);
        }

        $handle = fopen($this->file, 'a+');

        fwrite($handle, date('Y-m-d H:i:s') . ' -=- ' . $message);
        fclose($handle);
    }
}