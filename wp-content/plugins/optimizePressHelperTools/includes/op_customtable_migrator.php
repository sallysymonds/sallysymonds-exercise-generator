<?php

require_once 'op_migrator.php';

/**
 * Migrates OP custom tables
 *
 * @author OptimizePress <info@optimizepress.com>
 */
class Op_CustomTable_Migrator extends Op_Migrator
{
	/**
	 * @see Op_Migrator::switch_domains()
	 */
	public function switch_domains()
	{
		global $wpdb;

		// optimizepress_post_layouts
		$rows = $wpdb->get_results('SELECT id, layout FROM ' . $wpdb->prefix . 'optimizepress_post_layouts');
		if (null !== $rows) {
			foreach ($rows as $row) {

				$old_replacement_number = $this->replacement_number;

				$layout = base64_encode($this->recursive_unserialize_replace(base64_decode($row->layout)));

				// We are only doing an update if there are some actual updates
				if ($this->replacement_number > $old_replacement_number) {
					$wpdb->update(
						$wpdb->prefix . 'optimizepress_post_layouts',
						array('layout' => $layout),
						array('id' => $row->id),
						array('%s'),
						array('%d')
					);
				}
			}
		}

		// optimizepress_predefined_layouts
		$row = $wpdb->get_results('SELECT id, layouts, settings FROM ' . $wpdb->prefix . 'optimizepress_predefined_layouts');
		if (null !== $rows) {
			foreach ($rows as $row) {

				$old_replacement_number = $this->replacement_number;

				$layouts = base64_encode($this->recursive_unserialize_replace(base64_decode($row->layouts)));
				$settings = base64_encode($this->recursive_unserialize_replace(base64_decode($row->settings)));

				// We are only doing an update if there are some actual updates
				if ($this->replacement_number > $old_replacement_number) {
					$wpdb->update(
						$wpdb->prefix . 'optimizepress_predefined_layouts',
						array(
							'layouts' => $layouts,
							'settings' => $settings,
						),
						array('id' => $row->id),
						array('%s', '%s'),
						array('%d')
					);
				}
			}
		}

		// optimizepress_presets
		$row = $wpdb->get_results('SELECT id, layouts, settings FROM ' . $wpdb->prefix . 'optimizepress_presets');
		if (null !== $rows) {
			foreach ($rows as $row) {

				$old_replacement_number = $this->replacement_number;

				$layouts = base64_encode($this->recursive_unserialize_replace(base64_decode($row->layouts)));
				$settings = base64_encode($this->recursive_unserialize_replace(base64_decode($row->settings)));

				// We are only doing an update if there are some actual updates
				if ($this->replacement_number > $old_replacement_number) {
					$wpdb->update(
						$wpdb->prefix . 'optimizepress_presets',
						array(
							'layouts' => $layouts,
							'settings' => $settings,
						),
						array('id' => $row->id),
						array('%s', '%s'),
						array('%d')
					);
				}
			}
		}
	}

	/**
	 * @see Op_Migrator::recursive_unserialize_replace()
	 */
	protected function recursive_unserialize_replace($data, $serialized = false)
	{
		try {
			$unserialized = @unserialize($data);
			if (is_string($data) && $unserialized !== false) {
				$data = $this->recursive_unserialize_replace($unserialized, true);
			} else if (is_array($data)) {
				$temp = array();
				foreach ($data as $key => $value) {
					if ($key === 'row_data_style') {
						$value = base64_decode($value);
						$temp[$key] = base64_encode($this->recursive_unserialize_replace($value, false));
					} else {
						$temp[$key] = $this->recursive_unserialize_replace($value, false);
					}

				}
				$data = $temp;
				unset($temp);
			} else {
				$count = 0;
				$data = str_replace($this->old_domain, $this->new_domain, $data, $count);
				$this->replacement_number += $count;
			}
		} catch (Exception $e) {
			error_log($e->getMesasge());
		}

		if ($serialized) {
			return serialize($data);
		} else {
			return $data;
		}
	}
}
