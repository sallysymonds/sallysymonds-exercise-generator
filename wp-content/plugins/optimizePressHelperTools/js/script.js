jQuery(document).ready(function($) {
    var $opEmsLogger = $('#op_ems_logger');
    $opEmsLogger.scrollTop($opEmsLogger[0].scrollHeight);
    var offset = parseInt($('.op-ems-logger-refresh').attr('data-offset'));

    $('.op-ems-logger-refresh').click(function() {
        $.ajax({
            url: ajaxurl,
            data: {
                action: 'op-ems-log',
                offset: offset
            }
        }).done(function(data) {
            if (data.data.offset > 0) {
                offset += parseInt(data.data.offset);
                $opEmsLogger.html($opEmsLogger.html() + data.data.log);
                $opEmsLogger.scrollTop($opEmsLogger[0].scrollHeight);
            }
        });

        return false;
    });

    $('.op-ems-logger-clear').click(function() {
        $.ajax({
            url: ajaxurl,
            data: {
                action: 'op-ems-clear'
            }
        }).done(function() {
            offset = 0;
            $opEmsLogger.html('');
        });

        return false;
    });
});