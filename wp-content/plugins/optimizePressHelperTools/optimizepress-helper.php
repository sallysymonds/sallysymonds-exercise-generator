<?php
/**
 * Plugin Name: OptimizePress Helper
 * Plugin URI:  http://www.optimizepress.com
 * Description: Assorted OptimizePress helper tools (EMS logger, domain migration and others)
 * Version:     1.2.1
 * Author:      OptimizePress
 * Author URI:  http://www.optimizepress.com
 * Text Domain: optimizepress-helper
 * Domain Path: /lang
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class Op_Helper {

    /**
     * Plugin version, used for cache-busting of style and script file references.
     * @since   1.0.0
     * @const   string
     */
    const VERSION = '1.2.1';

    /**
     * Unique identifier for your plugin.
     *
     * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
     * match the Text Domain file header in the main plugin file.
     * @since    1.0.0
     * @var      string
     */
    protected $plugin_slug = 'optimizepress-helper';

    /**
     * Instance of this class.
     * @since    1.0.0
     * @var      object
     */
    protected static $instance = null;

    /**
     * Slug of the plugin screen.
     * @since    1.0.0
     * @var      string
     */
    protected $plugin_screen_hook_suffix = null;

    /**
     * Initialize the plugin by setting localization, filters, and administration functions.
     * @since     1.0.0
     */
    private function __construct()
    {
        /*
         * Actions
         */
        add_action('init', array($this, 'load_plugin_textdomain'));
        add_action('wp_ajax_op-ems-log', array($this, 'load_log_content'));
        add_action('wp_ajax_op-ems-clear', array($this, 'clear_log_content'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_js'));
        add_action('admin_menu', array($this, 'add_plugin_admin_menu'));

        /*
         * Filters
         */
        add_filter('op_ems_logger_class', array($this, 'load_class'));
        add_filter('op_ems_use_cache', array($this, 'disable_cache'));
    }

    /**
     * Return an instance of this class.
     * @since     1.0.0
     * @return    object    A single instance of this class.
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;

            if (!file_exists(self::get_log_file_path())) {
                self::activate();
            }
        }

        return self::$instance;
    }

    /**
     * Load the plugin text domain for translation.
     * @since    1.0.0
     */
    public function load_plugin_textdomain()
    {
        $domain = $this->plugin_slug;
        $locale = apply_filters('plugin_locale', get_locale(), $domain);

        load_textdomain($domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo');
        load_plugin_textdomain($domain, FALSE, basename(dirname(__FILE__)) . '/lang/');
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu()
    {
        $this->plugin_screen_hook_suffix = add_management_page(
            __('OP Helper Tools', $this->plugin_slug),
            __('OP Helper Tools', $this->plugin_slug),
            'update_core',
            $this->plugin_slug,
            array($this, 'display_plugin_admin_page')
        );

    }

    /**
     * Includes file and instantiates the file logger class
     * @since 1.2.1
     * @param  OptimizePress_Modules_Email_LoggerInterface $class
     * @return OptimizePress_Modules_Email_Logger_File
     */
    public function load_class($class)
    {
        require_once OP_MOD . 'email/LoggerInterface.php';
        require_once plugin_dir_path(__FILE__) . 'includes/ems-logger.php';

        return new OptimizePress_Modules_Email_Logger_File(self::get_log_file_path());
    }

    /**
     * Disables cache decorator
     * @since  1.2.1
     * @param  bool $cache
     * @return bool
     */
    public function disable_cache($cache)
    {
        return false;
    }

    /**
     * Creates log file on plugin activation
     * @since  1.2.1
     * @return void
     */
    public static function activate()
    {
        $handle = fopen(self::get_log_file_path(), "w");
        fclose($handle);
    }

    /**
     * Deletes log file on plugin deactivation
     * @since  1.2.1
     * @return void
     */
    public static function deactivate()
    {
        unlink(self::get_log_file_path());
    }

    /**
     * Returns log file path
     * @since  1.2.1
     * @return string
     */
    protected static function get_log_file_path()
    {
        $dir    = wp_upload_dir();
        $file   = $dir['basedir'] . '/op-ems.log';

        return $file;
    }

    /**
     * Enqueues JS file needed for AJAX log calls
     * @since  1.2.1
     * @param  string $hookSuffix
     * @return void
     */
    public function enqueue_js($hookSuffix)
    {
        if ($this->plugin_screen_hook_suffix !== $hookSuffix) {
            return;
        }

        wp_enqueue_script(OP_SN . '-ems_logger', plugin_dir_url(__FILE__) . 'js/script.js', array('jquery'), Op_Helper::VERSION);
    }

    /**
     * Responds to AJAX call and echoes log content from $_GET['offset'] to end of file
     * @since  1.2.1
     * @return void
     */
    public function load_log_content()
    {
        $offset = 0;
        if (isset($_GET['offset'])) {
            $offset = intval(sanitize_text_field($_GET['offset']));
        }

        $data = $this->load_content($offset);

        wp_send_json_success(array(
            'log'       => $data,
            'offset'    => strlen($data),
        ));
    }

    /**
     * Reads log file and returns content (from given $offset to end of file)
     * @since  1.2.1
     * @param  integer $byteOffset
     * @return string
     */
    public function load_content($byteOffset = 0)
    {
        $filename   = self::get_log_file_path();
        $data       = '';

        if (filesize($filename) > 0) {
            $handle = fopen($filename, "r");
            fseek($handle, $byteOffset);
            $data   = fread($handle, filesize($filename));

            fclose($handle);
        }

        return $data;
    }

    /**
     * Clears log file
     * @since  1.2.1
     * @return void
     */
    public function clear_log_content()
    {
        unlink(self::get_log_file_path());

        $handle = fopen(self::get_log_file_path(), "w");
        fclose($handle);
    }

    /**
     * Render the settings page for this plugin.
     * @since    1.0.0
     */
    public function display_plugin_admin_page()
    {
        if (isset($_POST['domain_migration']) && check_admin_referer('migrate_domain', 'migrate_domain_wpnonce')) {
            $old_domain = sanitize_text_field($_POST['old_domain']);
            $new_domain = sanitize_text_field($_POST['new_domain']);
            $domain_replacement_number = $this->migrate_domain($old_domain, $new_domain);
        } else if (isset($_POST['missing_templates']) && check_admin_referer('missing_templates', 'missing_templates_wpnonce')) {
            $number = $this->fix_missing_templates();
            $info_message = sprintf(
                _n(
                    'Missing templates successfully fixed (%d replacement made).',
                    'Missing templates successfully fixed (%d replacements made).',
                    $number,
                    $this->plugin_slug),
                $number);
        } else {
            $old_domain = $new_domain = '';
        }

        if (!isset($_GET['tab'])|| $_GET['tab'] === 'ems') {
            $data = $this->load_content();
        }

        $tabs = array(
            'ems'           => __('EMS Logger', $this->plugin_slug),
            'migrations'    => __('Migrations', $this->plugin_slug),
            'misc'          => __('Miscellaneous', $this->plugin_slug),
            //'soon'        => __('Coming soon...', $this->plugin_slug),
        );

        include_once('views/admin.php');
    }

    /**
     * Fixes missing wp page templates, replaces all "_wp_page_template" values (in postmeta table) where template is non-existent
     * @return integer
     * @since  1.1.0
     */
    protected function fix_missing_templates()
    {
        global $wpdb;

        $allowed_template_names = array_merge(array('default'), array_values(get_page_templates()));

        $status = $wpdb->query($wpdb->prepare(
            "UPDATE " . $wpdb->base_prefix . "postmeta
            SET meta_value = 'default'
            WHERE meta_key = '_wp_page_template' AND meta_value NOT IN (" . substr(str_repeat('%s,', count($allowed_template_names)), 0, -1) . ")",
            $allowed_template_names
        ));

        return $status;
    }

    /**
     * Replaces old domain name with new one (only in OP tables)
     * @param  string $old_domain
     * @param  string $new_domain
     * @return int
     * @since  1.0.0
     */
    protected function migrate_domain($old_domain, $new_domain)
    {
        require_once 'includes/op_customtable_migrator.php';
        $op_tables = new Op_CustomTable_Migrator($old_domain, $new_domain);
        $op_tables->switch_domains();
        return $op_tables->replacement_number;
    }

    /**
     * Returns plugin slug (used for translations)
     * @return string
     * @since  1.0.0
     */
    public function slug()
    {
        return $this->plugin_slug;
    }
}

register_activation_hook(__FILE__, array('Op_Helper', 'activate'));
register_deactivation_hook(__FILE__, array('Op_Helper', 'deactivate'));

add_action('plugins_loaded', array('Op_Helper', 'get_instance'));