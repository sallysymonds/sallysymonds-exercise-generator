<div class="wrap">
    <?php screen_icon(); ?>
    <h2 class="nav-tab-wrapper">
    <?php $activeTab = null; ?>
    <?php foreach ($tabs as $tab => $name) : ?>
        <?php
            if ((isset($_GET['tab']) && $_GET['tab'] === $tab) || (!isset($_GET['tab']) && $tab === 'ems')) {
                $activeTab = $tab;
                $class = ' nav-tab-active';
            } else {
                $class = '';
            }
        ?>
        <a class="nav-tab<?php echo $class; ?>" href="<?php echo admin_url('tools.php?page=' . Op_Helper::slug() . '&tab=' . $tab); ?>"><?php echo $name; ?></a>
    <?php endforeach; ?>
    </h2>

    <?php if ($activeTab === 'ems') : ?>
    <div class="op-ems-actions" style="text-align: right; margin-top: 15px;">
        <a href="#refresh" data-offset="<?php echo strlen($data); ?>" class="op-ems-logger-refresh button button-primary"><?php _e('Reload', Op_Helper::slug()); ?></a>
        <a href="#clear" class="op-ems-logger-clear button button-primary"><?php _e('Clear', Op_Helper::slug()); ?></a>
    </div>
    <pre id="op_ems_logger" style="overflow-y: scroll; background: #fff; border: 1px solid #ddd; padding: 5px; height: 500px;">
<?php echo $data; ?>
    </pre>

    <?php elseif ($activeTab === 'migrations') : ?>
    <h3 class="title"><?php _e('Domain Migration', Op_Helper::slug()); ?></h3>
    <?php if (isset($domain_replacement_number)) : ?>
    <div id="message" class="updated"><p><?php printf(_n('Domain has been successfully updated (%d replacement).', 'Domain has been successfully updated (%d replacements).', $domain_replacement_number, Op_Helper::slug()), $domain_replacement_number); ?></p></div>
    <?php endif; ?>
    <p class="install-help"><?php _e('After moving your content to a new domain you need to modify old domain image paths that OptimizePress uses. Simply enter old and new domain and all DB fields will be updated. Remember to back up your DB before doing this.', Op_Helper::slug()); ?></p>
    <form method="POST">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="old_domain"><?php _e('Old domain', Op_Helper::slug()); ?></label>
                </th>
                <td>
                    <input type="text" name="old_domain" id="old_domain" value="<?php echo $old_domain; ?>" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <label for="new_domain"><?php _e('New domain', Op_Helper::slug()); ?></label>
                </th>
                <td>
                    <input type="text" name="new_domain" id="new_domain" value="<?php echo $new_domain; ?>" />
                </td>
            </tr>
        </table>
        <?php wp_nonce_field('migrate_domain', 'migrate_domain_wpnonce', true, true); ?>
        <?php submit_button(__('Migrate domain', Op_Helper::slug()), 'primary', 'domain_migration'); ?>
    </form>
    <?php elseif ($activeTab === 'misc') : ?>
    <h3 class="title"><?php _e('Miscellaneous operations', OP_Helper::slug()); ?></h3>
    <?php if (isset($info_message)) : ?>
    <div id="message" class="updated"><p><?php echo $info_message; ?></p></div>
    <?php endif; ?>
    <p class="install-help"><?php _e('Asorted tools and operations.', Op_Helper::slug()); ?></p>
    <form method="POST">
        <h4 class="title"><?php _e('Missing page templates', Op_Helper::slug()); ?></h4>
        <p><?php _e('Fix for WordPress bug <a href="http://core.trac.wordpress.org/ticket/25334" target="_blank">#25334</a>. Actions attached to "save_post" are not triggered in case where WordPress is missing a page template. This action will go through the DB and clean all non-existing templates (in postmeta table).', Op_Helper::slug()); ?></p>
        <?php wp_nonce_field('missing_templates', 'missing_templates_wpnonce', true, true); ?>
        <?php submit_button(__('Fix missing page templates', Op_Helper::slug()), 'primary', 'missing_templates'); ?>
    </form>
    <?php endif; ?>
</div>
