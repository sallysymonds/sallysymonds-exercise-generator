<?php
function egen_set_definitions() {

	$labels = array(
		'name'                => _x( 'Set Definitions', 'Post Type General Name', 'sallysymonds' ),
		'singular_name'       => _x( 'Set Definition', 'Post Type Singular Name', 'sallysymonds' ),
		'menu_name'           => __( 'Set Definitions', 'sallysymonds' ),
		'parent_item_colon'   => __( 'Parent Item:', 'sallysymonds' ),
		'all_items'           => __( 'All Items', 'sallysymonds' ),
		'view_item'           => __( 'View Item', 'sallysymonds' ),
		'add_new_item'        => __( 'Add New Item', 'sallysymonds' ),
		'add_new'             => __( 'Add New', 'sallysymonds' ),
		'edit_item'           => __( 'Edit Item', 'sallysymonds' ),
		'update_item'         => __( 'Update Item', 'sallysymonds' ),
		'search_items'        => __( 'Search Item', 'sallysymonds' ),
		'not_found'           => __( 'Exercises Not found', 'sallysymonds' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'sallysymonds' ),
	);
	$args = array(
		'label'               => __( 'set definitions', 'sallysymonds' ),
		'description'         => __( 'set definitions', 'sallysymonds' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail','custom-fields' ), 
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
	    //'register_meta_box_cb' => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	
	register_post_type( 'set-definitions', $args );
	//flush_rewrite_rules();

}

// Hook into the 'init' action
add_action( 'init', 'egen_set_definitions', 0 );

/* Custom MetaBox*/


add_action( 'add_meta_boxes', 'mb_set_definitions_bodyparts_no' );
	
function mb_set_definitions_bodyparts_no() {
	    add_meta_box(
	    		'mb_set_definitions_bodyparts_no_form', 
	    		__( 'How many body parts?' , 'sallysymonds' ), 
	    		'mb_set_definitions_bodyparts_no_form', 
	    		'set-definitions', 
	    		'side', 
	    		'default'
	    );
}	

function mb_set_definitions_bodyparts_no_form(){
	
	global $post;
	
	_e( '<input type="hidden" name="mb_set_definitions_bodyparts_no_noncename" id="mb_set_definitions_bodyparts_no_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />');
	
	$egen_quantity	= get_post_meta( $post->ID, '_egen_quantity', true );
	
	$table = '<table style="width: 100%;">';
	$table .= '<tr>';
	$table .= '<td>';
	$table .= '<input type="number" value="'.$egen_quantity.'" name="_egen_quantity" min="1" max="100" placeholder="Quantity (between 1 and 5)" style="width: 100%;">';
    $table .= '</td>';
	$table .= '</tr>';
    $table .= '</table>';
    _e( $table );
	
}

function save_mb_set_definitions_bodyparts_no_form($post_id, $post) {
	
	if ( !wp_verify_nonce( $_POST['mb_set_definitions_bodyparts_no_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}
	
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	
	$egen_quantity_meta['_egen_quantity'] 		= $_POST['_egen_quantity']; 
	
	foreach ( $egen_quantity_meta as $key => $value ) { 
		
		if( $post->post_type == 'revision' ) return; 
		$value = implode(',', (array)$value); 
		if(get_post_meta($post->ID, $key, FALSE)) { 
			update_post_meta($post->ID, $key, $value);
		} else { 
			add_post_meta($post->ID, $key, $value);
		}
		
		if(!$value) delete_post_meta($post->ID, $key); 
	}

}

add_action('save_post', 'save_mb_set_definitions_bodyparts_no_form', 1, 2); // save the custom fields

