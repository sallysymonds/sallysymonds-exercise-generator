<?php

function sally_tax_fitnes_level() {
  $labels = array(
    'name'              => _x( 'Fitness Levels', 'taxonomy general name' ),
    'singular_name'     => _x( 'Fitness Level', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Fitness Levels' ),
    'all_items'         => __( 'All Fitness Level' ),
    'parent_item'       => __( 'Parent Fitness Level' ),
    'parent_item_colon' => __( 'Parent Fitness Level:' ),
    'edit_item'         => __( 'Edit Fitness Level' ), 
    'update_item'       => __( 'Update Fitness Level' ),
    'add_new_item'      => __( 'Add New Fitness Level' ),
    'new_item_name'     => __( 'New Fitness Level' ),
    'menu_name'         => __( 'Fitness Levels' ),
  );
  
  $args = array(
    'labels' 		=> $labels,
    'hierarchical' 	=> true,
    'public' 		=> true,
    'show_ui' 		=> true,
  	'show_admin_column' => true,
  	'sort' => true
  	
  );
  
  register_taxonomy( 'fitness_level', 'exercise-generator', $args );
  //flush_rewrite_rules();
}

add_action( 'init', 'sally_tax_fitnes_level', 0 );