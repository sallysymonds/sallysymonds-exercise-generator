<?php
 /**
  * Plugin Name: Exercise Generator Search and Tag Plugin
  * Plugin URI: http://www.sallysymonds.com
  * Description: Exercise Generator Search and Tag Plugin
  * Version: 1.0
  * Author: Netseek
  * Author URI:  http://netseek.com.au
  */


function egen_shortcode_func( $atts ){
	
	extract( shortcode_atts( array(
			'default' 		=> '', 
			'fitness_level'	=> '' // beginner , intermediate , advanced 
		), $atts , 'exercise_egenerator' ) 
	);
	
	

	$taxonomy = 'bodyparts';
	$taxonomy_terms = get_terms( $taxonomy, array(
    	'hide_empty' => 0,
    	'fields' => 'ids',
    	'order'  => 'ASC',
		'orderby' => 'ID',
	) );

	$taxonomy_query = new WP_Query( array(
	    'tax_query' => array(
	        array(
	            'taxonomy' => $taxonomy,
	            'field' => 'id',
	            'terms' => $taxonomy_terms,
	        ),
	    ),
	) );
	
	$bodyparts_tax = $taxonomy_query->query_vars[ 'tax_query' ][0][ 'terms' ];
	
	
	$tax_fitness_level = 'fitness_level';
	$tax_level_terms = get_terms( $tax_fitness_level, array(
    	'hide_empty' => 0,
    	'fields' => 'ids',
    	'order'  => 'ASC',
		'orderby' => 'ID',
	) );

	$tax_level_query = new WP_Query( array(
	    'tax_query' => array(
	        array(
	            'taxonomy' => $tax_fitness_level,
	            'field' => 'id',
	            'terms' => $tax_level_terms,
	        ),
	    ),
	) );
		
	
	$fitness_level_tax = $tax_level_query->query_vars[ 'tax_query' ][0][ 'terms' ];
	
    
    $exerciseshtml = '';
    
    $exerciseshtml .= wp_nonce_field( 'egen_nonce_action','egen_security_nonce' );
    
    $exerciseshtml .= '<div id="le_body_row_1_col_1_el_2" data-style="" class="element-container cf"><div class="element"> What is your fitness level?  </div></div>';
    
    
    $exerciseshtml .= '<ul class="tag-fitness-level">';
	
	if( is_array ( $fitness_level_tax ) ){
		$exerciseshtml .= '<select id="_egen_fitness_level" name="_egen_fitness_level"  style="width: 70%;padding:5px;">';
    	foreach ( $fitness_level_tax as $term_level_id ){
    		$term_level = get_term( $term_level_id, $tax_fitness_level );
    		//$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart[]" value="'.$termid.'" > '.$term_level->name .'</li>';
    		
  			$exerciseshtml .= '<option value="'.$term_level_id.'" >'.__( $term_level->name  ,'sallysymonds').'</option>';
  			//$exerciseshtml .= '<option value="intermediate" >'.__('Intermediate','sallysymonds').'</option>';
  			//$exerciseshtml .= '<option value="advanced">'.__('Advanced','sallysymonds').'</option>';
  	
    	}
    	$exerciseshtml .= '</select>';
    	
    }
    
    /*
	$exerciseshtml .= '<li>';
	$exerciseshtml .= '<select id="_egen_fitness_level" name="_egen_fitness_level"  style="width: 70%;padding:5px;">';
  	$exerciseshtml .= '<option value="beginner" >'.__('Beginner','sallysymonds').'</option>';
  	$exerciseshtml .= '<option value="intermediate" >'.__('Intermediate','sallysymonds').'</option>';
  	$exerciseshtml .= '<option value="advanced">'.__('Advanced','sallysymonds').'</option>';
  	$exerciseshtml .= '</select>';
  	$exerciseshtml .= '</li>';
  	*/
  	
	$exerciseshtml .= '</ul>';
	
    
    $exerciseshtml .= '<div id="le_body_row_1_col_1_el_2" data-style="" class="element-container cf"><div class="element"> Which body part/s would you like to exercise today: </div></div>';
    $exerciseshtml .= '<div id="le_body_row_1_col_1_el_3" data-style="" class="element-container cf"><div class="element"> <div style="width:100%;text-align: left;" class="op-text-block">';
    
    $exerciseshtml .= '<form name="egenForm" id="egenForm" method="post">';
    
    $exerciseshtml .= '<ul class="tag-search">';

/*    
$taxonomyName = $taxonomy;
$terms = get_terms($taxonomyName,array('parent' => 0));
echo '<pre>'; print_r($terms); echo '</pre>';
foreach($terms as $term) {
    echo '<a href="'.get_term_link($term->slug,$taxonomyName).'">'.$term->name.'</a>';
    $term_children = get_term_children($term->term_id,$taxonomyName);
    echo '<ul>';
    foreach($term_children as $term_child_id) {
        $term_child = get_term_by('id',$term_child_id,$taxonomyName);
        echo '<li><a href="' . get_term_link( $term_child->term_id, $taxonomyName ) . '">' . $term_child->name . '</a></li>';
    }
    echo '</ul>';
}
 */   
   
    if( is_array ( $bodyparts_tax ) ){
    	
    	foreach ( $bodyparts_tax as $termid ){
    		$term = get_term( $termid, $taxonomy );
    		
    		 if( $term->parent ==  0 ){
    		 	
					$termchildren = get_term_children( $termid, $taxonomy );
    		 	
					//$display	  = ( !empty( $termchildren ) ? 'style="width: 100%;"' : '' );
					$onclick	  = ( !empty( $termchildren ) ? "onclick=showChildrenBP(this.value);return false;" : '' );
					
    	 			$exerciseshtml .= '<li id="li_'.$termid.'" ><input type="checkbox" id="bodypart" name="bodypart[]" value="'.$termid.'" '.$onclick.' > '.$term->name;
    	 			
    	 			if( ! empty( $termchildren ) ){
    	 		
						//$exerciseshtml .=  '<ul style="display: block; height: auto;clear: both; ">';
						$exerciseshtml .=  '<ul id="ul_'.$termid.'" style="display: none; height: auto;clear: both;margin-left: 45px; ">';
						foreach ( $termchildren as $child ) {
							$term = get_term_by( 'id', $child, $taxonomy );
							//$exerciseshtml .=  '<li><a href="' . get_term_link( $child, $taxonomy ) . '">' . $term->name . '</a></li>';
							$exerciseshtml .=  '<li><input type="checkbox" id="bodypart" name="bodypart[]" value="'.$child.'" >' . $term->name . '</li>';
						}
						$exerciseshtml .=  '</ul>';
						
    	 			}
    	 			
    	 			$exerciseshtml .= '</li>';
    	 	
    	 	 }
    		
    		
    		
    	}
    	
    }
	/*
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="legs" > Legs</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="thighs-butt"> Thighs &amp; Butt</li>';
	$exerciseshtml .= '<li><input id="body-part-abdominals" type="checkbox" id="bodypart" name="bodypart" value="abdominals"> Abdominals </li>';
	
	$exerciseshtml .= '<li style="display:none" id="abdominals-sub"> <input id="upsize-abdominals-bodypart" type="checkbox" name="bodypart" value="upsize-abdominals"> Upsize Abdominals </li>';
	
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="chest"> Chest</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="back"> Back</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="arms"> Arms</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="biceps"> Biceps</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="triceps"> Triceps</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="shoulders"> Shoulders</li>';
	$exerciseshtml .= '<li><input type="checkbox" id="bodypart" name="bodypart" value="calves"> Calves</li>';
	*/
	$exerciseshtml .= '</ul>';
	$exerciseshtml .= '</div> </div></div>';
	$exerciseshtml .= '</form>';
	$exerciseshtml .= '<div class="egen_loader"></div>';
    $exerciseshtml .= '<div class="video_wrapper"></div>';
	 
    
    return $exerciseshtml; 
    
}

add_shortcode( 'egen', 'egen_shortcode_func' );


function queryEgen( $params ){
	
	$args = array(
	  'post_type' 		=> 'exercise-generator',	
	  'posts_per_page' 	=> -1,	
	  'numberposts' 	=> -1,
      'post_status'   	=> 'any',
	  'orderby'       	=> 'title',
	  'order' 		  	=> 'ASC'
    );
   
    if( ! empty( $params[ 'bodyparts' ] ) ){
   		
    	/*
    	$args['meta_query'] = array(
										array(
											'key'     => '_egen_fitness_level',
											'value'   => $params[ 'fitneslevel' ] ,
											'compare' => '=',
										)
									);
		*/							

		$args['tax_query'] = array(	
									//'relation' => 'AND',
										array(
											'taxonomy' => 'bodyparts',
											'field'    => 'id',
											'terms'    => $params[ 'bodyparts' ],
											//'include_children' => true,
											'operator' => 'IN',
										), 
										//'relation' => 'AND',
										array(
											'taxonomy' => 'fitness_level',
											'field'    => 'id',
											'terms'    => $params[ 'fitneslevel' ],
											//'include_children' => true,
											'operator' => 'IN',
										), 
									);									
    	
    }
   
	
 	
    
    
    $videoPosts = new WP_Query( $args );
    
    $exerciseshtml = '';
    
	//$exerciseshtml .= '<div class="video_wrapper">';
	$exerciseshtml .= '<div class="video_menus">';
	
	if( $videoPosts->have_posts() ) {
	$exerciseshtml .= '<div id="egen_sally_video_list" class="sally_video_list">';
	//$exerciseshtml .= '<ul class="ul3col">';
	
	 foreach ( $videoPosts->posts as $post ) {
	 	
	 	$title =  $post->post_title;
		if (strlen($title) > 20) {
				$title = substr($title, 0, 15) . '...';
		}
		

 				$post_id 	= $post->ID;
				$taxonomy 	= 'bodyparts';
				$args 		= array();
				
				$terms = wp_get_post_terms( $post_id, $taxonomy, $args );
				
				if( !empty( $terms ) ){
					foreach ($terms as $term ) {
						$term_id = $term->term_id;
						$term_name = $term->name;
						//$tags[$term_id][$post_id][] = $title;
						
						$tags[$term_id][] = array(		
													'bp_id'   => $term_id,
													'bp_name' => $term_name,
													'post_id' => $post->ID,
													'post_title' => $title
												);
						
						
						
						//$tags[] = $term->name;
						//$parts = $term->name; 
					}
					
					
					//$tags = array_unique($tags, SORT_REGULAR);
					
					
					/*
					$parts = implode(',', $tags );	
				
							if (strlen($parts) > 20) {
							$parts = substr($parts, 0, 15) . '...';
					}
					*/
					
				}
	 			
			/*	
	 		$add_id = 'add_'.$post->ID;
	 		$exerciseshtml .= '<li>';
	 		$exerciseshtml .= '<center>';
	 		$exerciseshtml .= '<span class="egenVideoTitle">'. __( $parts , 'sallysymonds' ) . '</span>';
	 		$exerciseshtml .= '<br>';
	 		$exerciseshtml .= '<span class="egenVideoTitle">'. __( $title , 'sallysymonds' ) . '</span>';
	 		$exerciseshtml .= '<br>';
	 		$exerciseshtml .= "<a href='#egen_sally_video_list' id='egenSmallImg' onclick=egenCheckFunction('".$add_id."');return false;>". get_the_post_thumbnail( $post->ID, 'thumbnail' ) ."</a>";
	 		$add_checkbox_id = 'addcheckBox_'.$post->ID;
	 		$exerciseshtml .= "<br><span class='egenCheckLabel' >Select: <input  type='checkbox' id='".$add_id."' name='bodypart2[]' value='".$post->ID."' style='border-bottom-width: 0px !important;' onclick=egenCheckBoxFunction('".$add_id."');return false;></span></center>";
        	$exerciseshtml .= '</li>';
        	*/
	 }
	 
	 
	 if ( ! empty( $params[ 'bodyparts' ] ) ) {
	 	foreach ( $params[ 'bodyparts' ] as $p ){
	 		
	 		
	 		$r = $tags[$p];
	 		$t = get_term( $p, 'bodyparts' );
	 		
	 		if( !empty($r) ){
	 			$exerciseshtml .= '<ul class="ul3col">';
					$exerciseshtml .= '<li><span style="background-color: rgb(204, 204, 204); height: 17px; padding: 5px; width: 100%">'.__( $t->name , 'sallysymonds' ).'</span>';
						$exerciseshtml .= '<ul>';	 	
							foreach ( $r as $rs ){
									
								$add_id = 'add_'.$rs['post_id'];
						 		$exerciseshtml .= '<li>';
						 		$exerciseshtml .= '<center>';
						 		$exerciseshtml .= '<span class="egenVideoTitle">'. __( $t->name , 'sallysymonds' ) . '</span>';
						 		$exerciseshtml .= '<br>';
						 		$exerciseshtml .= '<span class="egenVideoTitle">'. __( $rs['post_title'] , 'sallysymonds' ) . '</span>';
						 		$exerciseshtml .= '<br>';
						 		//$exerciseshtml .= "<a href='#egen_sally_video_list' id='egenSmallImg' onclick=egenCheckFunction('".$add_id."','".$p."');return false;>". get_the_post_thumbnail( $rs['post_id'] , 'thumbnail' ) ."</a>";
						 		$exerciseshtml .= "<a href='#egen_sally_video_list' id='egenSmallImg' onclick=egenCheckFunction('".$add_id."');return false;>". get_the_post_thumbnail( $rs['post_id'] , 'thumbnail' ) ."</a>";
						 		$add_checkbox_id = 'addcheckBox_'.$rs['post_id'];
						 		$exerciseshtml .= "<br><span class='egenCheckLabel' >Select: <input  type='checkbox' id='".$add_id."' name='bodypart2[]' data_term='".$p."' value='".$rs['post_id']."' style='border-bottom-width: 0px !important;' onclick=egenCheckBoxFunction('".$add_id."');return false;></span></center>";
					        	$exerciseshtml .= '</li>';
        	
							}
						$exerciseshtml .= '</ul>';					
					$exerciseshtml .= '</li>';					
	 			$exerciseshtml .= '</ul>';	
	 		}
	 		
	 	}
	 }
	 
	 
	 //echo '<pre>'; print_r( $tags ); echo '</pre>';
	  
	//$exerciseshtml .= '</ul>';
	$exerciseshtml .= '<div class="egen_video_tags" style="clear: both;" ></div>';
	$exerciseshtml .= '</div><!-- end  -->';	
	
	}else{
		$exerciseshtml .=  '<p>'. __( 'Sorry, no posts matched your criteria.' ). '</p>';
	}
	
	$exerciseshtml .= '</div><!-- end video_menus -->';
	//$exerciseshtml .= '</div><!-- end video_wrapper -->';
	
	
	
	wp_reset_postdata();
	
	return $exerciseshtml;
    
    
}

/* Custom MetaBox*/


//add_action( 'add_meta_boxes', 'add_egen_fitness_level_metaboxes' );
	
function add_egen_fitness_level_metaboxes() {
	    add_meta_box('egen_fitness_level_form', 'Fitness Level', 'egen_fitness_level_form', 'exercise-generator', 'side', 'default');
}	

function egen_fitness_level_form(){
	
	global $post;
	
	_e( '<input type="hidden" name="egen_fitness_level_form_meta_noncename" id="egen_fitness_level_form_meta_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />');
	
	$fitness_level	= get_post_meta( $post->ID, '_egen_fitness_level', true );
	
	$beginner = ( 'beginner' == $fitness_level ? 'selected' : '' );
	$intermediate = ( 'intermediate' == $fitness_level ? 'selected': '' );
	$advanced = ( 'advanced' == $fitness_level ? 'selected' : '' );
	
	$table = '<table style="width: 100%;">';
	$table .= '<tr>';
	$table .= '<td>';
	$table .= '<select name="_egen_fitness_level" style="width: 100%;">';
  	$table .= '<option value="beginner" '.$beginner.'>'.__('Beginner','sallysymonds').'</option>';
  	$table .= '<option value="intermediate" '.$intermediate.'>'.__('Intermediate','sallysymonds').'</option>';
  	$table .= '<option value="advanced" '.$advanced.'>'.__('Advanced','sallysymonds').'</option>';
  	$table .= '</select>';
 	$table .= '</td>';
	$table .= '</tr>';
    $table .= '</table>';
    _e( $table );
	
}

function save_egen_fitness_level_form($post_id, $post) {
	
	if ( !wp_verify_nonce( $_POST['egen_fitness_level_form_meta_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}
	
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	
	$fitness_level_meta['_egen_fitness_level'] 		= $_POST['_egen_fitness_level'];
	
	foreach ( $fitness_level_meta as $key => $value ) { 
		
		if( $post->post_type == 'revision' ) return; 
		$value = implode(',', (array)$value); 
		if(get_post_meta($post->ID, $key, FALSE)) { 
			update_post_meta($post->ID, $key, $value);
		} else { 
			add_post_meta($post->ID, $key, $value);
		}
		
		if(!$value) delete_post_meta($post->ID, $key); 
	}

}

//add_action('save_post', 'save_egen_fitness_level_form', 1, 2); // save the custom fields

add_action( 'wp_enqueue_scripts', 'egen_js_library' );

function egen_js_library() {
	
	if( ! is_admin() ){
	
		wp_register_style( 'egen-css',  plugins_url( 'assets/css/style.css' , __FILE__ ) );
		wp_enqueue_style( 'egen-css' );
	
		wp_register_script( 'egen-script', plugins_url( 'assets/js/scripts.js' , __FILE__ ) , array( 'jquery' )  );
		wp_localize_script( 'egen-script', 'egenAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ))); 
		
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'egen-script' );
	
	}
	
}


add_action( 'wp_ajax_egen_get_exercises', 'egen_get_exercises_service' );
add_action( 'wp_ajax_nopriv_egen_get_exercises', 'egen_get_exercises_service' );

function egen_get_exercises_service() {
    
	if ( 
	    ! isset( $_REQUEST[ 'token' ] ) 
	    || ! wp_verify_nonce( $_REQUEST[ 'token' ], 'egen_nonce_action' ) 
	) {
	   
	   $return[ 'ResponseCode' ] 		= FALSE;
	   $return[ 'ResponseMessage' ] 	= 'Sorry, your nonce did not verify.'; 
	   wp_send_json( $return );
	   exit;
	
	} else {
	   
		switch ( $_REQUEST['method'] ){
			
			case 'get_video_exercises':
		
			   if( ! empty( $_REQUEST[ 'bodyParts' ] ) ){
			   		
			   		$parts = array();
			   		
			   		foreach ( $_REQUEST[ 'bodyParts' ] as $part ){
			   			$parts[] = $part[ 'value' ];
			   		}
			   		
			   		$params[ 'bodyparts' ] = $parts;
			   		$params[ 'fitneslevel' ] = $_REQUEST[ 'fitnesLevel' ];
			   		
			   		$html = queryEgen( $params );
			   		
			   		$return[ 'ResponseCode' ] = true;
			   		$return[ 'ResponseHtml' ] = $html;
			   			
			   }else{
			   	
			   		$return[ 'ResponseCode' ] 		= FALSE;
			   		$return[ 'ResponseMessage' ] 	= 'No Exercise(s) Found!';
			   		
			   } 

			break;
			case 'get_video_tags':
				
				if( ! empty( $_REQUEST[ 'id' ] ) ){
					
					$post_id = explode( '_', $_REQUEST[ 'id' ] );
					$post_id = $post_id[1];
					$taxonomy = 'bodyparts';
					$args = array();
					
					$terms = wp_get_post_terms( $post_id, $taxonomy, $args );
					
					if( !empty( $terms ) ){
						foreach ($terms as $term ) {
							$tags[] = $term->name; 
						}

						$index = $_REQUEST[ 'index' ];
						
						$parts = implode(',', $tags );
						$html = "<p> Video {$index}: {$parts} </p>";
						
						$return[ 'ResponseCode' ] = true;
			   			$return[ 'ResponseHtml' ] = $html;
			   		
					}else{
						$return[ 'ResponseCode' ] 		= false;
			   			$return[ 'ResponseMessage' ] 	= 'No Exercise(s) Found!';
					}
					
					//echo '<pre>'; print_r( $terms ); echo '</pre>';
					//exit;
					 
				}else{
					$return[ 'ResponseCode' ] 		= false;
			   		$return[ 'ResponseMessage' ] 	= 'No Exercise(s) Found!';
				}
				
				//echo '<pre>'; print_r( $_REQUEST ); echo '</pre>';
				//exit;
				
				//
				
				break;	
				
			case 'get_set_definitions':
				
				//echo '<pre>'; print_r( $_REQUEST ); echo '</pre>';
				//exit;
				
				if( ! empty( $_REQUEST[ 'index' ] ) ){
					
					// Get Post Ids
					
					$post_ids 	= $_REQUEST['ids'];
					$term_ids    = $_REQUEST['term_ids'];
					$exercise_bodyparts = gen_tags_reader( $post_ids , $term_ids );
				
					
					$args = array(
					  	'post_type' 	=> 'set-definitions',	
					  	'posts_per_page'=> -1,	
					  	'numberposts' 	=> -1,
				      	'post_status'   => 'any',
					  	'orderby'       => 'title',
					  	'order' 		=> 'ASC',
						'meta_query'	=>	array(
													array(
														'key'     => '_egen_quantity',
														'value'   => $_REQUEST[ 'index' ] ,
														'compare' => '=',
													)
											)
				    );
				   
				    
				    
    				$definitions = new WP_Query( $args );
    				
    				if( $definitions->have_posts() ) {
    					
  						$optionvalue = '';
  						$postsvalue = '';
    					
  						foreach ( $definitions->posts as $key => $definition ){
    						//$set_definition[] = $definition->post_title;
    						
  							//$the_exercise = $exercise_bodyparts[$key]['exercise'];
  							//$the_bodyparts = $exercise_bodyparts[$key]['bodypart'];
  							
  							//
  							 
  							//echo '<pre>'; print_r( $the_bodyparts ); echo '</pre>';
  							
  							//$exercise_bodyparts;
  							
  							$the_title = str_replace( array_keys( $exercise_bodyparts ), $exercise_bodyparts , $definition->post_title );
  							$the_content = str_replace( array_keys( $exercise_bodyparts ), $exercise_bodyparts , $definition->post_content );
  							
    						$optionvalue .= '<option value="'.$definition->ID.'" >'.__( $the_title ,'sallysymonds').'</option>';
    						$postsvalue .= '<div style="display:none" id="'.$definition->ID.'" ><strong>'.__( $the_title ,'sallysymonds').'</strong> <hr>'.wpautop( __( $the_content ,'sallysymonds') ).'</div>';
    					}
    					
    					
    					$html .= '<div id="_egen_definitions_title_holder" >';
    					$html .= '<select onChange="showDefinitionsContent(this.value)" id="_egen_definitions_title" name="_egen_definitions_title" style="width: 70%;padding:5px;">';
  						$html .= '<option value="" >'.__( 'Please Select Set Definitions ' ,'sallysymonds').'</option>';
    					$html .= $optionvalue;
    					$html .= '</select>';
    					$html .= '</div>';
    					$html .= '<div id="definitionsContent" >';
    					$html .= $postsvalue;
    					$html .= '</div>';
    					
    					//echo '<pre>'; print_r( $definitions->posts ); echo '</pre>';
    					
    					
    					
    					$return[ 'ResponseCode' ] = true;
			   			$return[ 'ResponseHtml' ] = $html;
    					
    				}else{
    					
    					$return[ 'ResponseCode' ] 		= false;
			   			$return[ 'ResponseMessage' ] 	= 'No Set Definition(s) Found!';
    				}
    				
    				//echo '<pre>'; print_r( $definitions->posts ); echo '</pre>';
					wp_reset_postdata();
					
					
				}else{
					$return[ 'ResponseCode' ] 		= false;
			   		$return[ 'ResponseMessage' ] 	= 'No Set Definition(s) Found!';
				}
				
				break;
		}
		
	   wp_send_json( $return );
	   exit;
	}
    //
    exit;
}

function gen_tags_reader( $post_ids , $term_ids ){
	
	if( ! empty( $post_ids ) ) {
	
		$args = array();
		$alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
		$results = array();
		
		
		foreach ( $term_ids as $tk => $tid ){
			$terms = get_term( $tid, 'bodyparts' );
			$tags = $terms->name;
			//$exercise_no = 	$k + 1;
	      	//$results['{{EXERCISECHECKBOX_'.$exercise_no.'}}'] = $title;
	      	$results['{{BODYPART_'.$alpha[$tk].'}}'] = $tags;
	      						
		}
		
		foreach ( $post_ids as $k => $id ){
			$title = get_the_title( $id );
			$exercise_no = 	$k + 1;
	      	$results['{{EXERCISECHECKBOX_'.$exercise_no.'}}'] = $title;
	      	
	      						
		}
		
		//echo '<pre>'; print_r($term_ids); echo '</pre>';
		//echo '<pre>'; print_r($results); echo '</pre>';
		
		/*
		foreach ( $post_ids as $k => $id ){
			$title = get_the_title( $id );
			$terms = wp_get_post_terms( $id , 'bodyparts', $args );
				if( !empty( $terms ) ){
					//echo '<pre>'; print_r($term_id); echo '</pre>';
					//echo '<pre>'; print_r($terms); echo '</pre>';
					foreach ($terms as $term ) {
							$tags = $term->name;	 
					}
				}

			$exercise_no = 	$k + 1;
			
	      	$results['{{EXERCISECHECKBOX_'.$exercise_no.'}}'] = $title;
	      	$results['{{BODYPART_'.$alpha[$k].'}}'] = $tags;
	      						
		}
		*/
		
	}
	
	if( empty( $results )){
		$results = array();
	}
	
	return $results;
	
}


include_once 'set-definitions-post-type.php';
include_once 'fitness-level-tax.php';
