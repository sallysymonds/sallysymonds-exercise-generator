jQuery(document).ready(function($) {
	
	var egenForm = $( '#egenForm' );
	var egen_fitness_level = $( '#_egen_fitness_level' );
	var egen_nonce = $( '#egen_security_nonce' ).val();
	
	
	$( "#_egen_fitness_level" ).change(function() {
		var level_id  = $(this).val();
		var n 		  = $( "input[name='bodypart[]']:checked" ).length;
		 
		 if( n > 5 ){
			 alert( "Exercise today limit has been reached. " );
			 return false;
		 }
		
		 var bodyParts 	= egenForm.serializeArray();
		 var level 		= egen_fitness_level.val();
		
		 $( '.egen_loader' ).html( '<img style="margin-top:10px;" src="/wp-content/plugins/wp-sally-exercise-gen/assets/images/103.gif" alt="" style="">' );
		 $( '.video_wrapper' ).html( '' );
		 
		 		 
		 jQuery.post(
				 egenAjax.ajaxurl, 
				    {
				        'action':	 	'egen_get_exercises',
				        'bodyParts':  	bodyParts,
				        'fitnesLevel':	level,
				        'token':		 egen_nonce,
				        'method': 		'get_video_exercises'
				    }, 
				    function( response ){
				        $( '.egen_loader' ).html( '' );
				        $( '.video_wrapper' ).html( response.ResponseHtml );
				    }
				);
		 
		 
	});
	
	$( "input[type=checkbox]" ).on( "click",  function() {
		
		 //var n 			= $( "input:checked" ).length;
		var n 			= $( "input[name='bodypart[]']:checked" ).length;
		 
		 if( n > 5 ){
			 alert( "Exercise today limit has been reached. " );
			 return false;
		 }
		 
		 var bodyParts 	= egenForm.serializeArray();
		 var level 		= egen_fitness_level.val();
		//Processing please wait...
		 $( '.egen_loader' ).html( '<img style="margin-top:10px;" src="/wp-content/plugins/wp-sally-exercise-gen/assets/images/103.gif" alt="" style="">' );
		 //$( '.egen_loader' ).html( 'Processing please wait...' );
		 $( '.video_wrapper' ).html( '' );
		 		 
		 jQuery.post(
				 egenAjax.ajaxurl, 
				    {
				        'action':	 	'egen_get_exercises',
				        'bodyParts':  	bodyParts,
				        'fitnesLevel':	level,
				        'token':		 egen_nonce,
				        'method': 		'get_video_exercises'
				    }, 
				    function( response ){
				    	
				        $( '.egen_loader' ).html( '' );
				        $( '.video_wrapper' ).html( '' );
				        
				        if( true === response.ResponseCode ){
				        	$( '.video_wrapper' ).html( response.ResponseHtml );
				        }else{
				        	$( '.video_wrapper' ).html( response.ResponseMessage );
				        }
				        
				    }
				);
		 
		}
	);
	
	
	if( $( '#abdominals-exercises' ).is(':checked') ){
		$( '#abdominals-exercises-sub' ).show();
		$( '#upsize-abdominals-exercises' ).prop('checked', true );
    }

    $( '#abdominals-exercises' ).click( function(){
    	
    	 if( $(this).is(':checked') ){
    		 $( '#abdominals-exercises-sub' ).show();
    		 $( '#upsize-abdominals-exercises' ).prop('checked', true );
    	 }else{
    		 $( '#abdominals-exercises-sub' ).hide();
    		 $( '#upsize-abdominals-exercises' ).prop('checked', false );
    	 }
    	
    });
    
    
   
    
});


function showDefinitionsContent( id ){
	//$( "#definitionsContent" ).siblings().hide();
	
	$( "#definitionsContent" ).children().hide();
	
	if( id != ''  ){
		$( '#'+ id  ).show();
	}
}

function egenCheckBoxFunction( id  ){
	
		var egen_nonce = $( '#egen_security_nonce' ).val();
		
		var n 			= $( "input[name='bodypart2[]']:checked" ).length;
		var m 			= $("input[name='bodypart2[]']:checked").map( function () {return this.value;}).get();
		var o 			= $("input[name='bodypart2[]']:checked").map( function () {
			
			var ret  = $(this).attr("data_term");
			return ret;
			
			}).get();
		
		if( n > 5 ){
			 alert( "Video checkbox selections limit has been reached." );
			 $( '#' + id ).prop('checked', false );
			 return false;
		 }
		
		 
		console.log( m );
		
		$( '.egen_video_tags' ).html( '' );
		
		
			
			$( '.egen_video_tags' ).html( 'Processing please wait...' );
			
			jQuery.post(
					 egenAjax.ajaxurl, 
					    {
					        'action' : 'egen_get_exercises',
					        'token'  : egen_nonce,
					        'method' : 'get_set_definitions',
					        'id'     : id,
					        'index'  : n,
					        'ids'    : m,
					        'term_ids': o
					    }, 
					    function( response ){
					    	console.log( response );
					    	if( true === response.ResponseCode ){
					    		$( '.egen_video_tags' ).html( response.ResponseHtml );
					    	}else{
					    		$( '.egen_video_tags' ).html( response.ResponseMessage );
					    	}
					    	
					    }
					);
	
	
	return false;
}

function egenCheckFunction( id , term_id ){
	
	var egen_nonce = $( '#egen_security_nonce' ).val();
	
	if( $( '#' + id ).is(':checked') ){
		$( '#' + id ).prop('checked', false );
		//var n = $( "input[name='bodypart2[]']" ).length;
	}else{
		$( '#' + id ).prop('checked', true );
		
		var n 			= $( "input[name='bodypart2[]']:checked" ).length;
		var m 			= $("input[name='bodypart2[]']:checked").map( function () {return this.value;}).get();
		var o 			= $("input[name='bodypart2[]']:checked").map( function () {
			
			var ret  = $(this).attr("data_term");
			return ret;
			
			}).get();
		
		if( n > 5 ){
			 alert( "Video checkbox selections limit has been reached." );
			 $( '#' + id ).prop('checked', false );
			 return false;
		 }
		
		 
		
		//var count = $(".egen_video_tags > p").children().size();
		
		console.log( o );
		
		$( '.egen_video_tags' ).html( '' );
		
		$( '.egen_video_tags' ).html( 'Processing please wait...' );
		
		jQuery.post(
				 egenAjax.ajaxurl, 
				    {
				        'action': 'egen_get_exercises',
				        'token' : egen_nonce,
				        'method': 'get_set_definitions',
				        'id'    : id,
				        'index' : n,
				        'ids'   : m,
				        'term_ids': o
				    }, 
				    function( response ){
				    	console.log( response );
				        //$( '.egen_loader' ).html( '' );
				        //$( '.egen_video_tags' ).append( response.ResponseHtml );
				    	if( true === response.ResponseCode ){
				    		$( '.egen_video_tags' ).html( response.ResponseHtml );
				    	}else{
				    		$( '.egen_video_tags' ).html( response.ResponseMessage );
				    	}
				    	
				    }
				);
		
	}
	
	return false;
}

function showChildrenBP( id ){

	//alert( id );
	
	//if( $( '#' + id ).is(':checked') ){
		
	//}
	
	if( false === $( '#ul_' + id ).is(':visible') ) {
		$( '#li_' + id ).css("width","100%");
		$( '#ul_' + id ).css("display","block");
	}else{
		$( '#li_' + id ).css("width","");
		$( '#ul_' + id ).css("display","none");
	}
	
	
	
	
}
