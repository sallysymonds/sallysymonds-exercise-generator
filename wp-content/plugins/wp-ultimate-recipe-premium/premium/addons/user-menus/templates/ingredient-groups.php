<div class="wrap">
    <h2><?php _e( 'Ingredient Groups', 'wp-ultimate-recipe' ); ?></h2>


    <div class="ingredient-group-header">
        <div class="ingredient-group-input-container">
            <input type="text" id="newGroup" placeholder="<?php _e( 'Vegetables', 'wp-ultimate-recipe' ); ?>"><br/>
            <button class="button" onclick="IngredientGroups.addGroup(this)"><?php _e( 'Add Group', 'wp-ultimate-recipe' ); ?></button>
        </div>
    </div>

    <div class="ingredient-groups">
        <?php
        $args = array(
            'hide_empty' => false
        );
        $ingredients = get_terms( 'ingredient', $args );

        $ungrouped = '# ' . __('Ungrouped', 'wp-ultimate-recipe');

        $groups = array(
            $ungrouped => array()
        );

        foreach( $ingredients as $ingredient ) {
            $group = WPURP_Taxonomy_MetaData::get( 'ingredient', $ingredient->slug, 'group' );

            if(!$group) {
                $groups[$ungrouped][] = $ingredient;
            } else {
                if( !isset( $groups[$group] ) ) {
                    $groups[$group] = array();
                }
                $groups[$group][] = $ingredient;
            }
        }

        ksort( $groups );

        foreach( $groups as $group => $ingredients ) {

            echo '<div class="ingredient-group-container">';
            echo '<h3>'.$group.'</h3>';
            echo '<ul class="ingredient-group" data-name="'.$group.'">';

            foreach( $ingredients as $ingredient ) {
                echo '<li data-slug="'.$ingredient->slug.'">' . $ingredient->name . '</li>';
            }

            echo '</ul></div>';
        }
        ?>
    </div>
</div>