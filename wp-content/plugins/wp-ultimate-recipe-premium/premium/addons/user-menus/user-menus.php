<?php

class WPURP_User_Menus extends WPURP_Premium_Addon {

    public function __construct( $name = 'user-menus' ) {
        parent::__construct( $name );

        // Actions
        add_action( 'init', array( $this, 'assets' ) );
        add_action( 'init', array( $this, 'menus_init' ));
        add_action( 'admin_init', array( $this, 'user_menus_admin_init' ));
        add_action( 'admin_menu', array( $this, 'ingredient_groups_menu' ), 5 );

        // Ajax
        add_action( 'wp_ajax_user_menus_groupby', array( $this, 'ajax_user_menus_groupby' ) );
        add_action( 'wp_ajax_nopriv_user_menus_groupby', array( $this, 'ajax_user_menus_groupby' ) );
        add_action( 'wp_ajax_user_menus_get_ingredients', array( $this, 'ajax_user_menus_get_ingredients' ) );
        add_action( 'wp_ajax_nopriv_user_menus_get_ingredients', array( $this, 'ajax_user_menus_get_ingredients' ) );
        add_action( 'wp_ajax_user_menus_save', array( $this, 'ajax_user_menus_save' ) );
        add_action( 'wp_ajax_nopriv_user_menus_save', array( $this, 'ajax_user_menus_save' ) );
        add_action( 'wp_ajax_ingredient_groups_save', array( $this, 'ajax_ingredient_groups_save' ) );
        add_action( 'wp_ajax_nopriv_ingredient_groups_save', array( $this, 'ajax_ingredient_groups_save' ) );

        //Filters
        add_filter( 'the_content', array( $this, 'user_menus_content' ), 10 );

        // Shortcode
        add_shortcode( 'wpurp_user_menus', array( $this, 'user_menus_shortcode' ) ); // For backwards compatibility
        add_shortcode( 'ultimate-recipe-user-menus', array( $this, 'user_menus_shortcode' ) );
        add_shortcode( 'ultimate-recipe-menu', array( $this, 'display_menu_shortcode' ) );
    }

    public function assets() {
        WPUltimateRecipe::get()->helper( 'assets' )->add(
            // User menus shortcode
            array(
                'file' => WPUltimateRecipe::get()->coreUrl . '/vendor/select2/select2.css',
                'display' => 'public',
                'setting_inverse' => array( 'user_menus_enable', 'off' ),
            ),
            array(
                'file' => $this->addonUrl . '/css/user-menus.css',
                'display' => 'public',
                'setting_inverse' => array( 'user_menus_enable', 'off' ),
            ),
            array(
                'file' => WPUltimateRecipe::get()->coreUrl . '/vendor/select2/select2.min.js',
                'name' => 'select2',
                'display' => 'public',
                'setting_inverse' => array( 'user_menus_enable', 'off' ),
                'deps' => array(
                    'jquery',
                ),
            ),
            array(
                'file' => $this->addonUrl . '/js/user-menus.js',
                'name' => 'wpurp-user-menus',
                'display' => 'public',
                'setting_inverse' => array( 'user_menus_enable', 'off' ),
                'deps' => array(
                    'jquery',
                    'wpurp-unit-conversion',
                    'js-quantities',
                    'jquery-ui-sortable',
                    'jquery-ui-droppable',
                    'select2',
                ),
                'data' => array(
                    'name' => 'wpurp_user_menus',
                    'ajaxurl' => WPUltimateRecipe::get()->helper('ajax')->url(),
                    'addonUrl' => $this->addonUrl,
                    'nonce' => wp_create_nonce( 'wpurp_user_menus' ),
                    'adjustable_system' => WPUltimateRecipe::option( 'recipe_adjustable_units', '1' ),
                    'default_system' => WPUltimateRecipe::option( 'user_menus_default_unit_system', '0' ),
                )
            ),
            // Ingredient groups page
            array(
                'file' => $this->addonUrl . '/css/ingredient-groups.css',
                'display' => 'admin',
                'page' => 'recipe_page_wpurp_ingredient_groups',
            ),
            array(
                'file' => $this->addonUrl . '/js/ingredient-groups.js',
                'display' => 'admin',
                'page' => 'recipe_page_wpurp_ingredient_groups',
                'deps' => array(
                    'jquery',
                    'jquery-ui-sortable',
                    'jquery-ui-droppable',
                ),
                'data' => array(
                    'name' => 'wpurp_ingredient_groups',
                    'ajaxurl' => WPUltimateRecipe::get()->helper('ajax')->url(),
                    'nonce' => wp_create_nonce( 'wpurp_ingredient_groups' )
                )
            )
        );
    }

    public function ingredient_groups_menu()
    {
        add_submenu_page( 'edit.php?post_type=recipe', 'WP Ultimate Recipe ' . __( 'Ingredient Groups', 'wp-ultimate-recipe' ), __( 'Ingredient Groups', 'wp-ultimate-recipe' ), 'manage_options', 'wpurp_ingredient_groups', array( $this, 'ingredient_groups_menu_page' ) );
    }

    public function ingredient_groups_menu_page() {
        include( $this->addonDir . '/templates/ingredient-groups.php' );
    }

    public function ajax_ingredient_groups_save()
    {
        if(check_ajax_referer( 'wpurp_ingredient_groups', 'security', false ) )
        {
            $slug = $_POST['slug'];
            $group = $_POST['group'];

            if( $group == '# ' . __( 'Ungrouped', 'wp-ultimate-recipe' ) ) {
                $group = '';
            }

            WPURP_Taxonomy_MetaData::set( 'ingredient', $slug, 'group', $group );
        }

        die();
    }

    public function menus_init()
    {
        $slug = WPUltimateRecipe::option( 'user_menus_slug', 'menu' );

        $name = __( 'Menus', 'wp-ultimate-recipe' );
        $singular = __( 'Menu', 'wp-ultimate-recipe' );

        $args = apply_filters( 'wpurp_register_menu_post_type',
            array(
                'labels' => array(
                    'name' => $name,
                    'singular_name' => $singular,
                    'add_new' => __( 'Add New', 'wp-ultimate-recipe' ),
                    'add_new_item' => __( 'Add New', 'wp-ultimate-recipe' ) . ' ' . $singular,
                    'edit' => __( 'Edit', 'wp-ultimate-recipe' ),
                    'edit_item' => __( 'Edit', 'wp-ultimate-recipe' ) . ' ' . $singular,
                    'new_item' => __( 'New', 'wp-ultimate-recipe' ) . ' ' . $singular,
                    'view' => __( 'View', 'wp-ultimate-recipe' ),
                    'view_item' => __( 'View', 'wp-ultimate-recipe' ) . ' ' . $singular,
                    'search_items' => __( 'Search', 'wp-ultimate-recipe' ) . ' ' . $name,
                    'not_found' => __( 'No', 'wp-ultimate-recipe' ) . ' ' . $name . ' ' . __( 'found.', 'wp-ultimate-recipe' ),
                    'not_found_in_trash' => __( 'No', 'wp-ultimate-recipe' ) . ' ' . $name . ' ' . __( 'found in trash.', 'wp-ultimate-recipe' ),
                    'parent' => __( 'Parent', 'wp-ultimate-recipe' ) . ' ' . $singular,
                ),
                'public' => true,
                'menu_position' => 5,
                'supports' => array( 'title' ),
                'taxonomies' => array( '' ),
                'menu_icon' =>  WPUltimateRecipe::get()->coreUrl . '/img/icon_16.png',
                'has_archive' => true,
                'rewrite' => array(
                    'slug' => $slug
                ),
                'show_in_menu' => 'edit.php?post_type=recipe',
            )
        );

        register_post_type( 'menu', $args );
    }

    public function user_menus_admin_init() {
        add_meta_box(
            'user_menus_meta_box',
            __( 'Menu', 'wp-ultimate-recipe' ),
            array( $this, 'user_menus_meta_box' ),
            'menu',
            'normal',
            'high'
        );
    }

    public function user_menus_meta_box( $menu, $menu_id = '' )
    {
        _e( 'The menu can be edited from the front end:', 'wp-ultimate-recipe' );
        echo '<br/>';
        echo '<a href="'.get_permalink( $menu->ID ).'">';
        echo get_the_title( $menu->ID );
        echo '</a>';
    }

    public function user_menus_content( $content ) {

        if ( is_single() && get_post_type() == 'menu' ) {
            remove_filter( 'the_content', array( $this, 'user_menus_content' ), 10 );

            if( !$content ) {

                $menu = get_post();

                $recipes = get_post_meta( $menu->ID, 'user-menus-recipes' );
                $order = get_post_meta( $menu->ID, 'user-menus-order' );

                wp_localize_script( 'wpurp-user-menus', 'wpurp_user_menu',
                    array(
                        'recipes' => $recipes[0],
                        'order' => $order[0],
                        'nbrRecipes' => get_post_meta( $menu->ID, 'user-menus-nbrRecipes', true ),
                        'unitSystem' => get_post_meta( $menu->ID, 'user-menus-unitSystem', true ),
                        'menuId' => $menu->ID,
                    )
                );

                ob_start();
                include( $this->addonDir . '/templates/user-menus.php');
                $content = ob_get_contents();
                ob_end_clean();

                $content = apply_filters( 'wpurp_user_menus_form', $content, $menu );

                add_filter('the_content', array( $this, 'user_menus_content' ), 10);

            }
        }

        return $content;
    }

    public function ajax_user_menus_save()
    {
        if( check_ajax_referer( 'wpurp_user_menus', 'security', false ) )
        {
            global $user_ID;

            $menu_id = intval( $_POST['menuId'] );
            $title = $_POST['title'];
            $recipes = $_POST['recipes'];
            $order = $_POST['order'];
            $nbrRecipes = $_POST['nbrRecipes'];
            $unitSystem = $_POST['unitSystem'];

            // Create new menu
            if( $menu_id === 0 ) {
                $post = array(
                    'post_status' => 'publish',
                    'post_date' => date('Y-m-d H:i:s'),
                    'post_author' => $user_ID,
                    'post_type' => 'menu',
                    'post_title' => $title,
                );

                //Save post
                $menu_id = wp_insert_post( $post );
            } else {
                $post = array(
                    'ID' => $menu_id,
                    'post_title' => $title
                );

                wp_update_post( $post );
            }

            update_post_meta( $menu_id, 'user-menus-recipes', $recipes );
            update_post_meta( $menu_id, 'user-menus-order', $order );
            update_post_meta( $menu_id, 'user-menus-nbrRecipes', $nbrRecipes );
            update_post_meta( $menu_id, 'user-menus-unitSystem', $unitSystem );

            die( get_permalink( $menu_id ) );
        }

        die();
    }

    public function ajax_user_menus_get_ingredients()
    {
        if( check_ajax_referer( 'wpurp_user_menus', 'security', false ) )
        {
            $recipe_id = intval( $_POST['recipe_id'] );

            $ingredients = get_post_meta( $recipe_id, 'recipe_ingredients' );
            $ingredients = $ingredients[0];

            $ingredients_with_groups = array();

            foreach( $ingredients as $ingredient )
            {
                $term = get_term( $ingredient['ingredient_id'], 'ingredient' );
                $ingredient['group'] = WPURP_Taxonomy_MetaData::get( 'ingredient', $term->slug, 'group' );

                if( !$ingredient['group'] ) {
                    $ingredient['group'] = __( 'Other', 'wp-ultimate-recipe' );
                }

                $ingredients_with_groups[] = $ingredient;
            }

            echo json_encode( $ingredients_with_groups );
        }

        die();
    }

    public function ajax_user_menus_groupby()
    {
        if( check_ajax_referer( 'wpurp_user_menus', 'security', false ) )
        {
            $groupby = $_POST['groupby'];
            $groups = $this->get_recipes_grouped_by( $groupby );
            echo $this->get_select_options( $groups );
        }

        die();
    }

    public function get_recipes_grouped_by( $groupby )
    {
        global $wpurp;

        $recipes_grouped = array();
        switch( $groupby ) {

            case 'a-z':
                $recipes = WPUltimateRecipe::get()->query()->order_by( 'title' )->order( 'ASC' )->get();

                $current_letter = '';
                $current_recipes = array();

                foreach( $recipes as $recipe ) {
                    $letter = strtoupper( mb_substr( $recipe->title(), 0, 1 ) );

                    if( $letter != $current_letter ) {
                        if($current_letter != '') {
                            $recipes_grouped[] = array(
                                'group_name' => $current_letter,
                                'recipes' => $current_recipes
                            );
                        }

                        $current_letter = $letter;
                        $current_recipes = array();
                    }

                    $current_recipes[] = $recipe;
                }

                if( count( $current_recipes ) > 0 ) {
                    $recipes_grouped[] = array(
                        'group_name' => $current_letter,
                        'recipes' => $current_recipes
                    );
                }

                break;

            default:
                $terms = get_terms( $groupby );

                foreach( $terms as $term ) {
                    $recipes_grouped[] = array(
                        'group_name'    => $term->name,
                        'recipes' => WPUltimateRecipe::get()->query()->order_by( 'title' )->order( 'ASC' )->taxonomy( $groupby )->term( $term->slug )->get(),
                    );
                }
                break;
        }

        return $recipes_grouped;
    }

    public function get_select_options($recipe_groups)
    {
        $out = '<option></option>';

        foreach( $recipe_groups as $group )
        {
            $out .= '<optgroup label="'.$group['group_name'].'">';

            foreach( $group['recipes'] as $recipe )
            {
                $servings = $recipe->servings_normalized();
                if( $servings < 1 ) {
                    $servings = 1;
                }

                $out .= '<option value="' . $recipe->ID() . '" data-servings="' . $servings . '" data-link="' . $recipe->link() . '">' . $recipe->title() . '</option>';
            }

            $out .= '</optgroup>';
        }

        return $out;
    }

    public function user_menus_shortcode()
    {

        switch( WPUltimateRecipe::option( 'user_menus_enable', 'guests' ) ) {

            case 'off':
                return '<p class="errorbox">' . __( 'Sorry, the site administrator has disabled user menus.', 'wp-ultimate-recipe' ) . '</p>';
                break;

            case 'registered':
                if( !is_user_logged_in() ) {
                    return '<p class="errorbox">' . __( 'Sorry, only registered users may create menus.', 'wp-ultimate-recipe' ) . '</p>';
                }
            // Logged in? Fall through!
            case 'guests':
                ob_start();
                include( $this->addonDir . '/templates/user-menus.php' );
                return ob_get_clean();
                break;
        }

    }

    public function display_menu_shortcode( $options )
    {
        $options = shortcode_atts( array(
            'id' => 'random', // If no ID given, show a random menu
            'template' => 'default'
        ), $options );

        $menu = null;

        if( $options['id'] == 'random' ) {

            $posts = get_posts(array(
                'post_type' => 'menu',
                'nopaging' => true
            ));

            $menu = $posts[ array_rand( $posts ) ];
        } else {
            $menu = get_post( intval( $options['id'] ) );
        }

        if( !is_null( $menu ) && $menu->post_type == 'menu' )
        {
            $recipes = get_post_meta( $menu->ID, 'user-menus-recipes' );
            $order = get_post_meta( $menu->ID, 'user-menus-order' );

            wp_localize_script( 'wpurp-user-menus', 'wpurp_user_menu',
                array(
                    'recipes' => $recipes[0],
                    'order' => $order[0],
                    'nbrRecipes' => get_post_meta( $menu->ID, 'user-menus-nbrRecipes', true ),
                    'unitSystem' => get_post_meta( $menu->ID, 'user-menus-unitSystem', true ),
                    'menuId' => $menu->ID,
                )
            );

            $menu_display_only = true;

            ob_start();
            include( $this->addonDir . '/templates/user-menus.php');
            $output = ob_get_contents();
            ob_end_clean();
        }
        else
        {
            $output = '';
        }

        return $output;
    }
}

WPUltimateRecipe::loaded_addon( 'user-menus', new WPURP_User_Menus() );