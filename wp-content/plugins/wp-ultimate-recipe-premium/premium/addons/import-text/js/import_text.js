var ImportText = ImportText || {};

jQuery(document).ready(function() {
    ImportText.init();
});

ImportText.init = function() {

    // Variables
    ImportText.raw_input = '';
    ImportText.recipe = {};

    // Rangy
    rangy.init();
    ImportText.rangyCSStitle        = rangy.createCssClassApplier("region-title-highlight", {normalize: true});
    ImportText.rangyCSSdescription  = rangy.createCssClassApplier("region-description-highlight", {normalize: true});
    ImportText.rangyCSSingredients  = rangy.createCssClassApplier("region-ingredients-highlight", {normalize: true});
    ImportText.rangyCSSinstructions = rangy.createCssClassApplier("region-instructions-highlight", {normalize: true});
    ImportText.rangyCSSnotes        = rangy.createCssClassApplier("region-notes-highlight", {normalize: true});

    jQuery('#input-recipe').on('keyup change', function() {
        ImportText.raw_input = jQuery(this).val();
        ImportText.updateRegionsText();
    });

    // Select region
    jQuery('.regions-button').on('click', function() {

        // Get region type from button id
        var type = jQuery(this).attr('id').replace('region-', '');

        // Get and highlight selected text
        var text = ImportText.getAndHighlightText(type);
        ImportText.recipe[type] = text;

        // Update corresponding form field and trigger change event
        if(type == 'instructions' || type == 'ingredients') {
            jQuery('#raw_recipe_'+type).val(text).change();
        } else {
            jQuery('#recipe_'+type).val(text).change();
        }
    });

    // Adaptive textareas
    jQuery('#raw_recipe_ingredients, #raw_recipe_instructions').on('keyup change', function() {
        ImportText.adaptiveTextarea(jQuery(this));
        ImportText.getSeparateLines(jQuery(this));
    });

    jQuery('#recipe_instructions_separate_type').on('change', function() {
        // Trigger change in instructions
        jQuery('#raw_recipe_instructions').change();
    });
}

ImportText.updateRegionsText = function() {
    jQuery('#regions-text').html(ImportText.raw_input.replace(/\r?\n/g,'<br/>'));
}

ImportText.getAndHighlightText = function(type) {
    var selectedText = '';
    var sel = rangy.getSelection(), rangeCount = sel.rangeCount;

    var range = rangy.createRange();
    range.selectNodeContents(jQuery('#regions-text')[0]);

    for (var i = 0; i < rangeCount; ++i) {
        var textInRange = sel.getRangeAt(i).intersection(range);

        if(textInRange !== null) {
            selectedText += textInRange.toHtml();
        }
    }

    selectedText = selectedText.replace(/<br\s*[\/]?>/gi, '\n');

    ImportText['rangyCSS' + type].undoToRange(range);
    ImportText['rangyCSS' + type].toggleSelection();
    range.detach();
    if(rangeCount > 0) {
        sel.collapseToStart();
    }
    return selectedText;
}

ImportText.adaptiveTextarea = function(textarea) {
    var scrollTop = jQuery(window).scrollTop();
    textarea.height(0).height(textarea.prop('scrollHeight'));
    jQuery(window).scrollTop(scrollTop);
}

ImportText.getSeparateLines = function(textarea) {
    var type = 'every_line';
    var id = textarea.attr('id');
    var lines = textarea.val();
    var target = jQuery('#' + id + '_lines');

    if(id == 'raw_recipe_instructions') {
        type = jQuery('#recipe_instructions_separate_type option:selected').val();
    }

    if(type == 'every_line') {
        lines = lines.split('\n');
    } else {
        lines = lines.split(/\n\s*\n/);
    }

    target.empty();
    if(id == 'raw_recipe_instructions') {
        jQuery('#recipe_instructions_output').empty();
    } else {
        jQuery('#define-ingredient-details tbody').empty();
    }

    for(var i = 0, l = lines.length; i<l; i++) {
        target.append('<li>' + lines[i].replace(/\r?\n/g,'<br/>') + '</li>');

        if(id == 'raw_recipe_instructions') {
            ImportText.addInstructionRow(lines[i], i);
        } else {
            var ingredient = ImportText.parseIngredient(lines[i]);
            ImportText.addRecipeRow(ingredient, i);
        }
    }
}

ImportText.parseIngredient = function(text) {
    var ingredient = {};

    // Amount
    ingredient.amount = '';
    var regex = /^\s*\d[\s\/\-\d.,]*/g;
    if(regex.test(text)) {
        var matches = text.match(regex);
        ingredient.amount = matches[0].trim();

        text = text.replace(regex, '');
    }

    // Unit
    ingredient.unit = '';
    var units = wpurp_import_text.unit_aliases;

    dance:
    for(var unit in units) {
        var regex = new RegExp('(?:^|\\s)+'+unit+'\\s','g');

        if(regex.test(text)) {
            var matches = text.match(regex);
            ingredient.unit = matches[0].trim();

            text = text.replace(regex, '');
            break dance;
        }
    }

    // Notes
    ingredient.notes = '';
    var regex = /,.*/g;
    if(regex.test(text)) {
        var matches = text.match(regex);
        var match = matches[0].substring(1); // Drop the ,
        ingredient.notes = match.trim();

        text = text.replace(regex, '');
    }

    var regex = /\([^\)]*\)/g;
    if(regex.test(text)) {
        var matches = text.match(regex);
        for(var i in matches) {
            if(ingredient.notes != '') {
                ingredient.notes += ', ';
            }

            var match = matches[i].replace('(','').replace(')','');
            ingredient.notes += match.trim();
        }

        text = text.replace(regex, '');
    }

    // Name
    ingredient.name = text.trim();

    return ingredient;
}

ImportText.addRecipeRow = function(ingredient, i) {
    var row = jQuery('<tr></tr>');
    var amount = jQuery('<td><input type="text" name="recipe_ingredients['+i+'][amount]" class="ingredients_amount" id="ingredients_amount_'+i+'" value="'+ingredient.amount+'" /></td>');
    var unit = jQuery('<td><input type="text" name="recipe_ingredients['+i+'][unit]" class="ingredients_unit" id="ingredients_unit_'+i+'" value="'+ingredient.unit+'" /></td>');
    var name = jQuery('<td><input type="text" name="recipe_ingredients['+i+'][ingredient]" class="ingredients_name" id="ingredients_'+i+'" value="'+ingredient.name+'" /></td>');
    var notes = jQuery('<td><input type="text" name="recipe_ingredients['+i+'][notes]" class="ingredients_notes" id="ingredients_notes_'+i+'" value="'+ingredient.notes+'" /></td>');

    row.append(amount)
        .append(unit)
        .append(name)
        .append(notes);

    jQuery('#define-ingredient-details tbody').append(row);
}

ImportText.addInstructionRow = function(instruction, i) {
    jQuery('#recipe_instructions_output')
        .append('<textarea name="recipe_instructions['+i+'][description]" id="ingredient_description_'+i+'">'+instruction+'</textarea>');
}