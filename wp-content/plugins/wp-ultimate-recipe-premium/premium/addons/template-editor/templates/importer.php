<?php
$data = file_get_contents( 'php://input' );
$objData = json_decode( $data );

require_once( '../../../../../../../wp-load.php' );

if( isset( $objData->template_id ) ) {
    $template = WPUltimateRecipe::get()->template( 'recipe', $objData->template_id );
} else {
    $template = get_option( 'wpurp_custom_template_preview' );
}

class WPURP_Importer {

    protected $template;
    protected $blocks = array();

    public function __construct( $template )
    {
        // Get the generated template
        $this->template = $template;

        $this->import( $template->blocks );
        echo json_encode( $this->blocks );
    }

    protected function import( $template_blocks )
    {
        foreach( $template_blocks as $index => $template_block)
        {
            $block = array();
            foreach( $template_block->settings as $setting => $value )
            {
                $block[$setting] = $value;
            }

            $this->blocks[$index] = $block;
        }
    }
}

new WPURP_Importer( $template );