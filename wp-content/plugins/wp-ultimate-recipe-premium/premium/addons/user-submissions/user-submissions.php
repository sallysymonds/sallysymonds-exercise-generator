<?php

class WPURP_User_Submissions extends WPURP_Premium_Addon {

    public function __construct( $name = 'user-submissions' ) {
        parent::__construct( $name );

        add_action( 'init', array( $this, 'assets' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'scripts_for_image_upload' ), -10 );
        add_action( 'init', array( $this, 'allow_logged_in_uploads' ) );

        add_action( 'wp_ajax_query-attachments', array( $this, 'ajax_restrict_media' ), 1 );
        add_action( 'wp_ajax_nopriv_query-attachments', array( $this, 'ajax_restrict_media' ), 1 );

        add_shortcode( 'wpurp_submissions', array( $this, 'submissions_shortcode' ) ); // For backwards compatibility
        add_shortcode( 'ultimate-recipe-submissions', array( $this, 'submissions_shortcode' ) );
    }

    public function assets() {
        WPUltimateRecipe::get()->helper( 'assets' )->add(
            array(
                'file' => $this->addonUrl . '/css/public.css',
                'display' => 'public',
                'shortcode' => array( 'wpurp_submissions', 'ultimate-recipe-submissions' ),
                'setting' => array( 'user_submission_css', '1' ),
            ),
            array(
                'file' => $this->addonUrl . '/css/public_base.css',
                'display' => 'public',
                'shortcode' => array( 'wpurp_submissions', 'ultimate-recipe-submissions' ),
            ),
            array(
                'file' => WPUltimateRecipe::get()->coreUrl . '/js/recipe_form.js',
                'display' => 'public',
                'shortcode' => array( 'wpurp_submissions', 'ultimate-recipe-submissions' ),
                'deps' => array(
                    'jquery',
                    'jquery-ui-sortable',
                    'suggest',
                ),
                'data' => array(
                    'name' => 'wpurp_recipe_form',
                    'coreUrl' => WPUltimateRecipe::get()->coreUrl,
                )
            )
        );
    }

    public function allow_logged_in_uploads() {
        if( is_user_logged_in() && !current_user_can('upload_files') && WPUltimateRecipe::option( 'user_submission_enable', 'guests' ) != 'off' ) {
            $user = wp_get_current_user();
            $user->add_cap('upload_files');
        }
    }

    public function scripts_for_image_upload() {

        if( current_user_can( 'upload_files' ) && WPUltimateRecipe::option( 'user_submission_enable', 'guests' ) != 'off' && WPUltimateRecipe::get()->helper( 'assets' )->check_for_shortcode( array( 'wpurp_submissions', 'ultimate-recipe-submissions' ) ) )
        {
            if( function_exists( 'wp_enqueue_media' ) ) {
                wp_enqueue_media();
            } else {
                wp_enqueue_style( 'thickbox' );
                wp_enqueue_script( 'media-upload' );
                wp_enqueue_script( 'thickbox' );
            }
        }
    }

    public function ajax_restrict_media()
    {
        if( WPUltimateRecipe::option( 'user_submission_restrict_media_access', '1' ) == '1' && !current_user_can( 'edit_others_posts' ) ) {
            exit;
        }
    }

    public function submissions_shortcode() {

        switch( WPUltimateRecipe::option( 'user_submission_enable', 'guests' ) ) {

            case 'off':
                return '<p class="errorbox">' . __( 'Sorry, the site administrator has disabled recipe submissions.', 'wp-ultimate-recipe' ) . '</p>';
                break;

            case 'guests':
                if( isset( $_POST['submitrecipe'] ) ) {
                    return $this->submissions_process();
                } else {
                    return $this->submissions_form();
                }
                break;

            case 'registered':
                if( !is_user_logged_in() ) {
                    return '<p class="errorbox">' . __( 'Sorry, only registered users may submit recipes.', 'wp-ultimate-recipe' ) . '</p>';
                } else {
                    if( isset( $_POST['submitrecipe'] ) ) {
                        return $this->submissions_process();
                    } else {
                        return $this->submissions_form();
                    }
                }
                break;

        }

    }

    public function submissions_form() {

        // Create autosave when submission page viewed
        global $user_ID;
        $recipe_draft = array(
            'post_status' => 'auto-draft',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => $user_ID,
            'post_type' => 'recipe',
        );

        $recipe_ID = wp_insert_post( $recipe_draft );
        $recipe = new WPURP_Recipe( $recipe_ID );

        ob_start();
        include( $this->addonDir . '/templates/public_recipe_form.php' );
        $form = ob_get_contents();
        ob_end_clean();

        return apply_filters( 'wpurp_user_submissions_form', $form, $recipe );
    }

    public function submissions_process() {
        $successmsg = '';

        if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {

            wp_verify_nonce( $_POST['submitrecipe'], 'recipe_submit' );

            $title = isset( $_POST['title'] ) ? $_POST['title'] : '';
            $_POST['recipe_title'] = $title;

            $post = array(
                'post_title' => $title,
                'post_type'	=> 'recipe',
            );

            $auto_approve = WPUltimateRecipe::option( 'user_submission_approve', 'off' );

            // Post status
            if( $auto_approve == 'guests' ) {
                $post['post_status'] = 'publish';
            } elseif( $auto_approve == 'registered' ) {
                if( is_user_logged_in() ) {
                    $post['post_status'] = 'publish';
                } else {
                    $post['post_status'] = 'draft';
                }
            } else {
                $post['post_status'] = 'draft';
            }

            // Save post
            $post_id = wp_insert_post( $post );

            // Success message
            if( 'draft' == $post['post_status'] ) {
                $successmsg = __( 'Recipe submitted! Thank you, your recipe is now awaiting moderation.', 'wp-ultimate-recipe' );
            } elseif( 'publish' == $post['post_status'] ) {
                $successmsg = __( 'Recipe submitted! Thank you, your recipe has been published.', 'wp-ultimate-recipe' );
            }

            // Add terms
            $taxonomies = WPUltimateRecipe::get()->tags();
            unset($taxonomies['ingredient']);

            if( WPUltimateRecipe::option( 'recipe_tags_user_submissions_categories', '0' ) == '1' ) {
                $taxonomies['category'] = true;
            }

            if( WPUltimateRecipe::option('recipe_tags_user_submissions_tags', '0' ) == '1' ) {
                $taxonomies['post_tag'] = true;
            }

            foreach( $taxonomies as $taxonomy => $options ) {
                if( isset( $_POST['recipe-'.$taxonomy] ) ) {
                    $term = get_term_by( 'id', $_POST['recipe-'.$taxonomy], $taxonomy );

                    if($term) {
                        wp_set_object_terms( $post_id, $term->name, $taxonomy );
                    }
                }
            }

            // If guest, add author name
            if( !is_user_logged_in() ) {
                if( $_POST['recipe-author'] != '' ) {
                    $authorname = $_POST['recipe-author'];
                } else {
                    $authorname = __( 'Anonymous', 'wp-ultimate-recipe' );
                }
                update_post_meta( $post_id, 'recipe-author', $authorname );
            }

            // Add featured image from media uploader
            if( isset( $_POST['recipe_thumbnail'] ) ) {
                update_post_meta( $post_id, '_thumbnail_id', $_POST['recipe_thumbnail'] );
            }

            // Add all images from basic uploader
            if( $_FILES ) {
                foreach( $_FILES as $key => $file ) {
                    if ( 'recipe-thumbnail' == $key ) {
                        if( $file['name'] != '' ) {
                            $this->insert_attachment_basic( $key, $post_id, true );
                        }
                    } else {
                        $this->insert_attachment_basic( $key, $post_id, false );
                    }
                }
            }

            // Send notification email to administrator
            if( WPUltimateRecipe::option('user_submission_email_admin', '0' ) == '1' ) {
                $to = get_option( 'admin_email' );

                if( $to ) {
                    $edit_link = get_edit_post_link( $post_id, '');

                    $subject = 'New user submission: ' . $title;
                    $message = 'A new recipe has been submitted on your website.';
                    $message .= "\r\n\r\n";
                    $message .= 'Edit this recipe: ' . $edit_link;

                    wp_mail( $to, $subject, $message );
                }
            }
        }

        do_action('wp_insert_post', 'wp_insert_post');
        return '<p class="successbox">' . $successmsg . '</p>';
    }

    public function insert_attachment_basic( $file_handler, $post_id, $setthumb = false ) {
        if ( $_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK ) {
            return;
        }

        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );

        $attach_id = media_handle_upload( $file_handler, $post_id );

        if( true == $setthumb ) { // Thumbnail image
            update_post_meta( $post_id, '_thumbnail_id', $attach_id );
        } else { // Instructions image
            $number = explode( '_', $file_handler );
            $number = $number[2];
            $instructions = get_post_meta( $post_id, 'recipe_instructions', true );
            $instructions[$number]['image'] = $attach_id;
            update_post_meta( $post_id, 'recipe_instructions', $instructions );
        }

        return $attach_id;
    }
}

WPUltimateRecipe::loaded_addon( 'user-submissions', new WPURP_User_Submissions() );