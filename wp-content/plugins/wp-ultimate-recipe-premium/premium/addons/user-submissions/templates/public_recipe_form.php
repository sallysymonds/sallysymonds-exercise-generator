<div id="wpurp_user_submission_form" class="postbox">
    <form id="new_recipe" name="new_recipe" method="post" action="" enctype="multipart/form-data">
        <p>
            <label for="title"><?php _e( 'Recipe title', 'wp-ultimate-recipe' ); ?></label><br />
            <input type="text" id="title" value="" size="20" name="title" />
        </p>

<?php if( !is_user_logged_in() ) { ?>
        <p>
            <label for="recipe-author"><?php _e( 'Your name', 'wp-ultimate-recipe' ); ?></label><br />
            <input type="text" id="recipe-author" value="" size="50" name="recipe-author" />
        </p>
<?php } ?>
<?php if ( !current_user_can( 'upload_files' ) ) { ?>
        <p>
            <label for="recipe-thumbnail"><?php _e( 'Featured image', 'wp-ultimate-recipe' ); ?></label><br />
            <input class="recipe_thumbnail_image button" type="file" id="recipe-thumbnail" value="" size="50" name="recipe-thumbnail" />
<?php } else { ?>
        <p>
            <input name="recipe_thumbnail" class="recipe_thumbnail_image" type="hidden" value="" />
            <input class="recipe_thumbnail_add_image button button" rel="<?php echo $recipe->ID(); ?>" type="button" value="<?php _e( 'Add Featured Image', 'wp-ultimate-recipe' ); ?>" />
            <input class="recipe_thumbnail_remove_image button wpurp-hide" type="button" value="<?php _e('Remove Featured Image', 'wp-ultimate-recipe' ); ?>" />
            <br /><img src="" class="recipe_thumbnail" />
        </p>
<?php } ?>
        <p>
<?php
        $taxonomies = WPUltimateRecipe::get()->tags();
        unset( $taxonomies['ingredient'] );

        $args = array(
            'echo' => 0,
            'hide_empty' => 0,
            'hierarchical' => 1,
        );

        foreach( $taxonomies as $taxonomy => $options ) {
            $args['show_option_none'] = $options['labels']['singular_name'];
            $args['taxonomy'] = $taxonomy;
            $args['name'] = 'recipe-' . $taxonomy;

            echo wp_dropdown_categories( $args );
        }

        if(WPUltimateRecipe::option( 'recipe_tags_user_submissions_categories', '0' ) == '1' ) {
            $args['show_option_none'] = __( 'Category', 'wp-ultimate-recipe' );
            $args['taxonomy'] = 'category';
            $args['name'] = 'recipe-category';

            echo wp_dropdown_categories( $args );

        }

        if(WPUltimateRecipe::option( 'recipe_tags_user_submissions_tags', '0' ) == '1' ) {
            $args['show_option_none'] = __( 'Tag', 'wp-ultimate-recipe' );
            $args['taxonomy'] = 'post_tag';
            $args['name'] = 'recipe-post_tag';

            echo wp_dropdown_categories( $args );
        }
?>
        </p>
<?php
        $wpurp_user_submission = true;
        include( WPUltimateRecipe::get()->coreDir . '/helpers/recipe_form.php' );
?>
        <p align="right">
            <input type="submit" value="<?php _e( 'Submit', 'wp-ultimate-recipe' ); ?>" id="submit" name="submit" />
        </p>
        <input type="hidden" name="action" value="post" />
        <?php echo wp_nonce_field( 'recipe_submit', 'submitrecipe' ); ?>
    </form>
</div>