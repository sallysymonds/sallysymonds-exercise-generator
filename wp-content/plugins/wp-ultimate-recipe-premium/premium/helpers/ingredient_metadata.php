<?php

class WPURP_Ingredient_Metadata {

    public function __construct()
    {
        new WPURP_Taxonomy_MetaData( 'ingredient', array(
            'link' => array(
                'label'       => __( 'Link', 'wp-ultimate-recipe' ),
                'desc'        => __( 'Send your visitors to a specific link when clicking on an ingredient.', 'wp-ultimate-recipe' ),
                'placeholder' => 'http://www.example.com',
            ),
            'group' => array(
                'label'       => __( 'Group', 'wp-ultimate-recipe' ),
                'desc'        => __( 'Use this to group ingredients in the shopping list.', 'wp-ultimate-recipe' ),
                'placeholder' => __( 'Vegetables', 'wp-ultimate-recipe' ),
            ),
        ) );

        add_filter( 'manage_edit-ingredient_columns', array( $this, 'add_link_column_to_ingredients' ) );
        add_filter( 'manage_ingredient_custom_column', array( $this, 'add_link_column_content' ), 10, 3 );
    }

    public function add_link_column_to_ingredients($columns)
    {
        $columns['link'] = __( 'Link', 'wp-ultimate-recipe' );
        return $columns;
    }

    public function add_link_column_content($content, $column_name, $term_id)
    {
        $term = get_term( $term_id, 'ingredient' );

        if( $column_name == 'link' ) {
            $custom_link = WPURP_Taxonomy_MetaData::get( 'ingredient', $term->slug, 'link' );

            if( $custom_link ) {
                $content = $custom_link;
            }
        }

        return $content;
    }
}