<?php
 /**
  * Plugin Name: Sally Symonds Calculator Plugins
  * Plugin URI: http://www.sallysymonds.com
  * Description: Sally Symonds Calculator Integration
  * Version: 1.0
  * Author: Netseek
  * Author URI:  http://netseek.com.au
  */

add_shortcode( 'BMI', 'sally_bmi_calculator' );

function sally_bmi_calculator(){
	ob_start();
	
	require_once  'includes/tpl.bmi.php';
	
	$rdata = ob_get_contents();
	ob_clean();
	return $rdata;
}

add_shortcode( 'BMR', 'sally_bmr_calculator' );

function sally_bmr_calculator(){
	ob_start();
	
	require_once  'includes/tpl.bmr.php';
	
	$rdata = ob_get_contents();
	ob_clean();
	return $rdata;
}

add_action( 'wp_enqueue_scripts', 'bmi_calculator_script_library' );

function bmi_calculator_script_library() {
	
	if( ! is_admin() ){
	
	wp_register_style( 'bmi-calculator-css',  plugins_url( 'assets/css/bmi.css' , __FILE__ ) );
	wp_enqueue_style( 'bmi-calculator-css' );
	
	}
	
}