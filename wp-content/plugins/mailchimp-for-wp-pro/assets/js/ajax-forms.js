window.mc4wpAjaxForms = (function() {


	/**
	 * @var object Shorthand for global jQuery object
	 */
	var $ = window.jQuery;

	/**
	 * @var object The context in which a form was submitted
	 */
	var $context;

	/**
	 * @var object Custom browser console
	 */
	var console;

	/**
	 * Initializes the MailChimp for WordPress JS functionality
	 */
	function init() {

		// init console
		console = {
			log: function(t) {
				if( window.console ) {
					window.console.log( "MailChimp for WP: " + t );
				}
			}
		};

		// jQuery.ajaxForm is undefined, bail.
		if( $.fn.ajaxForm === undefined ) {
			console.log( "jquery-forms.js is not loaded properly. Not activating AJAX.")
			return;
		}

		// Add AJAX to forms
		$(".mc4wp-ajax form").ajaxForm({
			data        : { action: 'mc4wp_submit_form' },
			dataType    : 'json',
			url         : mc4wp_vars.ajaxurl,
			delegation  : true,
			success     : onAjaxSuccess,
			error       : onAjaxError,
			beforeSubmit: beforeSubmit
		});

	}

	/**
	 * Runs before an AJAX form is submitted
	 * @param data
	 * @param $form
	 */
	function beforeSubmit( data, $form ) {
		var $ajaxLoader, $submitButton;

		$context = $form.parent('div.mc4wp-form');

		// Hide errors from previous sign-up
		$context.find('.mc4wp-alert').hide();
		$context.find("#mc4wp-mailchimp-error").remove();

		// find loader and button in form mark-up
		$ajaxLoader = $context.find('.mc4wp-ajax-loader');
		$submitButton = $form.find('input[type="submit"], button[type="submit"]').first();

		// Disable submit button
		$submitButton.attr( 'disabled', 'disabled' );

		// Insert loader after submit button
		$ajaxLoader.insertAfter( $submitButton );
		$ajaxLoader.show().css('display', 'inline-block');

		// Add small left-margin if loader doesn't have one yet
		if( parseInt( $ajaxLoader.css( 'margin-left' ) ) < 5 ) {
			$ajaxLoader.css('margin-left', '5px');
		}

		// Trigger mc4wp.submit JS event to hook into
		var event = $.Event( "mc4wp.submit" );
		event.form = $context;
		$( document ).trigger( event );
	}

	/**
	 * Runs after every successful AJAX request
	 *
	 * @param response
	 * @param status
	 */
	function onAjaxSuccess( response, status ) {
		var $ajaxLoader;
		$ajaxLoader = $context.find('.mc4wp-ajax-loader');
		$ajaxLoader.hide();

		// Re-enable submit button
		var $submitButton = $context.find('input[type=submit]');
		$submitButton.removeAttr( 'disabled' );

		// Act on response parameters
		if(response.success) {
			onSubscribeSuccess( response.redirect, response.hide_form, response.data );
		} else {
			onSubscribeError( response.error, response.data );
		}

	}

	/**
	 * Runs after every failed AJAX request
	 *
	 * @param response
	 */
	function onAjaxError( response ) {
		console.log(response);
	}

	function onSubscribeSuccess( redirect_url, hide_form, data ) {

		// Find form element
		var $form = $context.find('form');

		// Show success message
		$context.find('div.mc4wp-success-message').show();

		// Reset form to original state
		$form.trigger( 'reset' );

		// Redirect to the specified location
		if( redirect_url && redirect_url != '' ) {
			window.setTimeout(function() {
				window.location.replace( redirect_url );
			}, 2500);

		}

		// Hide the form if the "hide form" option is selected
		if( hide_form ) {
			$form.hide();
		}

		// Trigger mc4wp.success JS event to hook into
		var event = $.Event( "mc4wp.success" );
		event.form = $context;
		event.formData = data;
		$( document ).trigger( event );
	}

	/**
	 * @param error
	 */
	function onSubscribeError( error, data ) {
		var error_type, $message;
		error_type = ( error.type == '' ) ? 'error' : error.type;

		// Show error in console if no MailChimp list is selected
		if( error_type === 'no_lists_selected' ) {
			error_type = 'error';
			console.log( 'You didn\'t select a MailChimp list to subscribe to in the form settings.' );
		}

		// Show error div
		$message = $context.find('.mc4wp-' + error_type + '-message').show();

		// Show MailChimp error to admins
		if( error.show && error_type == 'error' && error.mailchimp_error != '' ) {
			$('<div class="mc4wp-alert mc4wp-notice" id="mc4wp-mailchimp-error"><strong>MailChimp returned this error:</strong><br>' + error.mailchimp_error + '<br><br><em>this message is only visible to administrators</em></div>').insertAfter( $message );
		}

		// Trigger mc4wp.error JS event to hook into
		var event = $.Event( "mc4wp.error" );
		event.form = $context;
		event.formData = data;
		$( document ).trigger( event );
	}

	return {
		init: function() {
			init();
		}
	}

})();

jQuery( document).ready( function() {
	mc4wpAjaxForms.init();
} );