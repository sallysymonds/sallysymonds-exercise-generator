<?php
$taxonomies = WPUltimateRecipe::get()->tags();
unset( $taxonomies['ingredient'] );

$options = '';
foreach( $taxonomies as $taxonomy => $tag_options )
{
    $options .= '<option value="' . $taxonomy . '">';
    $options .= $tag_options['labels']['name'];
    $options .= '</option>';
}
?>
    <div class="wrap">

        <div id="icon-themes" class="icon32"></div>
        <h2>WP Ultimate Recipe <?php _e( 'Import', 'wp-ultimate-recipe' );?></h2>
        <p><?php _e( "It's a good idea to backup your WP database before using the import feature.", 'wp-ultimate-recipe' ); ?></p>
        <form method="POST" action="<?php echo admin_url( 'admin.php' ); ?>" onsubmit="return confirm('ReciPress <?php _e('recipes are about to be imported to', 'wp-ultimate-recipe' ); ?> WP Ultimate Recipe. <?php _e('Are you sure you want to do this?', 'wp-ultimate-recipe' ); ?>');">
            <input type="hidden" name="action" value="import_recipress">
            <?php wp_nonce_field( 'import_recipress', 'import_recipress_nonce', false ); ?>
            <table class="form-table">
                <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e( 'New tag for', 'wp-ultimate-recipe' ); ?> "cuisine"</th>
                    <td>
                        <select id="wpurp_import_cuisine" name="wpurp_import_cuisine">
                            <option value=""><?php _e( 'Select a recipe tag', 'wp-ultimate-recipe' ); ?></option>
                            <?php echo $options; ?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e( 'New tag for', 'wp-ultimate-recipe' ); ?> "course"</th>
                    <td>
                        <select id="wpurp_import_course" name="wpurp_import_course">
                            <option value=""><?php _e( 'Select a recipe tag', 'wp-ultimate-recipe' ); ?></option>
                            <?php echo $options; ?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e( 'New tag for', 'wp-ultimate-recipe' ); ?> "skill level"</th>
                    <td>
                        <select id="wpurp_import_skill_level" name="wpurp_import_skill_level">
                            <option value=""><?php _e( 'Select a recipe tag', 'wp-ultimate-recipe' ); ?></option>
                            <?php echo $options; ?>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
            <?php submit_button( __( 'Import', 'wp-ultimate-recipe' ) . ' ReciPress' ); ?>
        </form>
    </div>