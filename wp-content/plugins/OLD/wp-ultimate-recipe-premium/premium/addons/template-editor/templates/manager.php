<?php
$data = file_get_contents( 'php://input' );
$objData = json_decode( $data );

require_once( '../../../../../../../wp-load.php' );

// Delete template
if( isset( $objData->template ) ) {
    WPUltimateRecipe::addon( 'custom-templates' )->delete_template( $objData->template );
}

// Load templates
$templates = WPUltimateRecipe::addon( 'custom-templates' )->get_templates();

$recipe_default = WPUltimateRecipe::option( 'recipe_template_recipe_template' , 0 );
$print_default = WPUltimateRecipe::option( 'recipe_template_print_template' , 1 );
$grid_default = WPUltimateRecipe::option( 'recipe_template_recipegrid_template' , 2 );

$template_list = array();
foreach( $templates as $index => $template )
{
    $active = array();

    if( $index == $recipe_default ) $active[] = 'Recipe Default';
    if( $index == $print_default ) $active[] = 'Print Default';
    if( $index == $grid_default ) $active[] = 'Recipe Grid Default';

    $template_list[] = array(
        'id' => $index,
        'name' => $template['name'],
        'active' => implode( ', ', $active ),
    );
}

echo json_encode( $template_list );