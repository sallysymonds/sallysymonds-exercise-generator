<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>WP Ultimate Recipe Plugin</title>
    <script>
        document.write('<link rel="stylesheet" type="text/css" href="'+window.opener.wpurp_user_menus.addonUrl + '/css/list-print.css">');
    </script>
</head>
<body onload="setTimeout(function(){window.print()}, 500);">
<div class="wpurp-container">
    <script>
        document.write(window.opener.wpurp_user_menus.shoppingListTitle);
        document.write(window.opener.wpurp_user_menus.shoppingList);
    </script>
</div>
</body>
</html>