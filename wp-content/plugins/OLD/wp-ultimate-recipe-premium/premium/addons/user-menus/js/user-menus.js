var RecipeUserMenus = RecipeUserMenus || {};

RecipeUserMenus.recipes = [];
RecipeUserMenus.order = [];
RecipeUserMenus.recipeIngredients = [];
RecipeUserMenus.nbrRecipes = 0;
RecipeUserMenus.ajaxGettingIngredients = 0;
RecipeUserMenus.generalServings = 4;
RecipeUserMenus.menuId = 0;
RecipeUserMenus.unitSystem = 0

RecipeUserMenus.init = function() {
    RecipeUserMenus.initSelect();

    // Set system if visitors are not allowed to switch
    if(wpurp_user_menus.adjustable_system != '1') {
        RecipeUserMenus.unitSystem = parseInt(wpurp_user_menus.default_system);
    }

    // Set default general servings
    RecipeUserMenus.changeServings(jQuery('.user-menus-servings-general'), true);

    if(typeof wpurp_user_menu !== 'undefined') {
        RecipeUserMenus.setSavedValues(wpurp_user_menu);
        RecipeUserMenus.redrawRecipes();
        RecipeUserMenus.updateIngredients();
    }

    jQuery('.user-menus-group-by').on('click', function(e) {
        e.preventDefault();

        var link = jQuery(this);
        if(!link.hasClass('user-menus-group-by-selected'))
        {
            RecipeUserMenus.groupBy(jQuery(this).data('groupby'));
            link.siblings('.user-menus-group-by-selected').removeClass('user-menus-group-by-selected');
            link.addClass('user-menus-group-by-selected');
        }
    });

    jQuery('.user-menus-selected-recipes').sortable({
        opacity: 0.5,
        start: function(ev, ui) {
            jQuery('.user-menus-recipes-delete-container').show();
            jQuery('.user-menus-recipes-delete').slideDown(500);
        },
        stop: function(ev, ui) {
            jQuery('.user-menus-recipes-delete').slideUp(500);
            jQuery('.user-menus-recipes-delete-container').slideUp(500);
        },
        update: function(ev, ui) {
            RecipeUserMenus.updateRecipeOrder(jQuery(this));
        }
    });

    jQuery('.user-menus-servings-general').on('keyup change', function() {
        RecipeUserMenus.changeServings(jQuery(this), true);
    });

    jQuery('.user-menus-selected-recipes').on('keyup change', '.user-menus-servings-recipe', function() {
        RecipeUserMenus.changeServings(jQuery(this), false);
    });

    jQuery('.user-menus-recipes-delete-container').droppable({
        accept: '.user-menus-recipe',
        hoverClass: 'drop-hover',
        drop: function(ev, ui) {
            ui.draggable.remove();
            RecipeUserMenus.nbrRecipes -= 1;
            RecipeUserMenus.checkIfEmpty();

            RecipeUserMenus.updateRecipeOrder(jQuery('.user-menus-selected-recipes'));
            RecipeUserMenus.updateIngredients();
        }
    });
};

RecipeUserMenus.setSavedValues = function(val)
{
    if(val.recipes !== null) {
        RecipeUserMenus.recipes = val.recipes;
        RecipeUserMenus.order = val.order;
    }

    if(val.nbrRecipes !== '') {
        RecipeUserMenus.nbrRecipes = parseInt(val.nbrRecipes);
    }
    if(val.unitSystem !== '') {
        RecipeUserMenus.unitSystem = parseInt(val.unitSystem);
    }
    if(val.menuId !== '') {
        RecipeUserMenus.menuId = parseInt(val.menuId);
    }
}

RecipeUserMenus.saveMenu = function()
{
    var data = {
        action: 'user_menus_save',
        security: wpurp_user_menus.nonce,
        menuId: RecipeUserMenus.menuId,
        title: jQuery('.user-menus-title').val(),
        recipes: RecipeUserMenus.recipes,
        order: RecipeUserMenus.order,
        nbrRecipes: RecipeUserMenus.nbrRecipes,
        unitSystem: RecipeUserMenus.unitSystem
    };

    jQuery.post(wpurp_user_menus.ajaxurl, data, function(url) {
        if(RecipeUserMenus.menuId == 0) {
            window.location.href = url;
        }
    }, 'html');
}

RecipeUserMenus.printShoppingList = function()
{
    wpurp_user_menus.shoppingListTitle = '<h2>' + jQuery('.user-menus-title').val() + '</h2>';
    wpurp_user_menus.shoppingList = '<table>' + jQuery('.user-menus-ingredients').html() + '</table>';

    window.open(wpurp_user_menus.addonUrl + '/templates/list-print.php');
}

RecipeUserMenus.updateRecipeOrder = function(list)
{
    RecipeUserMenus.order = list.sortable('toArray', { attribute: 'data-index'} );
}


RecipeUserMenus.changeServings = function(input, general) {
    var servings_new = input.val();

    if(isNaN(servings_new) || servings_new <= 0) {
        servings_new = 1;
        input.val(1);
    }

    if(general) {
        RecipeUserMenus.generalServings = servings_new;
    } else {
        var index = input.parent('.user-menus-recipe').data('index');
        RecipeUserMenus.recipes[index].servings_wanted = servings_new;

        RecipeUserMenus.updateIngredientsTable();
    }
}

RecipeUserMenus.initSelect = function() {
    jQuery('.user-menus-select').select2({
        width: 'off'
    }).on('change', function() {
            // Add the selected recipe
            RecipeUserMenus.addRecipe(jQuery(this).select2('data'));

            // Clear the selection
            jQuery(this).select2('val', '');
        });
}

RecipeUserMenus.addRecipe = function(recipe) {
    if(recipe.id !== '')
    {
        RecipeUserMenus.recipes.push({
            id: recipe.id,
            name: recipe.text,
            link: recipe.element[0].dataset.link,
            servings_original: recipe.element[0].dataset.servings,
            servings_wanted: RecipeUserMenus.generalServings
        });

        RecipeUserMenus.order.push((RecipeUserMenus.recipes.length - 1).toString());
        RecipeUserMenus.redrawRecipes();
        RecipeUserMenus.updateIngredients();
    }
}

RecipeUserMenus.redrawRecipes = function() {
    var container = jQuery('.user-menus-selected-recipes');
    container.empty();

    var recipes = RecipeUserMenus.recipes;
    var order = RecipeUserMenus.order;


    RecipeUserMenus.nbrRecipes = 0;
    for(var i = 0, l = order.length; i < l; i++)
    {
        var recipe = recipes[order[i]];
        container.append(
            '<div class="user-menus-recipe" data-recipe="'+recipe.id+'" data-index="'+order[i]+'">' +
                '<a href="'+recipe.link+'" target="_blank">'+recipe.name+'</a>' +
                '<input type="number" class="user-menus-servings-recipe" value="'+recipe.servings_wanted+'">' +
            '</div>');
        RecipeUserMenus.nbrRecipes += 1;
    }

    RecipeUserMenus.checkIfEmpty();
}

RecipeUserMenus.checkIfEmpty = function() {
    if(RecipeUserMenus.nbrRecipes === 0) {
        jQuery('.user-menus-no-recipes').show();
    } else {
        jQuery('.user-menus-no-recipes').hide();
    }
}

RecipeUserMenus.groupBy = function(groupby) {
    var data = {
        action: 'user_menus_groupby',
        security: wpurp_user_menus.nonce,
        groupby: groupby
    };

    jQuery.post(wpurp_user_menus.ajaxurl, data, function(html) {
        jQuery('.user-menus-select').select2('destroy').off().html(html);
        RecipeUserMenus.initSelect();
    });
}

RecipeUserMenus.updateIngredients = function()
{
    var order = RecipeUserMenus.order;
    var recipes = RecipeUserMenus.recipes;
    var ajaxCalls = 0;

    for(var i = 0, l = order.length; i < l; i++)
    {
        var recipe_id = recipes[order[i]].id;

        if(RecipeUserMenus.recipeIngredients[recipe_id] === undefined) {
            ajaxCalls++;

            RecipeUserMenus.recipeIngredients[recipe_id] = [];
            RecipeUserMenus.getIngredients(recipe_id);
        }
    }

    // No need to wait for non-existent ajax calls
    if(ajaxCalls === 0) {
        RecipeUserMenus.updateIngredientsTable();
    }
}

/**
 * Get recipe ingredients through ajax and put in cache
 *
 * @param recipe_id
 */
RecipeUserMenus.getIngredients = function(recipe_id)
{
    var data = {
        action: 'user_menus_get_ingredients',
        security: wpurp_user_menus.nonce,
        recipe_id: recipe_id
    };

    RecipeUserMenus.ajaxGettingIngredients++;

    jQuery.post(wpurp_user_menus.ajaxurl, data, function(ingredients) {
        RecipeUserMenus.ajaxGettingIngredients--;

        RecipeUserMenus.recipeIngredients[recipe_id] = ingredients;

        if(RecipeUserMenus.ajaxGettingIngredients === 0) {
            RecipeUserMenus.updateIngredientsTable();
        }

    }, 'json');
}

RecipeUserMenus.updateIngredientsTable = function()
{
    var ingredient_table = jQuery('table.user-menus-ingredients');
    ingredient_table.find('tbody').empty();

    var recipe_ingredients = RecipeUserMenus.recipeIngredients;
    var order = RecipeUserMenus.order;
    var recipes = RecipeUserMenus.recipes;
    var ingredients = [];

    for(var i = 0, l = order.length; i < l; i++)
    {
        var recipe_id = recipes[order[i]].id;
        var servings = recipes[order[i]].servings_wanted / parseFloat(recipes[order[i]].servings_original);

        for(var j = 0, m = recipe_ingredients[recipe_id].length; j < m; j++)
        {
            var ingredient = recipe_ingredients[recipe_id][j];
            var name = ingredient.ingredient;
            var group = ingredient.group;

            if(ingredients[group] === undefined) {
                ingredients[group] = [];
            }

            if(ingredients[group][name] === undefined) {
                ingredients[group][name] = [];
            }

            var amount = ingredient.amount_normalized * servings;

            var unit = ingredient.unit;
            var type = RecipeUnitConversion.getUnitFromAlias(unit);

            if(type !== undefined) {
                var quantity = new Qty('' + amount + ' ' + type);
                var base_quantity = quantity.toBase();

                if(base_quantity.units() == 'm3') {
                    base_quantity = base_quantity.to('l');
                }

                unit = base_quantity.units();
                amount = base_quantity.scalar;

            }

            if(unit == '') {
                unit = 'wpurp_nounit';
            }

            if(ingredients[group][name][unit] === undefined) {
                ingredients[group][name][unit] = 0.0;
            }

            ingredients[group][name][unit] += parseFloat(amount);
        }
    }

    // Sort ingredients by name
    var group_keys = Object.keys(ingredients);
    group_keys.sort(function (a, b) { // Case insensitive sort
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });

    for(i = 0, l = group_keys.length; i < l; i++)
    {
        var group_key = group_keys[i];
        var group = ingredients[group_key];

        var group_row = jQuery('<tr><td colspan="2"><strong>'+group_key+'</strong></td></tr>');
        ingredient_table.append(group_row);

        var ingredient_keys = Object.keys(group);
        ingredient_keys.sort(function (a, b) { // Case insensitive sort
            return a.toLowerCase().localeCompare(b.toLowerCase());
        });

        for(j = 0, m = ingredient_keys.length; j < m; j++)
        {
            var ingredient = ingredient_keys[j];

            var units = group[ingredient];
            for(var unit in units)
            {
                var amount = units[unit];
                var actual_unit = RecipeUnitConversion.getUnitFromAlias(unit);


                if(unit == 'wpurp_nounit') {
                    unit = '';
                } else if(actual_unit !== undefined){
                    var quantity = RecipeUnitConversion.convertUnitToSystem(amount, actual_unit, 0, RecipeUserMenus.unitSystem);
                    amount = quantity.amount;
                    unit = RecipeUnitConversion.getUserAbbreviation(quantity.unit, amount);
                }

                amount = RecipeUnitConversion.formatNumber(amount, false);

                var ingredient_name = ingredient.charAt(0).toUpperCase() + ingredient.slice(1);
                var ingredient_row = jQuery('<tr><td>'+ingredient_name+'</td><td>'+amount+' '+ unit +'</td></tr>');
                ingredient_table.append(ingredient_row);
            }
        }
    }
}

RecipeUserMenus.changeUnits = function(dropdown)
{
    RecipeUserMenus.unitSystem = parseInt(jQuery(dropdown).val());
    RecipeUserMenus.updateIngredientsTable();
}

jQuery(document).ready(function(){
    if(jQuery('.wpurp-user-menus').length > 0) {
        RecipeUserMenus.init();
    }
});