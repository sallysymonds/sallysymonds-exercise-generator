var IngredientGroups = IngredientGroups || {};

jQuery(document).ready(function() {
    IngredientGroups.init();
});

IngredientGroups.init = function() {
    IngredientGroups.addSortable();

    jQuery('#newGroup').keyup(function (e) {
        if (e.keyCode == 13) {
            IngredientGroups.addNewGroup(jQuery(this).val());
        }
    });
}

IngredientGroups.addGroup = function(submit) {
    var input = jQuery(submit).siblings('#newGroup');

    IngredientGroups.addNewGroup(input.val());
}

IngredientGroups.addNewGroup = function(group) {
    var group = group.trim();
    if(group.length > 0) {
        jQuery('.ingredient-groups').append('<div class="ingredient-group-container"><h3>'+group+'</h3><ul class="ingredient-group" data-name="'+group+'"></ul></div>')
        IngredientGroups.addSortable();
    }

    jQuery('#newGroup').val('');
}

IngredientGroups.addSortable = function() {
    jQuery('.ingredient-group').sortable({
        connectWith: '.ingredient-group',
        update: function(ev, ui) {
            // Only if we're changing groups
            if(ui.sender !== null) {
                var ingredient = jQuery(ui.item);

                var slug = ingredient.data('slug');
                var group = ingredient.parent('.ingredient-group').data('name');

                IngredientGroups.update(slug, group);
            }
        }
    });
}

IngredientGroups.update = function(slug, group) {
    var data = {
        action: 'ingredient_groups_save',
        security: wpurp_ingredient_groups.nonce,
        slug: slug,
        group: group
    };

    jQuery.post(wpurp_ingredient_groups.ajaxurl, data, function(data) {
        // Nothing to do.
    });
}