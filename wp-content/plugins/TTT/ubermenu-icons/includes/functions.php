<?php

add_action( 'uberMenu_load_dependents' , 'umicons_init' , 20 );
function umicons_init(){
	
	global $uberMenu;
	if( !$uberMenu ) return;	//In case Icons was activated before UberMenu, just quit

	$settings = $uberMenu->getSettings();

	//register icons
	add_action( 'umicons_register_icons' , 'umicons_register_default_icons' );
	do_action( 'umicons_register_icons' );

	$icon_ops = 'icon_ops';
	$settings->registerPanel( $icon_ops, 'Icon Settings' );	


	//TOP LEVEL

	$settings->addSubHeader( $icon_ops, 'umicons-top-level' , __( 'Top Level'  , 'ubermenu' ) );


	//Icon Color
	$settings->addColorPicker( $icon_ops , 
			'umicons-color' , 
			__( 'Top Level Icon Color' , 'ubermenu' ),
			__( 'Defaults to inherited text color' , 'ubermenu' ) , 
			false
			);


	//Icon Hover Color
	$settings->addColorPicker( $icon_ops , 
			'umicons-color-hover' , 
			__( 'Top Level Icon Color [Hover]' , 'ubermenu' ),
			__( 'Defaults to inherited text color' , 'ubermenu' ) , 
			false
			);

	//Icon Size
	$settings->addTextInput( $icon_ops,
			'umicons-font-size',
			__( 'Top Level Icon Font Size' , 'ubermenu' ),
			'Defaults to same size as text when left blank. Example: 40px',
			'',
			'spark-minitext'

			);

	//Icon Position
	$settings->addSelect( $icon_ops,
			'umicons-position',		
			__( 'Top Level Icon Position', 'ubermenu' ),
			__( 'Should the icon be to the left of the text or above the text?' , 'ubermenu' ),
			array(
				'left'	=>	__( 'Left of Text', 'ubermenu' ),
				'top'	=>	__( 'Above Text', 'ubermenu' ),
			),
			'left'
			);

	

	//Vertical Padding
	$settings->addTextInput( $icon_ops,
			'umicons-padding-vertical',
			__( 'Top Level Vertical Padding' , 'ubermenu' ),
			__( 'Only valid for "Above Text" icon position.  Example: 20px', 'ubermenu' ),
			'20px',
			'spark-minitext'

			);

	//Horizontal Padding
	$settings->addTextInput( $icon_ops,
			'umicons-padding-horizontal',
			__( 'Top Level Item Horizontal Padding' , 'ubermenu' ),
			__( 'Only valid for "Above Text" icon position.  Example: 20px', 'ubermenu' ),
			'20px',
			'spark-minitext'
			);

	




	//SECOND LEVEL

	$settings->addSubHeader( $icon_ops, 'umicons-submenu-headers' , __( 'Submenu Headers (2nd Level)' , 'ubermenu' ) );


	//Icon Color - Submenu Headers
	$settings->addColorPicker( $icon_ops , 
			'umicons-color-2' , 
			__( 'Submenu Header Icon Color' , 'ubermenu' ),
			__( 'Defaults to inherited text color' , 'ubermenu' ) , 
			false
			);

	//Icon Hover Color - Submenu Headers
	$settings->addColorPicker( $icon_ops , 
			'umicons-color-hover-2' , 
			__( 'Submenu Header Icon Color [Hover]' , 'ubermenu' ),
			__( 'Defaults to inherited text color' , 'ubermenu' ) , 
			false
			);


	//Icon Size - Submenu Headers
	$settings->addTextInput( $icon_ops,
			'umicons-font-size-2',
			__( 'Submenu Header Icon Font Size' , 'ubermenu' ),
			__( 'Defaults to same size as text when left blank. Example: 40px', 'ubermenu' ),
			'',
			'spark-minitext'

			);

	//Icon Position - Submenu Headers
	$settings->addSelect( $icon_ops,
			'umicons-position-2',		
			__( 'Submenu Header Icon Position', 'ubermenu' ),
			__( 'Should the icon be to the left of the text or above the text?' , 'ubermenu' ),
			array(
				'left'	=>	__( 'Left of Text', 'ubermenu' ),
				'top'	=>	__( 'Above Text', 'ubermenu' ),
			),
			'left'
			);

	

	//Vertical Padding - Submenu Headers
	$settings->addTextInput( $icon_ops,
			'umicons-padding-vertical-2',
			__( 'Submenu Header Vertical Padding' , 'ubermenu' ),
			__( 'Only valid for "Above Text" icon position.  Example: 20px', 'ubermenu' ),
			'20px',
			'spark-minitext'

			);

	//Horizontal Padding - Submenu Headers
	$settings->addTextInput( $icon_ops,
			'umicons-padding-horizontal-2',
			__( 'Submenu Header Horizontal Padding' , 'ubermenu' ),
			__( 'Only valid for "Above Text" icon position.  Example: 20px', 'ubermenu' ),
			'20px',
			'spark-minitext'
			);





	//THIRD LEVEL

	$settings->addSubHeader( $icon_ops, 'umicons-links-3' , __( 'Submenu Column Links (3rd Level)' , 'ubermenu' ) );


	//Icon Color - Submenu Links
	$settings->addColorPicker( $icon_ops , 
			'umicons-color-3' , 
			__( 'Submenu Links Icon Color' , 'ubermenu' ),
			__( 'Defaults to inherited text color' , 'ubermenu' ) , 
			false
			);

	//Icon Hover Color - Submenu Links
	$settings->addColorPicker( $icon_ops , 
			'umicons-color-hover-3' , 
			__( 'Submenu Links  Icon Color [Hover]' , 'ubermenu' ),
			__( 'Defaults to inherited text color' , 'ubermenu' ) , 
			false
			);


	//Icon Size - Submenu Links
	$settings->addTextInput( $icon_ops,
			'umicons-font-size-3',
			__( 'Submenu Links Icon Font Size' , 'ubermenu' ),
			__( 'Defaults to same size as text when left blank. Example: 40px', 'ubermenu' ),
			'',
			'spark-minitext'

			);

	//Icon Position - Submenu Links
	$settings->addSelect( $icon_ops,
			'umicons-position-3',		
			__( 'Submenu Links Icon Position', 'ubermenu' ),
			__( 'Should the icon be to the left of the text or above the text?' , 'ubermenu' ),
			array(
				'left'	=>	__( 'Left of Text', 'ubermenu' ),
				'top'	=>	__( 'Above Text', 'ubermenu' ),
			),
			'left'
			);

	

	//Vertical Padding - Submenu Links
	$settings->addTextInput( $icon_ops,
			'umicons-padding-vertical-3',
			__( 'Submenu Links Vertical Padding' , 'ubermenu' ),
			__( 'Only valid for "Above Text" icon position.  Example: 20px', 'ubermenu' ),
			'20px',
			'spark-minitext'

			);

	//Horizontal Padding - Submenu Links
	$settings->addTextInput( $icon_ops,
			'umicons-padding-horizontal-3',
			__( 'Submenu Links Horizontal Padding' , 'ubermenu' ),
			__( 'Only valid for "Above Text" icon position.  Example: 20px', 'ubermenu' ),
			'20px',
			'spark-minitext'
			);






	//ADVANCED SETTINGS
	
	$settings->addSubHeader( $icon_ops, 'umicons-advanced' , __( 'Advanced Settings' , 'ubermenu' ) );

	$settings->addCheckbox( $icon_ops , 
			'umicons-load-fontawesome' , 
			__( 'Load Font Awesome Stylesheet' , 'ubermenu' ),
			__( 'You may wish to disable this if you already have Font Awesome included by your theme', 'ubermenu' ),
			'on'
			);

	$settings->addCheckbox( $icon_ops , 
			'umicons-convert-v3' , 
			__( 'Convert Font Awesome Version 3 Classes' , 'ubermenu' ),
			__( 'This will automatically convert your old saved Font Awesome v3 icon classes to v4 icon classes on the fly.  This is a mildly expensive operation, so once you resave your menu to update the icon classes permanently, you should disable this.', 'ubermenu' ),
			'off'
			);


	if( !is_admin() ){
		add_filter( 'uberMenu_insertCSS' , 'umicons_insertCSS' );
	}

	if( $settings->op( 'umicons-convert-v3' ) ) add_filter( 'ubermenu-icon-class' , 'umicons_convert_icon_class' );
	
}

function umicons_load_admin_resources(){
	$assets = UM_ICONS_PLUGIN_URL . 'assets/';
	wp_enqueue_style( 'ubermenu-icons-style' , $assets.'css/ubermenu-icons.css' );
	wp_enqueue_style( 'ubermenu-fontawesome' , $assets.'fontawesome/css/font-awesome.min.css' );
	wp_enqueue_script( 'ubermenu-icons' , $assets.'js/ubermenu-icons.js' );
}
add_action( 'admin_print_styles-nav-menus.php' , 'umicons_load_admin_resources' );

function umicons_load_resources(){
	global $uberMenu;
	$settings = $uberMenu->getSettings();

	$assets = UM_ICONS_PLUGIN_URL . 'assets/';

	if( $settings->op( 'umicons-load-fontawesome' ) ){
		wp_enqueue_style( 'ubermenu-fontawesome' , $assets.'fontawesome/css/font-awesome.min.css' );
	}
}
add_action( 'wp_enqueue_scripts' , 'umicons_load_resources' );


function umicons_insertCSS( $css ){

	global $uberMenu;
	$settings = $uberMenu->getSettings();


	$css.="\n/** UberMenu Icons **/\n";


	//TOP LEVEL

	$iconStyles = '';
	$hoverStyles = '';

	$fontSize = $settings->op( 'umicons-font-size' );	
	if( $fontSize != '' ){
		$iconStyles.= ' font-size: '.$fontSize.'; ';
	}

	$fontColor = $settings->op( 'umicons-color' );
	if( $fontColor != '' ){
		if( strpos( $fontColor , '#' ) === false ) $fontColor = '#' . $fontColor;
		$iconStyles.= ' color: '.$fontColor.'; ';
	}

	$fontColorHover = $settings->op( 'umicons-color-hover' );
	if( $fontColorHover != '' ){
		if( strpos( $fontColorHover , '#' ) === false ) $fontColorHover = '#' . $fontColorHover;
		$hoverStyles.= ' color: '.$fontColorHover.'; ';
	}

	$paddingVertical = $settings->op( 'umicons-padding-vertical' );
	$paddingHorizontal = $settings->op( 'umicons-padding-horizontal' );

	if( $settings->op( 'umicons-position' ) == 'top' ){
		$iconStyles.= " display:block; text-align:center; margin:0 auto; padding-bottom:3px;";
		$css.= "#megaMenu ul.megaMenu > li.menu-item.ss-nav-menu-with-icon > * > span.wpmega-link-title, \n#megaMenu ul.megaMenu > li.menu-item.ss-nav-menu-with-icon > * > span.wpmega-item-description{ text-align:center; } \n";

		$css.= "
#megaMenu.megaMenuHorizontal ul.megaMenu > li.menu-item > a, 
#megaMenu.megaMenuHorizontal ul.megaMenu > li.menu-item > span.um-anchoremulator{
	padding-top:$paddingVertical;
	padding-bottom:$paddingVertical;
	padding-right:$paddingHorizontal !important;
	padding-left:$paddingHorizontal;
}
#megaMenu.megaMenuHorizontal ul.megaMenu > li.menu-item.mega-with-sub.ss-nav-menu-with-icon > a:after, 
#megaMenu.megaMenuHorizontal ul.megaMenu > li.menu-item.ss-nav-menu-mega.ss-nav-menu-with-icon > a:after, 
#megaMenu.megaMenuHorizontal ul.megaMenu > li.menu-item.mega-with-sub.ss-nav-menu-with-icon > span.um-anchoremulator:after, 
#megaMenu.megaMenuHorizontal ul.megaMenu > li.menu-item.ss-nav-menu-mega.ss-nav-menu-with-icon > span.um-anchoremulator:after{
	top:auto;
	bottom:10px;
	right:auto !important;
	left:50%;
	margin-left:-3px !important;
}\n";
	}

	if( $iconStyles !== '' ){
		$css.= "#megaMenu ul.megaMenu > li.menu-item > a i.fa, #megaMenu ul.megaMenu > li.menu-item > span.um-anchoremulator i.fa{ ".$iconStyles." } \n";
	}
	if( trim( $hoverStyles ) !== '' ){
		$css.= "
#megaMenu ul.megaMenu > li.menu-item > a:hover i.fa, 
#megaMenu ul.megaMenu > li.menu-item:hover > a i.fa, 
#megaMenu ul.megaMenu > li.menu-item.megaHover > a i.fa, 
#megaMenu ul.megaMenu > li.menu-item > span.um-anchoremulator:hover i.fa,
#megaMenu ul.megaMenu > li.menu-item:hover > span.um-anchoremulator i.fa,
#megaMenu ul.megaMenu > li.menu-item.megaHover > span.um-anchoremulator i.fa{ ".$hoverStyles." } \n";
	}




	//SUBMENU HEADERS
	
	$iconStyles = '';
	$hoverStyles = '';

	$fontSize = $settings->op( 'umicons-font-size-2' );	
	if( $fontSize != '' ){
		$iconStyles.= ' font-size: '.$fontSize.'; ';
	}

	$fontColor = $settings->op( 'umicons-color-2' );
	if( $fontColor != '' ){
		if( strpos( $fontColor , '#' ) === false ) $fontColor = '#' . $fontColor;
		$iconStyles.= ' color: '.$fontColor.'; ';
	}

	$fontColorHover = $settings->op( 'umicons-color-hover-2' );
	if( $fontColorHover != '' ){
		if( strpos( $fontColorHover , '#' ) === false ) $fontColorHover = '#' . $fontColorHover;
		$hoverStyles.= ' color: '.$fontColorHover.'; ';
	}

	$paddingVertical = $settings->op( 'umicons-padding-vertical-2' );
	$paddingHorizontal = $settings->op( 'umicons-padding-horizontal-2' );

	if( $settings->op( 'umicons-position-2' ) == 'top' ){
		$iconStyles.= " display:block; text-align:center; margin:0 auto; padding-bottom:10px; ";
		$css.= "#megaMenu ul.megaMenu ul.sub-menu-1 > li.menu-item.ss-nav-menu-with-icon > * > span.wpmega-link-title, \n#megaMenu ul.megaMenu ul.sub-menu-1 > li.menu-item.ss-nav-menu-with-icon > * > span.wpmega-item-description{ text-align:center; } \n";

		$css.= "
#megaMenu.megaMenuHorizontal ul.megaMenu ul.sub-menu-1 > li.menu-item > a, 
#megaMenu.megaMenuHorizontal ul.megaMenu ul.sub-menu-1 > li.menu-item > span.um-anchoremulator{
	padding-top:$paddingVertical;
	padding-bottom:$paddingVertical !important;
	padding-right:$paddingHorizontal;
	padding-left:$paddingHorizontal;
}
\n";
	}

	if( $iconStyles !== '' ){
		$css.= "#megaMenu ul.sub-menu-1 > li.menu-item > a i.fa, #megaMenu ul.sub-menu-1 > li.menu-item > span.um-anchoremulator i.fa{ ".$iconStyles." } \n";
	}
	if( $hoverStyles !== '' ){
		$css.= "
#megaMenu ul.sub-menu-1 > li.menu-item > a:hover i.fa, 
#megaMenu ul.sub-menu-1 > li.menu-item:hover > a i.fa, 
#megaMenu ul.sub-menu-1 > li.menu-item.megaHover > a i.fa, 
#megaMenu ul.sub-menu-1 > li.menu-item > span.um-anchoremulator:hover i.fa,
#megaMenu ul.sub-menu-1 > li.menu-item:hover > span.um-anchoremulator i.fa,
#megaMenu ul.sub-menu-1 > li.menu-item.megaHover > span.um-anchoremulator i.fa{ ".$hoverStyles." } \n";
	}





	//SUBMENU LINKS
	
	$iconStyles = '';
	$hoverStyles = '';

	$fontSize = $settings->op( 'umicons-font-size-3' );	
	if( $fontSize != '' ){
		$iconStyles.= ' font-size: '.$fontSize.'; ';
	}

	$fontColor = $settings->op( 'umicons-color-3' );
	if( $fontColor != '' ){
		if( strpos( $fontColor , '#' ) === false ) $fontColor = '#' . $fontColor;
		$iconStyles.= ' color: '.$fontColor.'; ';
	}

	$fontColorHover = $settings->op( 'umicons-color-hover-3' );
	if( $fontColorHover != '' ){
		if( strpos( $fontColorHover , '#' ) === false ) $fontColorHover = '#' . $fontColorHover;
		$hoverStyles.= ' color: '.$fontColorHover.'; ';
	}

	$paddingVertical = $settings->op( 'umicons-padding-vertical-3' );
	$paddingHorizontal = $settings->op( 'umicons-padding-horizontal-3' );

	if( $settings->op( 'umicons-position-3' ) == 'top' ){
		$iconStyles.= " display:block; text-align:center; margin:0 auto; padding-bottom:10px; ";
		$css.= "#megaMenu ul.megaMenu ul.sub-menu-2 li.menu-item.ss-nav-menu-with-icon > * > span.wpmega-link-title, \n#megaMenu ul.megaMenu ul.sub-menu-2 li.menu-item.ss-nav-menu-with-icon > * > span.wpmega-item-description{ text-align:center; } \n";

		$css.= "
#megaMenu.megaMenuHorizontal ul.megaMenu ul.sub-menu-2 li.menu-item > a, 
#megaMenu.megaMenuHorizontal ul.megaMenu ul.sub-menu-2 li.menu-item > span.um-anchoremulator{
	padding-top:$paddingVertical;
	padding-bottom:$paddingVertical;
	padding-right:$paddingHorizontal;
	padding-left:$paddingHorizontal;
}
\n";
	}

	if( $iconStyles !== '' ){
		$css.= "#megaMenu ul.sub-menu-2 > li.menu-item > a i.fa, #megaMenu ul.sub-menu-2 > li.menu-item > span.um-anchoremulator i.fa{ ".$iconStyles." } \n";
	}
		if( $hoverStyles !== '' ){
		$css.= "
#megaMenu ul.sub-menu-2 li.menu-item > a:hover i.fa, 
#megaMenu ul.sub-menu-2 li.menu-item:hover > a i.fa, 
#megaMenu ul.sub-menu-2 li.menu-item.megaHover > a i.fa, 
#megaMenu ul.sub-menu-2 li.menu-item > span.um-anchoremulator:hover i.fa,
#megaMenu ul.sub-menu-2 li.menu-item:hover > span.um-anchoremulator i.fa,
#megaMenu ul.sub-menu-2 li.menu-item.megaHover > span.um-anchoremulator i.fa{ ".$hoverStyles." } \n";
	}


	//No text, image only, center vertically
	$css.= "
#megaMenu ul.megaMenu li.menu-item.ss-nav-menu-notext > * > i{
	display:block;
}";

	return $css;

}

function umicons_custom_fields( $item_id , $menu ){

	remove_action( 'ubermenu_extended_menu_item_options' , 'ubermenu_icon_class_option', 10, 2 );

	/*
	$menu->showCustomMenuOption(
		'icon', 
		$item_id, 
		array(
			'level' => '0-plus', 
			'title' => __( 'Select the icon to display with this menu item' , 'ubermenu' ), 
			'label' => __( 'Icon' , 'ubermenu' ), 			
			'type' => 'select',
			'ops' => umicons_get_icon_select()
		)
	);
	*/

	$id 	= 'icon';
	$level 	= '0-plus';
	$title 	= __( 'Select the icon to display with this menu item' , 'ubermenu' );
	$label 	= __( 'Icon' , 'ubermenu' );
	$ops 	= umicons_get_icon_select();

	$_val = $menu->getMenuItemOption( $item_id , $id );
	
	global $uberMenu;
	$settings = $uberMenu->getSettings();

	$desc = '<span class="ss-desc">'.$label.'<span class="ss-info-container">?<span class="ss-info">'.$title.'</span></span></span>';

	?>
			<p class="field-description description description-wide wpmega-custom wpmega-l<?php echo $level;?> wpmega-<?php echo $id;?>">
				<label for="edit-menu-item-<?php echo $id;?>-<?php echo $item_id;?>">
					<?php echo $desc;
						//$_val = get_post_meta($item_id, '_menu_item_' . $id, true);
						if( empty($_val) ) $_val = $default;
						?>
						<select 
							id="edit-menu-item-<?php echo $id; ?>-<?php echo $item_id; ?>"
							class="edit-menu-item-<?php echo $id; ?>"
							name="menu-item-<?php echo $id;?>[<?php echo $item_id;?>]">
							<?php foreach( $ops as $opval => $icon ): ?>
								<?php $optitle = $icon['title']; $v3_val = isset( $icon['v3'] ) ? $icon['v3'] : ''; ?>
								<option value="<?php echo $opval; ?>" <?php 
									if( $_val == $opval || ( $v3_val != '' && $v3_val == $_val ) ) echo 'selected="selected"'; ?> ><?php echo $optitle; ?></option>
							<?php endforeach; ?>
						</select>
				</label>
			</p>
	<?php	

}
add_action( 'ubermenu_extended_menu_item_options' , 'umicons_custom_fields' , 9 , 2 );



$umicons_backconvert;
function umicons_convert_icon_class( $class ){
	global $umicons_backconvert;
	if( !is_array( $umicons_backconvert ) ) $umicons_backconvert = array();
	if( count( $umicons_backconvert ) == 0 ){
		$icons = umicons_get_icons();
		foreach( $icons as $icon_group => $group ){
			$iconmap = $group['iconmap'];
			$prefix = isset( $group['class_prefix'] ) ? $group['class_prefix'] : '';
			foreach( $iconmap as $icon_class => $icon ){
				$umicons_backconvert[$icon['v3']] = $prefix.$icon_class;
			}
		}
	}

	switch( $class ){
		case 'icon-expand-alt':
			return 'fa fa-expand-o';
		case 'icon-collapse-alt':
			return 'fa fa-collapse-o';
		case 'icon-star-half-empty':
			return 'fa fa-star-half-o';
		default:
			return isset( $umicons_backconvert[$class] ) ? $umicons_backconvert[$class] : $class;
	}
}




function umicons_get_icon_select(){

	$icons = umicons_get_icons();

	$icon_select = array( '' => array( 'title' => 'None' ) );

	foreach( $icons as $icon_group => $group ){

		$iconmap = $group['iconmap'];
		$prefix = isset( $group['class_prefix'] ) ? $group['class_prefix'] : '';

		foreach( $iconmap as $icon_class => $icon ){

			$icon_select[$prefix.$icon_class] = $icon; //$icon['title']; //ucfirst( str_replace( '-' , ' ' , str_replace( 'icon-' , '' , $icon_class ) ) );

		}

	}

	return apply_filters( 'umicons_icon_select' , $icon_select );

}


function umicons_get_icons(){

	$umi = UM_ICONS();
	return $umi->get_registered_icons();

}

function umicons_register_icons( $group , $iconmap ){

	$umi = UM_ICONS();
	$umi->register_icons( $group, $iconmap );

}

function umicons_deregister_icons( $group ){

	$umi = UM_ICONS();
	$umi->deregister_icons( $group );

}

function umicons_register_default_icons(){
	umicons_register_icons( 'font-awesome' , array(
		'title' => 'Font Awesome',
		'class_prefix' => 'fa ',
		'iconmap' => umicons_fa_icons() 
	));
}

function umicons_fa_icons(){

	$icons = array(
		'fa-glass'	=>	array(
			'title'	=>	'Glass',
			'v3'	=>	'icon-glass',
		),
		'fa-music'	=>	array(
			'title'	=>	'Music',
			'v3'	=>	'icon-music',
		),
		'fa-search'	=>	array(
			'title'	=>	'Search',
			'v3'	=>	'icon-search',
		),
		'fa-envelope-o'	=>	array(
			'title'	=>	'Envelope (Outline)',
			'v3'	=>	'icon-envelope',
		),
		'fa-heart'	=>	array(
			'title'	=>	'Heart',
			'v3'	=>	'icon-heart',
		),
		'fa-star'	=>	array(
			'title'	=>	'Star',
			'v3'	=>	'icon-star',
		),
		'fa-star-o'	=>	array(
			'title'	=>	'Star (Outline)',
			'v3'	=>	'icon-star-empty',
		),
		'fa-user'	=>	array(
			'title'	=>	'User',
			'v3'	=>	'icon-user',
		),
		'fa-film'	=>	array(
			'title'	=>	'Film',
			'v3'	=>	'icon-film',
		),
		'fa-th-large'	=>	array(
			'title'	=>	'TH Large',
			'v3'	=>	'icon-th-large',
		),
		'fa-th'	=>	array(
			'title'	=>	'TH',
			'v3'	=>	'icon-th',
		),
		'fa-th-list'	=>	array(
			'title'	=>	'th-list',
			'v3'	=>	'icon-th-list',
		),
		'fa-check'	=>	array(
			'title'	=>	'Checkmark',
			'v3'	=>	'icon-ok',
		),
		'fa-times'	=>	array(
			'title'	=>	'Times',
			'v3'	=>	'icon-remove',
		),
		'fa-search-plus'	=>	array(
			'title'	=>	'Search Plus (Zoom In)',
			'v3'	=>	'icon-zoom-in',
		),
		'fa-search-minus'	=>	array(
			'title'	=>	'Search Minus (Zoom Out)',
			'v3'	=>	'icon-zoom-out',
		),
		'fa-power-off'	=>	array(
			'title'	=>	'Power Off',
			'v3'	=>	'icon-off',
		),
		'fa-signal'	=>	array(
			'title'	=>	'Signal',
			'v3'	=>	'icon-signal',
		),
		'fa-cog'	=>	array(
			'title'	=>	'Cog',
			'v3'	=>	'icon-cog',
		),
		'fa-trash-o'	=>	array(
			'title'	=>	'Trash (Outline)',
			'v3'	=>	'icon-trash',
		),
		'fa-home'	=>	array(
			'title'	=>	'Home',
			'v3'	=>	'icon-home',
		),
		'fa-file-o'	=>	array(
			'title'	=>	'File (Outline)',
			'v3'	=>	'icon-file',
		),
		'fa-clock-o'	=>	array(
			'title'	=>	'Clock (Outline)',
			'v3'	=>	'icon-time',
		),
		'fa-road'	=>	array(
			'title'	=>	'Road',
			'v3'	=>	'icon-road',
		),
		'fa-download'	=>	array(
			'title'	=>	'Download',
			'v3'	=>	'icon-download-alt',
		),
		'fa-arrow-circle-o-down'	=>	array(
			'title'	=>	'Arrow (Circle/Outline/Down)',
			'v3'	=>	'icon-download',
		),
		'fa-arrow-circle-o-up'	=>	array(
			'title'	=>	'Arrow (Circle/Outline/Up)',
			'v3'	=>	'icon-upload',
		),
		'fa-inbox'	=>	array(
			'title'	=>	'Inbox',
			'v3'	=>	'icon-inbox',
		),
		'fa-play-circle-o'	=>	array(
			'title'	=>	'Play (Circle/Outline)',
			'v3'	=>	'icon-play-circle',
		),
		'fa-repeat'	=>	array(
			'title'	=>	'Repeat',
			'v3'	=>	'icon-repeat',
		),
		'fa-refresh'	=>	array(
			'title'	=>	'Refresh',
			'v3'	=>	'icon-refresh',
		),
		'fa-list-alt'	=>	array(
			'title'	=>	'List (Alternative)',
			'v3'	=>	'icon-list-alt',
		),
		'fa-lock'	=>	array(
			'title'	=>	'Lock',
			'v3'	=>	'icon-lock',
		),
		'fa-flag'	=>	array(
			'title'	=>	'Flag',
			'v3'	=>	'icon-flag',
		),
		'fa-headphones'	=>	array(
			'title'	=>	'Headphones',
			'v3'	=>	'icon-headphones',
		),
		'fa-volume-off'	=>	array(
			'title'	=>	'Volume Off',
			'v3'	=>	'icon-volume-off',
		),
		'fa-volume-down'	=>	array(
			'title'	=>	'Volume Down',
			'v3'	=>	'icon-volume-down',
		),
		'fa-volume-up'	=>	array(
			'title'	=>	'Volume Up',
			'v3'	=>	'icon-volume-up',
		),
		'fa-qrcode'	=>	array(
			'title'	=>	'QR Code',
			'v3'	=>	'icon-qrcode',
		),
		'fa-barcode'	=>	array(
			'title'	=>	'Barcode',
			'v3'	=>	'icon-barcode',
		),
		'fa-tag'	=>	array(
			'title'	=>	'Tag',
			'v3'	=>	'icon-tag',
		),
		'fa-tags'	=>	array(
			'title'	=>	'tags',
			'v3'	=>	'icon-tags',
		),
		'fa-book'	=>	array(
			'title'	=>	'Book',
			'v3'	=>	'icon-book',
		),
		'fa-bookmark'	=>	array(
			'title'	=>	'Bookmark',
			'v3'	=>	'icon-bookmark',
		),
		'fa-print'	=>	array(
			'title'	=>	'Print',
			'v3'	=>	'icon-print',
		),
		'fa-camera'	=>	array(
			'title'	=>	'Camera',
			'v3'	=>	'icon-camera',
		),
		'fa-font'	=>	array(
			'title'	=>	'Font',
			'v3'	=>	'icon-font',
		),
		'fa-bold'	=>	array(
			'title'	=>	'Bold',
			'v3'	=>	'icon-bold',
		),
		'fa-italic'	=>	array(
			'title'	=>	'Italic',
			'v3'	=>	'icon-italic',
		),
		'fa-text-height'	=>	array(
			'title'	=>	'Text Height',
			'v3'	=>	'icon-text-height',
		),
		'fa-text-width'	=>	array(
			'title'	=>	'Text Width',
			'v3'	=>	'icon-text-width',
		),
		'fa-align-left'	=>	array(
			'title'	=>	'Align Left',
			'v3'	=>	'icon-align-left',
		),
		'fa-align-center'	=>	array(
			'title'	=>	'Align Center',
			'v3'	=>	'icon-align-center',
		),
		'fa-align-right'	=>	array(
			'title'	=>	'Align Right',
			'v3'	=>	'icon-align-right',
		),
		'fa-align-justify'	=>	array(
			'title'	=>	'Align Justify',
			'v3'	=>	'icon-align-justify',
		),
		'fa-list'	=>	array(
			'title'	=>	'List',
			'v3'	=>	'icon-list',
		),
		'fa-outdent'	=>	array(
			'title'	=>	'Outdent',
			'v3'	=>	'icon-indent-left',
		),
		'fa-indent'	=>	array(
			'title'	=>	'Indent',
			'v3'	=>	'icon-indent-right',
		),
		'fa-video-camera'	=>	array(
			'title'	=>	'video-camera',
			'v3'	=>	'icon-facetime-video',
		),
		'fa-picture-o'	=>	array(
			'title'	=>	'Picture (Outline)',
			'v3'	=>	'icon-picture',
		),
		'fa-pencil'	=>	array(
			'title'	=>	'Pencil',
			'v3'	=>	'icon-pencil',
		),
		'fa-map-marker'	=>	array(
			'title'	=>	'Map Marker',
			'v3'	=>	'icon-map-marker',
		),
		'fa-adjust'	=>	array(
			'title'	=>	'Adjust',
			'v3'	=>	'icon-adjust',
		),
		'fa-tint'	=>	array(
			'title'	=>	'Tint',
			'v3'	=>	'icon-tint',
		),
		'fa-pencil-square-o'	=>	array(
			'title'	=>	'Pencil (Square/Outline)',
			'v3'	=>	'icon-edit',
		),
		'fa-share-square-o'	=>	array(
			'title'	=>	'Share (Square/Outline)',
			'v3'	=>	'icon-share',
		),
		'fa-check-square-o'	=>	array(
			'title'	=>	'Check (Square/Outline)',
			'v3'	=>	'icon-check',
		),
		'fa-arrows'	=>	array(
			'title'	=>	'Arrows',
			'v3'	=>	'icon-move',
		),
		'fa-step-backward'	=>	array(
			'title'	=>	'Step Backward',
			'v3'	=>	'icon-step-backward',
		),
		'fa-fast-backward'	=>	array(
			'title'	=>	'Fast Backward',
			'v3'	=>	'icon-fast-backward',
		),
		'fa-backward'	=>	array(
			'title'	=>	'Backward',
			'v3'	=>	'icon-backward',
		),
		'fa-play'	=>	array(
			'title'	=>	'Play',
			'v3'	=>	'icon-play',
		),
		'fa-pause'	=>	array(
			'title'	=>	'Pause',
			'v3'	=>	'icon-pause',
		),
		'fa-stop'	=>	array(
			'title'	=>	'Stop',
			'v3'	=>	'icon-stop',
		),
		'fa-forward'	=>	array(
			'title'	=>	'Forward',
			'v3'	=>	'icon-forward',
		),
		'fa-fast-forward'	=>	array(
			'title'	=>	'Fast Forward',
			'v3'	=>	'icon-fast-forward',
		),
		'fa-step-forward'	=>	array(
			'title'	=>	'Step Forward',
			'v3'	=>	'icon-step-forward',
		),
		'fa-eject'	=>	array(
			'title'	=>	'Eject',
			'v3'	=>	'icon-eject',
		),
		'fa-chevron-left'	=>	array(
			'title'	=>	'Chevron Left',
			'v3'	=>	'icon-chevron-left',
		),
		'fa-chevron-right'	=>	array(
			'title'	=>	'Chevron Right',
			'v3'	=>	'icon-chevron-right',
		),
		'fa-plus-circle'	=>	array(
			'title'	=>	'Plus (Circle)',
			'v3'	=>	'icon-plus-sign',
		),
		'fa-minus-circle'	=>	array(
			'title'	=>	'Minus (Circle)',
			'v3'	=>	'icon-minus-sign',
		),
		'fa-times-circle'	=>	array(
			'title'	=>	'Times (Circle)',
			'v3'	=>	'icon-remove-sign',
		),
		'fa-check-circle'	=>	array(
			'title'	=>	'Check (Circle)',
			'v3'	=>	'icon-ok-sign',
		),
		'fa-question-circle'	=>	array(
			'title'	=>	'Question (Circle)',
			'v3'	=>	'icon-question-sign',
		),
		'fa-info-circle'	=>	array(
			'title'	=>	'Info (Circle)',
			'v3'	=>	'icon-info-sign',
		),
		'fa-crosshairs'	=>	array(
			'title'	=>	'Crosshairs',
			'v3'	=>	'icon-screenshot',
		),
		'fa-times-circle-o'	=>	array(
			'title'	=>	'Times (Circle/Outline)',
			'v3'	=>	'icon-remove-circle',
		),
		'fa-check-circle-o'	=>	array(
			'title'	=>	'Check (Circle/Outline)',
			'v3'	=>	'icon-ok-circle',
		),
		'fa-ban'	=>	array(
			'title'	=>	'Ban',
			'v3'	=>	'icon-ban-circle',
		),
		'fa-arrow-left'	=>	array(
			'title'	=>	'Arrow Left',
			'v3'	=>	'icon-arrow-left',
		),
		'fa-arrow-right'	=>	array(
			'title'	=>	'Arrow Right',
			'v3'	=>	'icon-arrow-right',
		),
		'fa-arrow-up'	=>	array(
			'title'	=>	'Arrow Up',
			'v3'	=>	'icon-arrow-up',
		),
		'fa-arrow-down'	=>	array(
			'title'	=>	'Arrow Down',
			'v3'	=>	'icon-arrow-down',
		),
		'fa-share'	=>	array(
			'title'	=>	'Share',
			'v3'	=>	'icon-share-alt',
		),
		'fa-expand'	=>	array(
			'title'	=>	'Expand',
			'v3'	=>	'icon-resize-full',
		),
		'fa-compress'	=>	array(
			'title'	=>	'Compress',
			'v3'	=>	'icon-resize-small',
		),
		'fa-plus'	=>	array(
			'title'	=>	'Plus',
			'v3'	=>	'icon-plus',
		),
		'fa-minus'	=>	array(
			'title'	=>	'Minus',
			'v3'	=>	'icon-minus',
		),
		'fa-asterisk'	=>	array(
			'title'	=>	'Asterisk',
			'v3'	=>	'icon-asterisk',
		),
		'fa-exclamation-circle'	=>	array(
			'title'	=>	'Exclamation Circle',
			'v3'	=>	'icon-exclamation-sign',
		),
		'fa-gift'	=>	array(
			'title'	=>	'Gift',
			'v3'	=>	'icon-gift',
		),
		'fa-leaf'	=>	array(
			'title'	=>	'Leaf',
			'v3'	=>	'icon-leaf',
		),
		'fa-fire'	=>	array(
			'title'	=>	'Fire',
			'v3'	=>	'icon-fire',
		),
		'fa-eye'	=>	array(
			'title'	=>	'Eye',
			'v3'	=>	'icon-eye-open',
		),
		'fa-eye-slash'	=>	array(
			'title'	=>	'Eye Slash',
			'v3'	=>	'icon-eye-close',
		),
		'fa-exclamation-triangle'	=>	array(
			'title'	=>	'Exclamation Triangle',
			'v3'	=>	'icon-warning-sign',
		),
		'fa-plane'	=>	array(
			'title'	=>	'Plane',
			'v3'	=>	'icon-plane',
		),
		'fa-calendar'	=>	array(
			'title'	=>	'Calendar',
			'v3'	=>	'icon-calendar',
		),
		'fa-random'	=>	array(
			'title'	=>	'Random',
			'v3'	=>	'icon-random',
		),
		'fa-comment'	=>	array(
			'title'	=>	'Comment',
			'v3'	=>	'icon-comment',
		),
		'fa-magnet'	=>	array(
			'title'	=>	'Magnet',
			'v3'	=>	'icon-magnet',
		),
		'fa-chevron-up'	=>	array(
			'title'	=>	'Chevron Up',
			'v3'	=>	'icon-chevron-up',
		),
		'fa-chevron-down'	=>	array(
			'title'	=>	'Chevron Down',
			'v3'	=>	'icon-chevron-down',
		),
		'fa-retweet'	=>	array(
			'title'	=>	'Retweet',
			'v3'	=>	'icon-retweet',
		),
		'fa-shopping-cart'	=>	array(
			'title'	=>	'Shopping Cart',
			'v3'	=>	'icon-shopping-cart',
		),
		'fa-folder'	=>	array(
			'title'	=>	'Folder',
			'v3'	=>	'icon-folder-close',
		),
		'fa-folder-open'	=>	array(
			'title'	=>	'Folder Open',
			'v3'	=>	'icon-folder-open',
		),
		'fa-arrows-v'	=>	array(
			'title'	=>	'Arrows V',
			'v3'	=>	'icon-resize-vertical',
		),
		'fa-arrows-h'	=>	array(
			'title'	=>	'Arrows H',
			'v3'	=>	'icon-resize-horizontal',
		),
		'fa-bar-chart-o'	=>	array(
			'title'	=>	'Bar Chart O',
			'v3'	=>	'icon-bar-chart',
		),
		'fa-twitter-square'	=>	array(
			'title'	=>	'Twitter Square',
			'v3'	=>	'icon-twitter-sign',
		),
		'fa-facebook-square'	=>	array(
			'title'	=>	'Facebook Square',
			'v3'	=>	'icon-facebook-sign',
		),
		'fa-camera-retro'	=>	array(
			'title'	=>	'Camera Retro',
			'v3'	=>	'icon-camera-retro',
		),
		'fa-key'	=>	array(
			'title'	=>	'Key',
			'v3'	=>	'icon-key',
		),
		'fa-cogs'	=>	array(
			'title'	=>	'Cogs',
			'v3'	=>	'icon-cogs',
		),
		'fa-comments'	=>	array(
			'title'	=>	'Comments',
			'v3'	=>	'icon-comments',
		),
		'fa-thumbs-o-up'	=>	array(
			'title'	=>	'Thumbs O Up',
			'v3'	=>	'icon-thumbs-up',
		),
		'fa-thumbs-o-down'	=>	array(
			'title'	=>	'Thumbs O Down',
			'v3'	=>	'icon-thumbs-down',
		),
		'fa-star-half'	=>	array(
			'title'	=>	'Star Half',
			'v3'	=>	'icon-star-half',
		),
		'fa-heart-o'	=>	array(
			'title'	=>	'Heart O',
			'v3'	=>	'icon-heart-empty',
		),
		'fa-sign-out'	=>	array(
			'title'	=>	'Sign Out',
			'v3'	=>	'icon-signout',
		),
		'fa-linkedin-square'	=>	array(
			'title'	=>	'Linkedin Square',
			'v3'	=>	'icon-linkedin-sign',
		),
		'fa-thumb-tack'	=>	array(
			'title'	=>	'Thumb Tack',
			'v3'	=>	'icon-pushpin',
		),
		'fa-external-link'	=>	array(
			'title'	=>	'External Link',
			'v3'	=>	'icon-external-link',
		),
		'fa-sign-in'	=>	array(
			'title'	=>	'Sign In',
			'v3'	=>	'icon-signin',
		),
		'fa-trophy'	=>	array(
			'title'	=>	'Trophy',
			'v3'	=>	'icon-trophy',
		),
		'fa-github-square'	=>	array(
			'title'	=>	'Github Square',
			'v3'	=>	'icon-github-sign',
		),
		'fa-upload'	=>	array(
			'title'	=>	'Upload',
			'v3'	=>	'icon-upload-alt',
		),
		'fa-lemon-o'	=>	array(
			'title'	=>	'Lemon O',
			'v3'	=>	'icon-lemon',
		),
		'fa-phone'	=>	array(
			'title'	=>	'Phone',
			'v3'	=>	'icon-phone',
		),
		'fa-square-o'	=>	array(
			'title'	=>	'Square O',
			'v3'	=>	'icon-check-empty',
		),
		'fa-bookmark-o'	=>	array(
			'title'	=>	'Bookmark O',
			'v3'	=>	'icon-bookmark-empty',
		),
		'fa-phone-square'	=>	array(
			'title'	=>	'Phone Square',
			'v3'	=>	'icon-phone-sign',
		),
		'fa-twitter'	=>	array(
			'title'	=>	'Twitter',
			'v3'	=>	'icon-twitter',
		),
		'fa-facebook'	=>	array(
			'title'	=>	'Facebook',
			'v3'	=>	'icon-facebook',
		),
		'fa-github'	=>	array(
			'title'	=>	'Github',
			'v3'	=>	'icon-github',
		),
		'fa-unlock'	=>	array(
			'title'	=>	'Unlock',
			'v3'	=>	'icon-unlock',
		),
		'fa-credit-card'	=>	array(
			'title'	=>	'Credit Card',
			'v3'	=>	'icon-credit-card',
		),
		'fa-rss'	=>	array(
			'title'	=>	'Rss',
			'v3'	=>	'icon-rss',
		),
		'fa-hdd-o'	=>	array(
			'title'	=>	'Hdd O',
			'v3'	=>	'icon-hdd',
		),
		'fa-bullhorn'	=>	array(
			'title'	=>	'Bullhorn',
			'v3'	=>	'icon-bullhorn',
		),
		'fa-bell'	=>	array(
			'title'	=>	'Bell',
			'v3'	=>	'icon-bell',
		),
		'fa-certificate'	=>	array(
			'title'	=>	'Certificate',
			'v3'	=>	'icon-certificate',
		),
		'fa-hand-o-right'	=>	array(
			'title'	=>	'Hand O Right',
			'v3'	=>	'icon-hand-right',
		),
		'fa-hand-o-left'	=>	array(
			'title'	=>	'Hand O Left',
			'v3'	=>	'icon-hand-left',
		),
		'fa-hand-o-up'	=>	array(
			'title'	=>	'Hand O Up',
			'v3'	=>	'icon-hand-up',
		),
		'fa-hand-o-down'	=>	array(
			'title'	=>	'Hand O Down',
			'v3'	=>	'icon-hand-down',
		),
		'fa-arrow-circle-left'	=>	array(
			'title'	=>	'Arrow Circle Left',
			'v3'	=>	'icon-circle-arrow-left',
		),
		'fa-arrow-circle-right'	=>	array(
			'title'	=>	'Arrow Circle Right',
			'v3'	=>	'icon-circle-arrow-right',
		),
		'fa-arrow-circle-up'	=>	array(
			'title'	=>	'Arrow Circle Up',
			'v3'	=>	'icon-circle-arrow-up',
		),
		'fa-arrow-circle-down'	=>	array(
			'title'	=>	'Arrow Circle Down',
			'v3'	=>	'icon-circle-arrow-down',
		),
		'fa-globe'	=>	array(
			'title'	=>	'Globe',
			'v3'	=>	'icon-globe',
		),
		'fa-wrench'	=>	array(
			'title'	=>	'Wrench',
			'v3'	=>	'icon-wrench',
		),
		'fa-tasks'	=>	array(
			'title'	=>	'Tasks',
			'v3'	=>	'icon-tasks',
		),
		'fa-filter'	=>	array(
			'title'	=>	'Filter',
			'v3'	=>	'icon-filter',
		),
		'fa-briefcase'	=>	array(
			'title'	=>	'Briefcase',
			'v3'	=>	'icon-briefcase',
		),
		'fa-arrows-alt'	=>	array(
			'title'	=>	'Arrows Alt',
			'v3'	=>	'icon-fullscreen',
		),
		'fa-users'	=>	array(
			'title'	=>	'Users',
			'v3'	=>	'icon-group',
		),
		'fa-link'	=>	array(
			'title'	=>	'Link',
			'v3'	=>	'icon-link',
		),
		'fa-cloud'	=>	array(
			'title'	=>	'Cloud',
			'v3'	=>	'icon-cloud',
		),
		'fa-flask'	=>	array(
			'title'	=>	'Flask',
			'v3'	=>	'icon-beaker',
		),
		'fa-scissors'	=>	array(
			'title'	=>	'Scissors',
			'v3'	=>	'icon-cut',
		),
		'fa-files-o'	=>	array(
			'title'	=>	'Files O',
			'v3'	=>	'icon-copy',
		),
		'fa-paperclip'	=>	array(
			'title'	=>	'Paperclip',
			'v3'	=>	'icon-paper-clip',
		),
		'fa-floppy-o'	=>	array(
			'title'	=>	'Floppy O',
			'v3'	=>	'icon-save',
		),
		'fa-square'	=>	array(
			'title'	=>	'Square',
			'v3'	=>	'icon-sign-blank',
		),
		'fa-bars'	=>	array(
			'title'	=>	'Bars',
			'v3'	=>	'icon-reorder',
		),
		'fa-list-ul'	=>	array(
			'title'	=>	'List Ul',
			'v3'	=>	'icon-list-ul',
		),
		'fa-list-ol'	=>	array(
			'title'	=>	'List Ol',
			'v3'	=>	'icon-list-ol',
		),
		'fa-strikethrough'	=>	array(
			'title'	=>	'Strikethrough',
			'v3'	=>	'icon-strikethrough',
		),
		'fa-underline'	=>	array(
			'title'	=>	'Underline',
			'v3'	=>	'icon-underline',
		),
		'fa-table'	=>	array(
			'title'	=>	'Table',
			'v3'	=>	'icon-table',
		),
		'fa-magic'	=>	array(
			'title'	=>	'Magic',
			'v3'	=>	'icon-magic',
		),
		'fa-truck'	=>	array(
			'title'	=>	'Truck',
			'v3'	=>	'icon-truck',
		),
		'fa-pinterest'	=>	array(
			'title'	=>	'Pinterest',
			'v3'	=>	'icon-pinterest',
		),
		'fa-pinterest-square'	=>	array(
			'title'	=>	'Pinterest Square',
			'v3'	=>	'icon-pinterest-sign',
		),
		'fa-google-plus-square'	=>	array(
			'title'	=>	'Google Plus Square',
			'v3'	=>	'icon-google-plus-sign',
		),
		'fa-google-plus'	=>	array(
			'title'	=>	'Google Plus',
			'v3'	=>	'icon-google-plus',
		),
		'fa-money'	=>	array(
			'title'	=>	'Money',
			'v3'	=>	'icon-money',
		),
		'fa-caret-down'	=>	array(
			'title'	=>	'Caret Down',
			'v3'	=>	'icon-caret-down',
		),
		'fa-caret-up'	=>	array(
			'title'	=>	'Caret Up',
			'v3'	=>	'icon-caret-up',
		),
		'fa-caret-left'	=>	array(
			'title'	=>	'Caret Left',
			'v3'	=>	'icon-caret-left',
		),
		'fa-caret-right'	=>	array(
			'title'	=>	'Caret Right',
			'v3'	=>	'icon-caret-right',
		),
		'fa-columns'	=>	array(
			'title'	=>	'Columns',
			'v3'	=>	'icon-columns',
		),
		'fa-sort'	=>	array(
			'title'	=>	'Sort',
			'v3'	=>	'icon-sort',
		),
		'fa-sort-asc'	=>	array(
			'title'	=>	'Sort Asc',
			'v3'	=>	'icon-sort-down',
		),
		'fa-sort-desc'	=>	array(
			'title'	=>	'Sort Desc',
			'v3'	=>	'icon-sort-up',
		),
		'fa-envelope'	=>	array(
			'title'	=>	'Envelope',
			'v3'	=>	'icon-envelope-alt',
		),
		'fa-linkedin'	=>	array(
			'title'	=>	'Linkedin',
			'v3'	=>	'icon-linkedin',
		),
		'fa-undo'	=>	array(
			'title'	=>	'Undo',
			'v3'	=>	'icon-undo',
		),
		'fa-gavel'	=>	array(
			'title'	=>	'Gavel',
			'v3'	=>	'icon-legal',
		),
		'fa-tachometer'	=>	array(
			'title'	=>	'Tachometer',
			'v3'	=>	'icon-dashboard',
		),
		'fa-comment-o'	=>	array(
			'title'	=>	'Comment O',
			'v3'	=>	'icon-comment-alt',
		),
		'fa-comments-o'	=>	array(
			'title'	=>	'Comments O',
			'v3'	=>	'icon-comments-alt',
		),
		'fa-bolt'	=>	array(
			'title'	=>	'Bolt',
			'v3'	=>	'icon-bolt',
		),
		'fa-sitemap'	=>	array(
			'title'	=>	'Sitemap',
			'v3'	=>	'icon-sitemap',
		),
		'fa-umbrella'	=>	array(
			'title'	=>	'Umbrella',
			'v3'	=>	'icon-umbrella',
		),
		'fa-clipboard'	=>	array(
			'title'	=>	'Clipboard',
			'v3'	=>	'icon-paste',
		),
		'fa-lightbulb-o'	=>	array(
			'title'	=>	'Lightbulb O',
			'v3'	=>	'icon-lightbulb',
		),
		'fa-exchange'	=>	array(
			'title'	=>	'Exchange',
			'v3'	=>	'icon-exchange',
		),
		'fa-cloud-download'	=>	array(
			'title'	=>	'Cloud Download',
			'v3'	=>	'icon-cloud-download',
		),
		'fa-cloud-upload'	=>	array(
			'title'	=>	'Cloud Upload',
			'v3'	=>	'icon-cloud-upload',
		),
		'fa-user-md'	=>	array(
			'title'	=>	'User Md',
			'v3'	=>	'icon-user-md',
		),
		'fa-stethoscope'	=>	array(
			'title'	=>	'Stethoscope',
			'v3'	=>	'icon-stethoscope',
		),
		'fa-suitcase'	=>	array(
			'title'	=>	'Suitcase',
			'v3'	=>	'icon-suitcase',
		),
		'fa-bell-o'	=>	array(
			'title'	=>	'Bell O',
			'v3'	=>	'icon-bell-alt',
		),
		'fa-coffee'	=>	array(
			'title'	=>	'Coffee',
			'v3'	=>	'icon-coffee',
		),
		'fa-cutlery'	=>	array(
			'title'	=>	'Cutlery',
			'v3'	=>	'icon-food',
		),
		'fa-file-text-o'	=>	array(
			'title'	=>	'File Text O',
			'v3'	=>	'icon-file-alt',
		),
		'fa-building-o'	=>	array(
			'title'	=>	'Building O',
			'v3'	=>	'icon-building',
		),
		'fa-hospital-o'	=>	array(
			'title'	=>	'Hospital O',
			'v3'	=>	'icon-hospital',
		),
		'fa-ambulance'	=>	array(
			'title'	=>	'Ambulance',
			'v3'	=>	'icon-ambulance',
		),
		'fa-medkit'	=>	array(
			'title'	=>	'Medkit',
			'v3'	=>	'icon-medkit',
		),
		'fa-fighter-jet'	=>	array(
			'title'	=>	'Fighter Jet',
			'v3'	=>	'icon-fighter-jet',
		),
		'fa-beer'	=>	array(
			'title'	=>	'Beer',
			'v3'	=>	'icon-beer',
		),
		'fa-h-square'	=>	array(
			'title'	=>	'H Square',
			'v3'	=>	'icon-h-sign',
		),
		'fa-plus-square'	=>	array(
			'title'	=>	'Plus Square',
			'v3'	=>	'icon-plus-sign-alt',
		),
		'fa-angle-double-left'	=>	array(
			'title'	=>	'Angle Double Left',
			'v3'	=>	'icon-double-angle-left',
		),
		'fa-angle-double-right'	=>	array(
			'title'	=>	'Angle Double Right',
			'v3'	=>	'icon-double-angle-right',
		),
		'fa-angle-double-up'	=>	array(
			'title'	=>	'Angle Double Up',
			'v3'	=>	'icon-double-angle-up',
		),
		'fa-angle-double-down'	=>	array(
			'title'	=>	'Angle Double Down',
			'v3'	=>	'icon-double-angle-down',
		),
		'fa-angle-left'	=>	array(
			'title'	=>	'Angle Left',
			'v3'	=>	'icon-angle-left',
		),
		'fa-angle-right'	=>	array(
			'title'	=>	'Angle Right',
			'v3'	=>	'icon-angle-right',
		),
		'fa-angle-up'	=>	array(
			'title'	=>	'Angle Up',
			'v3'	=>	'icon-angle-up',
		),
		'fa-angle-down'	=>	array(
			'title'	=>	'Angle Down',
			'v3'	=>	'icon-angle-down',
		),
		'fa-desktop'	=>	array(
			'title'	=>	'Desktop',
			'v3'	=>	'icon-desktop',
		),
		'fa-laptop'	=>	array(
			'title'	=>	'Laptop',
			'v3'	=>	'icon-laptop',
		),
		'fa-tablet'	=>	array(
			'title'	=>	'Tablet',
			'v3'	=>	'icon-tablet',
		),
		'fa-mobile'	=>	array(
			'title'	=>	'Mobile',
			'v3'	=>	'icon-mobile-phone',
		),
		'fa-circle-o'	=>	array(
			'title'	=>	'Circle O',
			'v3'	=>	'icon-circle-blank',
		),
		'fa-quote-left'	=>	array(
			'title'	=>	'Quote Left',
			'v3'	=>	'icon-quote-left',
		),
		'fa-quote-right'	=>	array(
			'title'	=>	'Quote Right',
			'v3'	=>	'icon-quote-right',
		),
		'fa-spinner'	=>	array(
			'title'	=>	'Spinner',
			'v3'	=>	'icon-spinner',
		),
		'fa-circle'	=>	array(
			'title'	=>	'Circle',
			'v3'	=>	'icon-circle',
		),
		'fa-reply'	=>	array(
			'title'	=>	'Reply',
			'v3'	=>	'icon-reply',
		),
		'fa-github-alt'	=>	array(
			'title'	=>	'Github Alt',
			'v3'	=>	'icon-github-alt',
		),
		'fa-folder-o'	=>	array(
			'title'	=>	'Folder O',
			'v3'	=>	'icon-folder-close-alt',
		),
		'fa-folder-open-o'	=>	array(
			'title'	=>	'Folder Open O',
			'v3'	=>	'icon-folder-open-alt',
		),
		'fa-smile-o'	=>	array(
			'title'	=>	'Smile O',
			'v3'	=>	'icon-smile',
		),
		'fa-frown-o'	=>	array(
			'title'	=>	'Frown O',
			'v3'	=>	'icon-frown',
		),
		'fa-meh-o'	=>	array(
			'title'	=>	'Meh O',
			'v3'	=>	'icon-meh',
		),
		'fa-gamepad'	=>	array(
			'title'	=>	'Gamepad',
			'v3'	=>	'icon-gamepad',
		),
		'fa-keyboard-o'	=>	array(
			'title'	=>	'Keyboard O',
			'v3'	=>	'icon-keyboard',
		),
		'fa-flag-o'	=>	array(
			'title'	=>	'Flag O',
			'v3'	=>	'icon-flag-alt',
		),
		'fa-flag-checkered'	=>	array(
			'title'	=>	'Flag Checkered',
			'v3'	=>	'icon-flag-checkered',
		),
		'fa-terminal'	=>	array(
			'title'	=>	'Terminal',
			'v3'	=>	'icon-terminal',
		),
		'fa-code'	=>	array(
			'title'	=>	'Code',
			'v3'	=>	'icon-code',
		),
		'fa-reply-all'	=>	array(
			'title'	=>	'Reply All',
			'v3'	=>	'icon-reply-all',
		),
		'fa-mail-reply-all'	=>	array(
			'title'	=>	'Mail Reply All',
			'v3'	=>	'icon-mail-reply-all',
		),
		'fa-star-half-o'	=>	array(
			'title'	=>	'Star Half O',
			'v3'	=>	'icon-star-half-full',
		),
		'fa-location-arrow'	=>	array(
			'title'	=>	'Location Arrow',
			'v3'	=>	'icon-location-arrow',
		),
		'fa-crop'	=>	array(
			'title'	=>	'Crop',
			'v3'	=>	'icon-crop',
		),
		'fa-code-fork'	=>	array(
			'title'	=>	'Code Fork',
			'v3'	=>	'icon-code-fork',
		),
		'fa-chain-broken'	=>	array(
			'title'	=>	'Chain Broken',
			'v3'	=>	'icon-unlink',
		),
		'fa-question'	=>	array(
			'title'	=>	'Question',
			'v3'	=>	'icon-question',
		),
		'fa-info'	=>	array(
			'title'	=>	'Info',
			'v3'	=>	'icon-info',
		),
		'fa-exclamation'	=>	array(
			'title'	=>	'Exclamation',
			'v3'	=>	'icon-exclamation',
		),
		'fa-superscript'	=>	array(
			'title'	=>	'Superscript',
			'v3'	=>	'icon-superscript',
		),
		'fa-subscript'	=>	array(
			'title'	=>	'Subscript',
			'v3'	=>	'icon-subscript',
		),
		'fa-eraser'	=>	array(
			'title'	=>	'Eraser',
			'v3'	=>	'icon-eraser',
		),
		'fa-puzzle-piece'	=>	array(
			'title'	=>	'Puzzle Piece',
			'v3'	=>	'icon-puzzle-piece',
		),
		'fa-microphone'	=>	array(
			'title'	=>	'Microphone',
			'v3'	=>	'icon-microphone',
		),
		'fa-microphone-slash'	=>	array(
			'title'	=>	'Microphone Slash',
			'v3'	=>	'icon-microphone-off',
		),
		'fa-shield'	=>	array(
			'title'	=>	'Shield',
			'v3'	=>	'icon-shield',
		),
		'fa-calendar-o'	=>	array(
			'title'	=>	'Calendar O',
			'v3'	=>	'icon-calendar-empty',
		),
		'fa-fire-extinguisher'	=>	array(
			'title'	=>	'Fire Extinguisher',
			'v3'	=>	'icon-fire-extinguisher',
		),
		'fa-rocket'	=>	array(
			'title'	=>	'Rocket',
			'v3'	=>	'icon-rocket',
		),
		'fa-maxcdn'	=>	array(
			'title'	=>	'Maxcdn',
			'v3'	=>	'icon-maxcdn',
		),
		'fa-chevron-circle-left'	=>	array(
			'title'	=>	'Chevron Circle Left',
			'v3'	=>	'icon-chevron-sign-left',
		),
		'fa-chevron-circle-right'	=>	array(
			'title'	=>	'Chevron Circle Right',
			'v3'	=>	'icon-chevron-sign-right',
		),
		'fa-chevron-circle-up'	=>	array(
			'title'	=>	'Chevron Circle Up',
			'v3'	=>	'icon-chevron-sign-up',
		),
		'fa-chevron-circle-down'	=>	array(
			'title'	=>	'Chevron Circle Down',
			'v3'	=>	'icon-chevron-sign-down',
		),
		'fa-html5'	=>	array(
			'title'	=>	'Html5',
			'v3'	=>	'icon-html5',
		),
		'fa-css3'	=>	array(
			'title'	=>	'Css3',
			'v3'	=>	'icon-css3',
		),
		'fa-anchor'	=>	array(
			'title'	=>	'Anchor',
			'v3'	=>	'icon-anchor',
		),
		'fa-unlock-alt'	=>	array(
			'title'	=>	'Unlock Alt',
			'v3'	=>	'icon-unlock-alt',
		),
		'fa-bullseye'	=>	array(
			'title'	=>	'Bullseye',
			'v3'	=>	'icon-bullseye',
		),
		'fa-ellipsis-h'	=>	array(
			'title'	=>	'Ellipsis H',
			'v3'	=>	'icon-ellipsis-horizontal',
		),
		'fa-ellipsis-v'	=>	array(
			'title'	=>	'Ellipsis V',
			'v3'	=>	'icon-ellipsis-vertical',
		),
		'fa-rss-square'	=>	array(
			'title'	=>	'Rss Square',
			'v3'	=>	'icon-rss-sign',
		),
		'fa-play-circle'	=>	array(
			'title'	=>	'Play Circle',
			'v3'	=>	'icon-play-sign',
		),
		'fa-ticket'	=>	array(
			'title'	=>	'Ticket',
			'v3'	=>	'icon-ticket',
		),
		'fa-minus-square'	=>	array(
			'title'	=>	'Minus Square',
			'v3'	=>	'icon-minus-sign-alt',
		),
		'fa-minus-square-o'	=>	array(
			'title'	=>	'Minus Square O',
			'v3'	=>	'icon-check-minus',
		),
		'fa-level-up'	=>	array(
			'title'	=>	'Level Up',
			'v3'	=>	'icon-level-up',
		),
		'fa-level-down'	=>	array(
			'title'	=>	'Level Down',
			'v3'	=>	'icon-level-down',
		),
		'fa-check-square'	=>	array(
			'title'	=>	'Check Square',
			'v3'	=>	'icon-check-sign',
		),
		'fa-pencil-square'	=>	array(
			'title'	=>	'Pencil Square',
			'v3'	=>	'icon-edit-sign',
		),
		'fa-external-link-square'	=>	array(
			'title'	=>	'External Link Square',
			'v3'	=>	'icon-external-link-sign',
		),
		'fa-share-square'	=>	array(
			'title'	=>	'Share Square',
			'v3'	=>	'icon-share-sign',
		),
		'fa-compass'	=>	array(
			'title'	=>	'Compass',
			'v3'	=>	'icon-compass',
		),
		'fa-caret-square-o-down'	=>	array(
			'title'	=>	'Caret Square O Down',
			'v3'	=>	'icon-collapse',
		),
		'fa-caret-square-o-up'	=>	array(
			'title'	=>	'Caret Square O Up',
			'v3'	=>	'icon-collapse-top',
		),
		'fa-caret-square-o-right'	=>	array(
			'title'	=>	'Caret Square O Right',
			'v3'	=>	'icon-expand',
		),
		'fa-eur'	=>	array(
			'title'	=>	'Eur',
			'v3'	=>	'icon-eur',
		),
		'fa-gbp'	=>	array(
			'title'	=>	'Gbp',
			'v3'	=>	'icon-gbp',
		),
		'fa-usd'	=>	array(
			'title'	=>	'Usd',
			'v3'	=>	'icon-usd',
		),
		'fa-inr'	=>	array(
			'title'	=>	'Inr',
			'v3'	=>	'icon-inr',
		),
		'fa-jpy'	=>	array(
			'title'	=>	'Jpy',
			'v3'	=>	'icon-jpy',
		),
		'fa-rub'	=>	array(
			'title'	=>	'Rub',
			'v3'	=>	'icon-cny',
		),
		'fa-krw'	=>	array(
			'title'	=>	'Krw',
			'v3'	=>	'icon-krw',
		),
		'fa-btc'	=>	array(
			'title'	=>	'Btc',
			'v3'	=>	'icon-btc',
		),
		'fa-file'	=>	array(
			'title'	=>	'File',
			'v3'	=>	'icon-file',
		),
		'fa-file-text'	=>	array(
			'title'	=>	'File Text',
			'v3'	=>	'icon-file-text',
		),
		'fa-sort-alpha-asc'	=>	array(
			'title'	=>	'Sort Alpha Asc',
			'v3'	=>	'icon-sort-by-alphabet',
		),
		'fa-sort-alpha-desc'	=>	array(
			'title'	=>	'Sort Alpha Desc',
			'v3'	=>	'icon-sort-by-alphabet-alt',
		),
		'fa-sort-amount-asc'	=>	array(
			'title'	=>	'Sort Amount Asc',
			'v3'	=>	'icon-sort-by-attributes',
		),
		'fa-sort-amount-desc'	=>	array(
			'title'	=>	'Sort Amount Desc',
			'v3'	=>	'icon-sort-by-attributes-alt',
		),
		'fa-sort-numeric-asc'	=>	array(
			'title'	=>	'Sort Numeric Asc',
			'v3'	=>	'icon-sort-by-order',
		),
		'fa-sort-numeric-desc'	=>	array(
			'title'	=>	'Sort Numeric Desc',
			'v3'	=>	'icon-sort-by-order-alt',
		),
		'fa-thumbs-up'	=>	array(
			'title'	=>	'Thumbs Up',
			'v3'	=>	'icon-thumbs-up',
		),
		'fa-thumbs-down'	=>	array(
			'title'	=>	'Thumbs Down',
			'v3'	=>	'icon-thumbs-down',
		),
		'fa-youtube-square'	=>	array(
			'title'	=>	'Youtube Square',
			'v3'	=>	'icon-youtube-sign',
		),
		'fa-youtube'	=>	array(
			'title'	=>	'Youtube',
			'v3'	=>	'icon-youtube',
		),
		'fa-xing'	=>	array(
			'title'	=>	'Xing',
			'v3'	=>	'icon-xing',
		),
		'fa-xing-square'	=>	array(
			'title'	=>	'Xing Square',
			'v3'	=>	'icon-xing-sign',
		),
		'fa-youtube-play'	=>	array(
			'title'	=>	'Youtube Play',
			'v3'	=>	'icon-youtube-play',
		),
		'fa-dropbox'	=>	array(
			'title'	=>	'Dropbox',
			'v3'	=>	'icon-dropbox',
		),
		'fa-stack-overflow'	=>	array(
			'title'	=>	'Stack Overflow',
			'v3'	=>	'icon-stackexchange',
		),
		'fa-instagram'	=>	array(
			'title'	=>	'Instagram',
			'v3'	=>	'icon-instagram',
		),
		'fa-flickr'	=>	array(
			'title'	=>	'Flickr',
			'v3'	=>	'icon-flickr',
		),
		'fa-adn'	=>	array(
			'title'	=>	'Adn',
			'v3'	=>	'icon-adn',
		),
		'fa-bitbucket'	=>	array(
			'title'	=>	'Bitbucket',
			'v3'	=>	'icon-bitbucket',
		),
		'fa-bitbucket-square'	=>	array(
			'title'	=>	'Bitbucket Square',
			'v3'	=>	'icon-bitbucket-sign',
		),
		'fa-tumblr'	=>	array(
			'title'	=>	'Tumblr',
			'v3'	=>	'icon-tumblr',
		),
		'fa-tumblr-square'	=>	array(
			'title'	=>	'Tumblr Square',
			'v3'	=>	'icon-tumblr-sign',
		),
		'fa-long-arrow-down'	=>	array(
			'title'	=>	'Long Arrow Down',
			'v3'	=>	'icon-long-arrow-down',
		),
		'fa-long-arrow-up'	=>	array(
			'title'	=>	'Long Arrow Up',
			'v3'	=>	'icon-long-arrow-up',
		),
		'fa-long-arrow-left'	=>	array(
			'title'	=>	'Long Arrow Left',
			'v3'	=>	'icon-long-arrow-left',
		),
		'fa-long-arrow-right'	=>	array(
			'title'	=>	'Long Arrow Right',
			'v3'	=>	'icon-long-arrow-right',
		),
		'fa-apple'	=>	array(
			'title'	=>	'Apple',
			'v3'	=>	'icon-apple',
		),
		'fa-windows'	=>	array(
			'title'	=>	'Windows',
			'v3'	=>	'icon-windows',
		),
		'fa-android'	=>	array(
			'title'	=>	'Android',
			'v3'	=>	'icon-android',
		),
		'fa-linux'	=>	array(
			'title'	=>	'Linux',
			'v3'	=>	'icon-linux',
		),
		'fa-dribbble'	=>	array(
			'title'	=>	'Dribbble',
			'v3'	=>	'icon-dribbble',
		),
		'fa-skype'	=>	array(
			'title'	=>	'Skype',
			'v3'	=>	'icon-skype',
		),
		'fa-foursquare'	=>	array(
			'title'	=>	'Foursquare',
			'v3'	=>	'icon-foursquare',
		),
		'fa-trello'	=>	array(
			'title'	=>	'Trello',
			'v3'	=>	'icon-trello',
		),
		'fa-female'	=>	array(
			'title'	=>	'Female',
			'v3'	=>	'icon-female',
		),
		'fa-male'	=>	array(
			'title'	=>	'Male',
			'v3'	=>	'icon-male',
		),
		'fa-gittip'	=>	array(
			'title'	=>	'Gittip',
			'v3'	=>	'icon-gittip',
		),
		'fa-sun-o'	=>	array(
			'title'	=>	'Sun O',
			'v3'	=>	'icon-sun',
		),
		'fa-moon-o'	=>	array(
			'title'	=>	'Moon O',
			'v3'	=>	'icon-moon',
		),
		'fa-archive'	=>	array(
			'title'	=>	'Archive',
			'v3'	=>	'icon-archive',
		),
		'fa-bug'	=>	array(
			'title'	=>	'Bug',
			'v3'	=>	'icon-bug',
		),
		'fa-vk'	=>	array(
			'title'	=>	'Vk',
			'v3'	=>	'icon-vk',
		),
		'fa-weibo'	=>	array(
			'title'	=>	'Weibo',
			'v3'	=>	'icon-weibo',
		),
		'fa-renren'	=>	array(
			'title'	=>	'Renren',
			'v3'	=>	'icon-renren',
		),
		'fa-pagelines'	=>	array(
			'title'	=>	'Pagelines',
			'v3'	=>	'',
		),
		'fa-stack-exchange'	=>	array(
			'title'	=>	'Stack Exchange',
			'v3'	=>	'',
		),
		'fa-arrow-circle-o-right'	=>	array(
			'title'	=>	'Arrow Circle O Right',
			'v3'	=>	'',
		),
		'fa-arrow-circle-o-left'	=>	array(
			'title'	=>	'Arrow Circle O Left',
			'v3'	=>	'',
		),
		'fa-caret-square-o-left'	=>	array(
			'title'	=>	'Caret Square O Left',
			'v3'	=>	'',
		),
		'fa-dot-circle-o'	=>	array(
			'title'	=>	'Dot Circle O',
			'v3'	=>	'',
		),
		'fa-wheelchair'	=>	array(
			'title'	=>	'Wheelchair',
			'v3'	=>	'',
		),
		'fa-vimeo-square'	=>	array(
			'title'	=>	'Vimeo Square',
			'v3'	=>	'',
		),
		'fa-try'	=>	array(
			'title'	=>	'Try',
			'v3'	=>	'',
		),
		'fa-plus-square-o'	=>	array(
			'title'	=>	'Plus Square O',
			'v3'	=>	'',
		),
	);

	return $icons;
}


function umicons_backconvert( $icon ){

}


function umicons_option_value_defaults( $defaults ){
	$defaults[ 'icon' ] = '';
	return $defaults;
}
add_filter( 'uberMenu_menu_item_options_value_defaults' , 'umicons_option_value_defaults' );






































/*

function umicons_fa_icons_working(){
	$icons = array(
		'fa-glass' =>	array( 
			'title' => 'Glass',
			'v3'	=> 'icon-glass',
		),
		'fa-music' =>	array( 
			'title' => 'Music',
			'v3'	=> 'icon-music',
		),
		'fa-search' =>	array( 
			'title' => 'Search',
			'v3'	=> 'icon-search',
		),
		'fa-envelope-o' =>	array( 
			'title' => 'Envelope (Outline)',
			'v3'	=> 'icon-envelope'
		),
		'fa-heart' =>	array( 
			'title' => 'Heart',
			'v3'	=> 'icon-heart',
		),
		'fa-star' =>	array( 
			'title' => 'Star',
			'v3'	=> 'icon-star',
		),
		'fa-star-o' =>	array( 
			'title' => 'Star (Outline)',
			'v3'	=> 'icon-star-empty'
		),
		'fa-user' =>	array( 
			'title' => 'User',
			'v3'	=> 'icon-user',
		),
		'fa-film' =>	array( 
			'title' => 'Film',
			'v3'	=> 'icon-film',
		),
		'fa-th-large' =>	array( 
			'title' => 'TH Large',
			'v3'	=> 'icon-th-large',
		),
		'fa-th' =>	array( 
			'title' => 'TH',
			'v3'	=> 'icon-th'
		),
		'fa-th-list' =>	array( 
			'title' => 'th-list',
			'v3'	=> 'icon-th-list'
		),
		'fa-check' =>	array( 
			'title' => 'Checkmark',
			'v3'	=> 'icon-ok'
		),
		'fa-times' =>	array( 
			'title' => 'Times',
			'v3'	=> 'icon-remove'
		),
		'fa-search-plus' =>	array( 
			'title' => 'Search Plus (Zoom In)',
			'v3'	=> 'icon-zoom-in',
		),
		'fa-search-minus' =>	array( 
			'title' => 'Search Minus (Zoom Out)',
			'v3'	=> 'icon-zoom-out'
		),
		'fa-power-off' =>	array( 
			'title' => 'Power Off',
			'v3'	=> 'icon-off'
		),
		'fa-signal' =>	array( 
			'title' => 'Signal',
			'v3'	=> 'icon-signal',
		),
		'fa-cog' =>	array( 
			'title' => 'Cog',
			'v3'	=> 'icon-cog',
		),
		'fa-trash-o' =>	array( 
			'title' => 'Trash (Outline)',
			'v3'	=> 'icon-trash',
		),
		'fa-home' =>	array( 
			'title' => 'Home',
			'v3'	=> 'icon-home',
		),
		'fa-file-o' =>	array( 
			'title' => 'File (Outline)',
			'v3'	=> 'icon-file',
		),
		'fa-clock-o' =>	array( 
			'title' => 'Clock (Outline)',
			'v3'	=> 'icon-time',
		),
		'fa-road' =>	array( 
			'title' => 'Road',
			'v3'	=> 'icon-road',
		),
		'fa-download' =>	array( 
			'title' => 'Download',
			'v3'	=> 'icon-download-alt'
		),
		'fa-arrow-circle-o-down' =>	array( 
			'title' => 'Arrow (Circle/Outline/Down)',
			'v3'	=> 'icon-download',
		),
		'fa-arrow-circle-o-up' =>	array( 
			'title' => 'Arrow (Circle/Outline/Up)',
			'v3'	=> 'icon-upload'
		),
		'fa-inbox' =>	array( 
			'title' => 'Inbox',
			'v3'	=> 'icon-inbox',
		),
		'fa-play-circle-o' =>	array( 
			'title' => 'Play (Circle/Outline)',
			'v3'	=> 'icon-play-circle',
		),
		'fa-repeat' =>	array( 
			'title' => 'Repeat',
			'v3'	=> 'icon-repeat',
		),
		'fa-refresh' =>	array( 
			'title' => 'Refresh',
			'v3'	=> 'icon-refresh'
		),
		'fa-list-alt' =>	array( 
			'title' => 'List (Alternative)',
			'v3'	=> 'icon-list-alt',
		),
		'fa-lock' =>	array( 
			'title' => 'Lock',
			'v3'	=> 'icon-lock',
		),
		'fa-flag' =>	array( 
			'title' => 'Flag',
			'v3'	=> 'icon-flag',
		),
		'fa-headphones' =>	array( 
			'title' => 'Headphones',
			'v3'	=> 'icon-headphones',
		),
		'fa-volume-off' =>	array( 
			'title' => 'Volume Off',
			'v3'	=> 'icon-volume-off',
		),
		'fa-volume-down' =>	array( 
			'title' => 'Volume Down',
			'v3'	=> 'icon-volume-down',
		),
		'fa-volume-up' =>	array( 
			'title' => 'Volume Up',
			'v3'	=> 'icon-volume-up',
		),
		'fa-qrcode' =>	array( 
			'title' => 'QR Code',
			'v3'	=> 'icon-qrcode',
		),
		'fa-barcode' =>	array( 
			'title' => 'Barcode',
			'v3'	=> 'icon-barcode',
		),
		'fa-tag' =>	array( 
			'title' => 'Tag',
			'v3'	=> 'icon-tag',
		),
		'fa-tags' =>	array( 
			'title' => 'tags',
			'v3'	=> 'icon-tags',
		),
		'fa-book' =>	array( 
			'title' => 'Book',
			'v3'	=> 'icon-book',
		),
		'fa-bookmark' =>array( 
			'title' => 'Bookmark',
			'v3'	=> 'icon-bookmark',
		),
		'fa-print' =>	array( 
			'title' => 'Print',
			'v3'	=> 'icon-print',
		),
		'fa-camera' =>	array( 
			'title' => 'Camera',
			'v3'	=> 'icon-camera',
		),
		'fa-font' =>	array( 
			'title' => 'Font',
			'v3'	=> 'icon-font',
		),
		'fa-bold' =>	array( 
			'title' => 'Bold',
			'v3'	=> 'icon-bold',
		),
		'fa-italic' =>	array( 
			'title' => 'Italic',
			'v3'	=> 'icon-italic'
		),
		'fa-text-height' =>	array( 
			'title' => 'Text Height',
			'v3'	=> 'icon-text-height'
		),
		'fa-text-width' =>	array( 
			'title' => 'Text Width',
			'v3'	=> 'icon-text-width'
		),
		'fa-align-left' =>	array( 
			'title' => 'Align Left',
			'v3'	=> 'icon-align-left',
		),
		'fa-align-center' =>	array( 
			'title' => 'Align Center',
			'v3'	=> 'icon-align-center'
		),
		'fa-align-right' =>	array( 
			'title' => 'Align Right',
			'v3'	=> 'icon-align-right',
		),
		'fa-align-justify' =>	array( 
			'title' => 'Align Justify',
			'v3'	=> 'icon-align-justify',
		),
		'fa-list' =>	array( 
			'title' => 'List',
			'v3'	=> 'icon-list',
		),
		'fa-outdent' =>	array( 
			'title' => 'Outdent',
			'v3'	=> 'icon-indent-left',
		),
		'fa-indent' =>	array( 
			'title' => 'Indent',
			'v3'	=> 'icon-indent-right',
		),
		'fa-video-camera' =>	array( 
			'title' => 'video-camera',
			'v3'	=> 'icon-facetime-video',
		),
		'fa-picture-o' =>	array( 
			'title' => 'Picture (Outline)',
			'v3'	=> 'icon-picture',
		),
		'fa-pencil' =>	array( 
			'title' => 'Pencil',
			'v3'	=> 'icon-pencil',
		),
		'fa-map-marker' =>	array( 
			'title' => 'Map Marker',
			'v3'	=> 'icon-map-marker',
		),
		'fa-adjust' =>	array( 
			'title' => 'Adjust',
			'v3'	=> 'icon-adjust',
		),
		'fa-tint' =>	array( 
			'title' => 'Tint',
			'v3'	=> 'icon-tint',
		),
		'fa-pencil-square-o' =>	array( 
			'title' => 'Pencil (Square/Outline)',
			'v3'	=> 'icon-edit',
		),
		'fa-share-square-o' =>	array( 
			'title' => 'Share (Square/Outline)',
			'v3'	=> 'icon-share',
		),
		'fa-check-square-o' =>	array( 
			'title' => 'Check (Square/Outline)',
			'v3'	=> 'icon-check',
		),
		'fa-arrows' =>	array( 
			'title' => 'Arrows',
			'v3'	=> 'icon-move',
		),
		'fa-step-backward' =>	array( 
			'title' => 'Step Backward',
			'v3'	=> 'icon-step-backward',
		),
		'fa-fast-backward' =>	array( 
			'title' => 'Fast Backward',
			'v3'	=> 'icon-fast-backward',
		),
		'fa-backward' =>	array( 
			'title' => 'Backward',
			'v3'	=> 'icon-backward',
		),
		'fa-play' =>	array( 
			'title' => 'Play',
			'v3'	=> 'icon-play',
		),
		'fa-pause' =>	array( 
			'title' => 'Pause',
			'v3'	=> 'icon-pause'
		),
		'fa-stop' =>	array( 
			'title' => 'Stop',
			'v3'	=> 'icon-stop',
		),
		'fa-forward' =>	array( 
			'title' => 'Forward',
			'v3'	=> 'icon-forward',
		),
		'fa-fast-forward' =>	array( 
			'title' => 'Fast Forward',
			'v3'	=> 'icon-fast-forward',
		),
		'fa-step-forward' =>	array( 
			'title' => 'Step Forward',
			'v3'	=> 'icon-step-forward',
		),
		'fa-eject' =>	array( 
			'title' => 'Eject',
			'v3'	=> 'icon-eject',
		),
		'fa-chevron-left' =>	array( 
			'title' => 'Chevron Left',
			'v3'	=> 'icon-chevron-left',
		),
		'fa-chevron-right' =>	array( 
			'title' => 'Chevron Right',
			'v3'	=> 'icon-chevron-right',
		),
		'fa-plus-circle' =>	array( 
			'title' => 'Plus (Circle)',
			'v3'	=> 'icon-plus-sign'
		),
		'fa-minus-circle' =>	array( 
			'title' => 'Minus (Circle)',
			'v3'	=> 'icon-minus-sign'
		),
		'fa-times-circle' =>	array( 
			'title' => 'Times (Circle)',
			'v3'	=> 'icon-remove-sign',
		),
		'fa-check-circle' =>	array( 
			'title' => 'Check (Circle)',
			'v3'	=> 'icon-ok-sign',
		),
		'fa-question-circle' =>	array( 
			'title' => 'Question (Circle)',
			'v3'	=> 'icon-question-sign',
		),
		'fa-info-circle' =>	array( 
			'title' => 'Info (Circle)',
			'v3'	=> 'icon-info-sign',
		),
		'fa-crosshairs' =>	array( 
			'title' => 'Crosshairs',
			'v3'	=> 'icon-screenshot',
		),
		'fa-times-circle-o' =>	array( 
			'title' => 'Times (Circle/Outline)',
			'v3'	=> 'icon-remove-circle',
		),
		'fa-check-circle-o' =>	array( 
			'title' => 'Check (Circle/Outline)',
			'v3'	=> 'icon-ok-circle',
		),
		'fa-ban' =>	array( 
			'title' => 'Ban',
			'v3'	=> 'icon-ban-circle'
		),
		'fa-arrow-left' =>	array( 
			'title' => 'Arrow Left',
			'v3'	=> 'icon-arrow-left'
		),
		'fa-arrow-right' =>	array( 
			'title' => 'arrow-right',
		),
		'fa-arrow-up' =>	array( 
			'title' => 'arrow-up',
		),
		'fa-arrow-down' =>	array( 
			'title' => 'arrow-down',
		),
		'fa-share' =>	array( 
			'title' => 'share',
		),
		'fa-expand' =>	array( 
			'title' => 'expand',
		),
		'fa-compress' =>	array( 
			'title' => 'compress',
		),
		'fa-plus' =>	array( 
			'title' => 'plus',
		),
		'fa-minus' =>	array( 
			'title' => 'minus',
		),
		'fa-asterisk' =>	array( 
			'title' => 'asterisk',
		),
		'fa-exclamation-circle' =>	array( 
			'title' => 'exclamation-circle',
		),
		'fa-gift' =>	array( 
			'title' => 'gift',
		),
		'fa-leaf' =>	array( 
			'title' => 'leaf',
		),
		'fa-fire' =>	array( 
			'title' => 'fire',
		),
		'fa-eye' =>	array( 
			'title' => 'eye',
		),
		'fa-eye-slash' =>	array( 
			'title' => 'eye-slash',
		),
		'fa-exclamation-triangle' =>	array( 
			'title' => 'exclamation-triangle',
		),
		'fa-plane' =>	array( 
			'title' => 'plane',
		),
		'fa-calendar' =>	array( 
			'title' => 'calendar',
		),
		'fa-random' =>	array( 
			'title' => 'random',
		),
		'fa-comment' =>	array( 
			'title' => 'comment',
		),
		'fa-magnet' =>	array( 
			'title' => 'magnet',
		),
		'fa-chevron-up' =>	array( 
			'title' => 'chevron-up',
		),
		'fa-chevron-down' =>	array( 
			'title' => 'chevron-down',
		),
		'fa-retweet' =>	array( 
			'title' => 'retweet',
		),
		'fa-shopping-cart' =>	array( 
			'title' => 'shopping-cart',
		),
		'fa-folder' =>	array( 
			'title' => 'folder',
		),
		'fa-folder-open' =>	array( 
			'title' => 'folder-open',
		),
		'fa-arrows-v' =>	array( 
			'title' => 'arrows-v',
		),
		'fa-arrows-h' =>	array( 
			'title' => 'arrows-h',
		),
		'fa-bar-chart-o' =>	array( 
			'title' => 'bar-chart-o',
		),
		'fa-twitter-square' =>	array( 
			'title' => 'twitter-square',
		),
		'fa-facebook-square' =>	array( 
			'title' => 'facebook-square',
		),
		'fa-camera-retro' =>	array( 
			'title' => 'camera-retro',
		),
		'fa-key' =>	array( 
			'title' => 'key',
		),
		'fa-cogs' =>	array( 
			'title' => 'cogs',
		),
		'fa-comments' =>	array( 
			'title' => 'comments',
		),
		'fa-thumbs-o-up' =>	array( 
			'title' => 'thumbs-o-up',
		),
		'fa-thumbs-o-down' =>	array( 
			'title' => 'thumbs-o-down',
		),
		'fa-star-half' =>	array( 
			'title' => 'star-half',
		),
		'fa-heart-o' =>	array( 
			'title' => 'heart-o',
		),
		'fa-sign-out' =>	array( 
			'title' => 'sign-out',
		),
		'fa-linkedin-square' =>	array( 
			'title' => 'linkedin-square',
		),
		'fa-thumb-tack' =>	array( 
			'title' => 'thumb-tack',
		),
		'fa-external-link' =>	array( 
			'title' => 'external-link',
		),
		'fa-sign-in' =>	array( 
			'title' => 'sign-in',
		),
		'fa-trophy' =>	array( 
			'title' => 'trophy',
		),
		'fa-github-square' =>	array( 
			'title' => 'github-square',
		),
		'fa-upload' =>	array( 
			'title' => 'upload',
		),
		'fa-lemon-o' =>	array( 
			'title' => 'lemon-o',
		),
		'fa-phone' =>	array( 
			'title' => 'phone',
		),
		'fa-square-o' =>	array( 
			'title' => 'square-o',
		),
		'fa-bookmark-o' =>	array( 
			'title' => 'bookmark-o',
		),
		'fa-phone-square' =>	array( 
			'title' => 'phone-square',
		),
		'fa-twitter' =>	array( 
			'title' => 'twitter',
		),
		'fa-facebook' =>	array( 
			'title' => 'facebook',
		),
		'fa-github' =>	array( 
			'title' => 'github',
		),
		'fa-unlock' =>	array( 
			'title' => 'unlock',
		),
		'fa-credit-card' =>	array( 
			'title' => 'credit-card',
		),
		'fa-rss' =>	array( 
			'title' => 'rss',
		),
		'fa-hdd-o' =>	array( 
			'title' => 'hdd-o',
		),
		'fa-bullhorn' =>	array( 
			'title' => 'bullhorn',
		),
		'fa-bell' =>	array( 
			'title' => 'bell',
		),
		'fa-certificate' =>	array( 
			'title' => 'certificate',
		),
		'fa-hand-o-right' =>	array( 
			'title' => 'hand-o-right',
		),
		'fa-hand-o-left' =>	array( 
			'title' => 'hand-o-left',
		),
		'fa-hand-o-up' =>	array( 
			'title' => 'hand-o-up',
		),
		'fa-hand-o-down' =>	array( 
			'title' => 'hand-o-down',
		),
		'fa-arrow-circle-left' =>	array( 
			'title' => 'arrow-circle-left',
		),
		'fa-arrow-circle-right' =>	array( 
			'title' => 'arrow-circle-right',
		),
		'fa-arrow-circle-up' =>	array( 
			'title' => 'arrow-circle-up',
		),
		'fa-arrow-circle-down' =>	array( 
			'title' => 'arrow-circle-down',
		),
		'fa-globe' =>	array( 
			'title' => 'globe',
		),
		'fa-wrench' =>	array( 
			'title' => 'wrench',
		),
		'fa-tasks' =>	array( 
			'title' => 'tasks',
		),
		'fa-filter' =>	array( 
			'title' => 'filter',
		),
		'fa-briefcase' =>	array( 
			'title' => 'briefcase',
		),
		'fa-arrows-alt' =>	array( 
			'title' => 'arrows-alt',
		),
		'fa-users' =>	array( 
			'title' => 'users',
		),
		'fa-link' =>	array( 
			'title' => 'link',
		),
		'fa-cloud' =>	array( 
			'title' => 'cloud',
		),
		'fa-flask' =>	array( 
			'title' => 'flask',
		),
		'fa-scissors' =>	array( 
			'title' => 'scissors',
		),
		'fa-files-o' =>	array( 
			'title' => 'files-o',
		),
		'fa-paperclip' =>	array( 
			'title' => 'paperclip',
		),
		'fa-floppy-o' =>	array( 
			'title' => 'floppy-o',
		),
		'fa-square' =>	array( 
			'title' => 'square',
		),
		'fa-bars' =>	array( 
			'title' => 'bars',
		),
		'fa-list-ul' =>	array( 
			'title' => 'list-ul',
		),
		'fa-list-ol' =>	array( 
			'title' => 'list-ol',
		),
		'fa-strikethrough' =>	array( 
			'title' => 'strikethrough',
		),
		'fa-underline' =>	array( 
			'title' => 'underline',
		),
		'fa-table' =>	array( 
			'title' => 'table',
		),
		'fa-magic' =>	array( 
			'title' => 'magic',
		),
		'fa-truck' =>	array( 
			'title' => 'truck',
		),
		'fa-pinterest' =>	array( 
			'title' => 'pinterest',
		),
		'fa-pinterest-square' =>	array( 
			'title' => 'pinterest-square',
		),
		'fa-google-plus-square' =>	array( 
			'title' => 'google-plus-square',
		),
		'fa-google-plus' =>	array( 
			'title' => 'google-plus',
		),
		'fa-money' =>	array( 
			'title' => 'money',
		),
		'fa-caret-down' =>	array( 
			'title' => 'caret-down',
		),
		'fa-caret-up' =>	array( 
			'title' => 'caret-up',
		),
		'fa-caret-left' =>	array( 
			'title' => 'caret-left',
		),
		'fa-caret-right' =>	array( 
			'title' => 'caret-right',
		),
		'fa-columns' =>	array( 
			'title' => 'columns',
		),
		'fa-sort' =>	array( 
			'title' => 'sort',
		),
		'fa-sort-asc' =>	array( 
			'title' => 'sort-asc',
		),
		'fa-sort-desc' =>	array( 
			'title' => 'sort-desc',
		),
		'fa-envelope' =>	array( 
			'title' => 'envelope',
		),
		'fa-linkedin' =>	array( 
			'title' => 'linkedin',
		),
		'fa-undo' =>	array( 
			'title' => 'undo',
		),
		'fa-gavel' =>	array( 
			'title' => 'gavel',
		),
		'fa-tachometer' =>	array( 
			'title' => 'tachometer',
		),
		'fa-comment-o' =>	array( 
			'title' => 'comment-o',
		),
		'fa-comments-o' =>	array( 
			'title' => 'comments-o',
		),
		'fa-bolt' =>	array( 
			'title' => 'bolt',
		),
		'fa-sitemap' =>	array( 
			'title' => 'sitemap',
		),
		'fa-umbrella' =>	array( 
			'title' => 'umbrella',
		),
		'fa-clipboard' =>	array( 
			'title' => 'clipboard',
		),
		'fa-lightbulb-o' =>	array( 
			'title' => 'lightbulb-o',
		),
		'fa-exchange' =>	array( 
			'title' => 'exchange',
		),
		'fa-cloud-download' =>	array( 
			'title' => 'cloud-download',
		),
		'fa-cloud-upload' =>	array( 
			'title' => 'cloud-upload',
		),
		'fa-user-md' =>	array( 
			'title' => 'user-md',
		),
		'fa-stethoscope' =>	array( 
			'title' => 'stethoscope',
		),
		'fa-suitcase' =>	array( 
			'title' => 'suitcase',
		),
		'fa-bell-o' =>	array( 
			'title' => 'bell-o',
		),
		'fa-coffee' =>	array( 
			'title' => 'coffee',
		),
		'fa-cutlery' =>	array( 
			'title' => 'cutlery',
		),
		'fa-file-text-o' =>	array( 
			'title' => 'file-text-o',
		),
		'fa-building-o' =>	array( 
			'title' => 'building-o',
		),
		'fa-hospital-o' =>	array( 
			'title' => 'hospital-o',
		),
		'fa-ambulance' =>	array( 
			'title' => 'ambulance',
		),
		'fa-medkit' =>	array( 
			'title' => 'medkit',
		),
		'fa-fighter-jet' =>	array( 
			'title' => 'fighter-jet',
		),
		'fa-beer' =>	array( 
			'title' => 'beer',
		),
		'fa-h-square' =>	array( 
			'title' => 'h-square',
		),
		'fa-plus-square' =>	array( 
			'title' => 'plus-square',
		),
		'fa-angle-double-left' =>	array( 
			'title' => 'angle-double-left',
		),
		'fa-angle-double-right' =>	array( 
			'title' => 'angle-double-right',
		),
		'fa-angle-double-up' =>	array( 
			'title' => 'angle-double-up',
		),
		'fa-angle-double-down' =>	array( 
			'title' => 'angle-double-down',
		),
		'fa-angle-left' =>	array( 
			'title' => 'angle-left',
		),
		'fa-angle-right' =>	array( 
			'title' => 'angle-right',
		),
		'fa-angle-up' =>	array( 
			'title' => 'angle-up',
		),
		'fa-angle-down' =>	array( 
			'title' => 'angle-down',
		),
		'fa-desktop' =>	array( 
			'title' => 'desktop',
		),
		'fa-laptop' =>	array( 
			'title' => 'laptop',
		),
		'fa-tablet' =>	array( 
			'title' => 'tablet',
		),
		'fa-mobile' =>	array( 
			'title' => 'mobile',
		),
		'fa-circle-o' =>	array( 
			'title' => 'circle-o',
		),
		'fa-quote-left' =>	array( 
			'title' => 'quote-left',
		),
		'fa-quote-right' =>	array( 
			'title' => 'quote-right',
		),
		'fa-spinner' =>	array( 
			'title' => 'spinner',
		),
		'fa-circle' =>	array( 
			'title' => 'circle',
		),
		'fa-reply' =>	array( 
			'title' => 'reply',
		),
		'fa-github-alt' =>	array( 
			'title' => 'github-alt',
		),
		'fa-folder-o' =>	array( 
			'title' => 'folder-o',
		),
		'fa-folder-open-o' =>	array( 
			'title' => 'folder-open-o',
		),
		'fa-smile-o' =>	array( 
			'title' => 'smile-o',
		),
		'fa-frown-o' =>	array( 
			'title' => 'frown-o',
		),
		'fa-meh-o' =>	array( 
			'title' => 'meh-o',
		),
		'fa-gamepad' =>	array( 
			'title' => 'gamepad',
		),
		'fa-keyboard-o' =>	array( 
			'title' => 'keyboard-o',
		),
		'fa-flag-o' =>	array( 
			'title' => 'flag-o',
		),
		'fa-flag-checkered' =>	array( 
			'title' => 'flag-checkered',
		),
		'fa-terminal' =>	array( 
			'title' => 'terminal',
		),
		'fa-code' =>	array( 
			'title' => 'code',
		),
		'fa-reply-all' =>	array( 
			'title' => 'reply-all',
		),
		'fa-mail-reply-all' =>	array( 
			'title' => 'mail-reply-all',
		),
		'fa-star-half-o' =>	array( 
			'title' => 'star-half-o',
		),
		'fa-location-arrow' =>	array( 
			'title' => 'location-arrow',
		),
		'fa-crop' =>	array( 
			'title' => 'crop',
		),
		'fa-code-fork' =>	array( 
			'title' => 'code-fork',
		),
		'fa-chain-broken' =>	array( 
			'title' => 'chain-broken',
		),
		'fa-question' =>	array( 
			'title' => 'question',
		),
		'fa-info' =>	array( 
			'title' => 'info',
		),
		'fa-exclamation' =>	array( 
			'title' => 'exclamation',
		),
		'fa-superscript' =>	array( 
			'title' => 'superscript',
		),
		'fa-subscript' =>	array( 
			'title' => 'subscript',
		),
		'fa-eraser' =>	array( 
			'title' => 'eraser',
		),
		'fa-puzzle-piece' =>	array( 
			'title' => 'puzzle-piece',
		),
		'fa-microphone' =>	array( 
			'title' => 'microphone',
		),
		'fa-microphone-slash' =>	array( 
			'title' => 'microphone-slash',
		),
		'fa-shield' =>	array( 
			'title' => 'shield',
		),
		'fa-calendar-o' =>	array( 
			'title' => 'calendar-o',
		),
		'fa-fire-extinguisher' =>	array( 
			'title' => 'fire-extinguisher',
		),
		'fa-rocket' =>	array( 
			'title' => 'rocket',
		),
		'fa-maxcdn' =>	array( 
			'title' => 'maxcdn',
		),
		'fa-chevron-circle-left' =>	array( 
			'title' => 'chevron-circle-left',
		),
		'fa-chevron-circle-right' =>	array( 
			'title' => 'chevron-circle-right',
		),
		'fa-chevron-circle-up' =>	array( 
			'title' => 'chevron-circle-up',
		),
		'fa-chevron-circle-down' =>	array( 
			'title' => 'chevron-circle-down',
		),
		'fa-html5' =>	array( 
			'title' => 'html5',
		),
		'fa-css3' =>	array( 
			'title' => 'css3',
		),
		'fa-anchor' =>	array( 
			'title' => 'anchor',
		),
		'fa-unlock-alt' =>	array( 
			'title' => 'unlock-alt',
		),
		'fa-bullseye' =>	array( 
			'title' => 'bullseye',
		),
		'fa-ellipsis-h' =>	array( 
			'title' => 'ellipsis-h',
		),
		'fa-ellipsis-v' =>	array( 
			'title' => 'ellipsis-v',
		),
		'fa-rss-square' =>	array( 
			'title' => 'rss-square',
		),
		'fa-play-circle' =>	array( 
			'title' => 'play-circle',
		),
		'fa-ticket' =>	array( 
			'title' => 'ticket',
		),
		'fa-minus-square' =>	array( 
			'title' => 'minus-square',
		),
		'fa-minus-square-o' =>	array( 
			'title' => 'minus-square-o',
		),
		'fa-level-up' =>	array( 
			'title' => 'level-up',
		),
		'fa-level-down' =>	array( 
			'title' => 'level-down',
		),
		'fa-check-square' =>	array( 
			'title' => 'check-square',
		),
		'fa-pencil-square' =>	array( 
			'title' => 'pencil-square',
		),
		'fa-external-link-square' =>	array( 
			'title' => 'external-link-square',
		),
		'fa-share-square' =>	array( 
			'title' => 'share-square',
		),
		'fa-compass' =>	array( 
			'title' => 'compass',
		),
		'fa-caret-square-o-down' =>	array( 
			'title' => 'caret-square-o-down',
		),
		'fa-caret-square-o-up' =>	array( 
			'title' => 'caret-square-o-up',
		),
		'fa-caret-square-o-right' =>	array( 
			'title' => 'caret-square-o-right',
		),
		'fa-eur' =>	array( 
			'title' => 'eur',
		),
		'fa-gbp' =>	array( 
			'title' => 'gbp',
		),
		'fa-usd' =>	array( 
			'title' => 'usd',
		),
		'fa-inr' =>	array( 
			'title' => 'inr',
		),
		'fa-jpy' =>	array( 
			'title' => 'jpy',
		),
		'fa-rub' =>	array( 
			'title' => 'rub',
		),
		'fa-krw' =>	array( 
			'title' => 'krw',
		),
		'fa-btc' =>	array( 
			'title' => 'btc',
		),
		'fa-file' =>	array( 
			'title' => 'file',
		),
		'fa-file-text' =>	array( 
			'title' => 'file-text',
		),
		'fa-sort-alpha-asc' =>	array( 
			'title' => 'sort-alpha-asc',
		),
		'fa-sort-alpha-desc' =>	array( 
			'title' => 'sort-alpha-desc',
		),
		'fa-sort-amount-asc' =>	array( 
			'title' => 'sort-amount-asc',
		),
		'fa-sort-amount-desc' =>	array( 
			'title' => 'sort-amount-desc',
		),
		'fa-sort-numeric-asc' =>	array( 
			'title' => 'sort-numeric-asc',
		),
		'fa-sort-numeric-desc' =>	array( 
			'title' => 'sort-numeric-desc',
		),
		'fa-thumbs-up' =>	array( 
			'title' => 'thumbs-up',
		),
		'fa-thumbs-down' =>	array( 
			'title' => 'thumbs-down',
		),
		'fa-youtube-square' =>	array( 
			'title' => 'youtube-square',
		),
		'fa-youtube' =>	array( 
			'title' => 'youtube',
		),
		'fa-xing' =>	array( 
			'title' => 'xing',
		),
		'fa-xing-square' =>	array( 
			'title' => 'xing-square',
		),
		'fa-youtube-play' =>	array( 
			'title' => 'youtube-play',
		),
		'fa-dropbox' =>	array( 
			'title' => 'dropbox',
		),
		'fa-stack-overflow' =>	array( 
			'title' => 'stack-overflow',
		),
		'fa-instagram' =>	array( 
			'title' => 'instagram',
		),
		'fa-flickr' =>	array( 
			'title' => 'flickr',
		),
		'fa-adn' =>	array( 
			'title' => 'adn',
		),
		'fa-bitbucket' =>	array( 
			'title' => 'bitbucket',
		),
		'fa-bitbucket-square' =>	array( 
			'title' => 'bitbucket-square',
		),
		'fa-tumblr' =>	array( 
			'title' => 'tumblr',
		),
		'fa-tumblr-square' =>	array( 
			'title' => 'tumblr-square',
		),
		'fa-long-arrow-down' =>	array( 
			'title' => 'long-arrow-down',
		),
		'fa-long-arrow-up' =>	array( 
			'title' => 'long-arrow-up',
		),
		'fa-long-arrow-left' =>	array( 
			'title' => 'long-arrow-left',
		),
		'fa-long-arrow-right' =>	array( 
			'title' => 'long-arrow-right',
		),
		'fa-apple' =>	array( 
			'title' => 'apple',
		),
		'fa-windows' =>	array( 
			'title' => 'windows',
		),
		'fa-android' =>	array( 
			'title' => 'android',
		),
		'fa-linux' =>	array( 
			'title' => 'linux',
		),
		'fa-dribbble' =>	array( 
			'title' => 'dribbble',
		),
		'fa-skype' =>	array( 
			'title' => 'skype',
		),
		'fa-foursquare' =>	array( 
			'title' => 'foursquare',
		),
		'fa-trello' =>	array( 
			'title' => 'trello',
		),
		'fa-female' =>	array( 
			'title' => 'female',
		),
		'fa-male' =>	array( 
			'title' => 'male',
		),
		'fa-gittip' =>	array( 
			'title' => 'gittip',
		),
		'fa-sun-o' =>	array( 
			'title' => 'sun-o',
		),
		'fa-moon-o' =>	array( 
			'title' => 'moon-o',
		),
		'fa-archive' =>	array( 
			'title' => 'archive',
		),
		'fa-bug' =>	array( 
			'title' => 'bug',
		),
		'fa-vk' =>	array( 
			'title' => 'vk',
		),
		'fa-weibo' =>	array( 
			'title' => 'weibo',
		),
		'fa-renren' =>	array( 
			'title' => 'renren',
		),
		'fa-pagelines' =>	array( 
			'title' => 'pagelines',
		),
		'fa-stack-exchange' =>	array( 
			'title' => 'stack-exchange',
		),
		'fa-arrow-circle-o-right' =>	array( 
			'title' => 'arrow-circle-o-right',
		),
		'fa-arrow-circle-o-left' =>	array( 
			'title' => 'arrow-circle-o-left',
		),
		'fa-caret-square-o-left' =>	array( 
			'title' => 'caret-square-o-left',
		),
		'fa-dot-circle-o' =>	array( 
			'title' => 'dot-circle-o',
		),
		'fa-wheelchair' =>	array( 
			'title' => 'wheelchair',
		),
		'fa-vimeo-square' =>	array( 
			'title' => 'vimeo-square',
		),
		'fa-try' =>	array( 
			'title' => 'try',
		),
		'fa-plus-square-o' =>	array( 
			'title' => 'plus-square-o',
		),
	);

	$old = umicons_get_class_array_v3();
	$k = 0;
	echo '<pre>';
	foreach( $icons as $class => $icon ){
		if( !isset( $icon['v3'] ) ){
			$icons[$class]['v3'] = $old[$k];
			//$icons[$class]['title'] = str_replace( '-o' , ' (Outline)', $icons[$class]['title'] );
			$title = str_replace( '-' , ' ', $icon['title'] );

			$icons[$class]['title'] = ucwords( $title );
		}

		?>
'<?php echo $class; ?>'	=>	array(
			'title'	=>	'<?php echo $icons[$class]['title']; ?>',
			'v3'	=>	'<?php echo $icons[$class]['v3']; ?>',
		),
		<?php

		$k++;
		//echo $k;
	}
	echo '</pre>';
	//umssd( $icons );
}

//umicons_fa_icons();
function umicons_get_class_array_v3(){
	$icon_class_array = array(
		'icon-glass',
		'icon-music',
		'icon-search',
		'icon-envelope',
		'icon-heart',
		'icon-star',
		'icon-star-empty',
		'icon-user',
		'icon-film',
		'icon-th-large',
		'icon-th',
		'icon-th-list',
		'icon-ok',
		'icon-remove',
		'icon-zoom-in',

		'icon-zoom-out',
		'icon-off',
		'icon-signal',
		'icon-cog',
		'icon-trash',
		'icon-home',
		'icon-file',
		'icon-time',
		'icon-road',
		'icon-download-alt',
		'icon-download',
		'icon-upload',
		'icon-inbox',
		'icon-play-circle',
		'icon-repeat',

		'icon-refresh',
		'icon-list-alt',
		'icon-lock',
		'icon-flag',
		'icon-headphones',
		'icon-volume-off',
		'icon-volume-down',
		'icon-volume-up',
		'icon-qrcode',
		'icon-barcode',
		'icon-tag',
		'icon-tags',
		'icon-book',
		'icon-bookmark',
		'icon-print',

		'icon-camera',
		'icon-font',
		'icon-bold',
		'icon-italic',
		'icon-text-height',
		'icon-text-width',
		'icon-align-left',
		'icon-align-center',
		'icon-align-right',
		'icon-align-justify',
		'icon-list',
		'icon-indent-left',
		'icon-indent-right',
		'icon-facetime-video',
		'icon-picture',

		'icon-pencil',
		'icon-map-marker',
		'icon-adjust',
		'icon-tint',
		'icon-edit',
		'icon-share',
		'icon-check',
		'icon-move',
		'icon-step-backward',
		'icon-fast-backward',
		'icon-backward',
		'icon-play',
		'icon-pause',
		'icon-stop',
		'icon-forward',

		'icon-fast-forward',
		'icon-step-forward',
		'icon-eject',
		'icon-chevron-left',
		'icon-chevron-right',
		'icon-plus-sign',
		'icon-minus-sign',
		'icon-remove-sign',
		'icon-ok-sign',
		'icon-question-sign',
		'icon-info-sign',
		'icon-screenshot',
		'icon-remove-circle',
		'icon-ok-circle',
		'icon-ban-circle',

		'icon-arrow-left',
		'icon-arrow-right',
		'icon-arrow-up',
		'icon-arrow-down',
		'icon-share-alt',
		'icon-resize-full',
		'icon-resize-small',
		'icon-plus',
		'icon-minus',
		'icon-asterisk',
		'icon-exclamation-sign',
		'icon-gift',
		'icon-leaf',
		'icon-fire',
		'icon-eye-open',

		'icon-eye-close',
		'icon-warning-sign',
		'icon-plane',
		'icon-calendar',
		'icon-random',
		'icon-comment',
		'icon-magnet',
		'icon-chevron-up',
		'icon-chevron-down',
		'icon-retweet',
		'icon-shopping-cart',
		'icon-folder-close',
		'icon-folder-open',
		'icon-resize-vertical',
		'icon-resize-horizontal',

		'icon-bar-chart',
		'icon-twitter-sign',
		'icon-facebook-sign',
		'icon-camera-retro',
		'icon-key',
		'icon-cogs',
		'icon-comments',
		'icon-thumbs-up',
		'icon-thumbs-down',
		'icon-star-half',
		'icon-heart-empty',
		'icon-signout',
		'icon-linkedin-sign',
		'icon-pushpin',
		'icon-external-link',

		'icon-signin',
		'icon-trophy',
		'icon-github-sign',
		'icon-upload-alt',
		'icon-lemon',
		'icon-phone',
		'icon-check-empty',
		'icon-bookmark-empty',
		'icon-phone-sign',
		'icon-twitter',
		'icon-facebook',
		'icon-github',
		'icon-unlock',
		'icon-credit-card',
		'icon-rss',

		'icon-hdd',
		'icon-bullhorn',
		'icon-bell',
		'icon-certificate',
		'icon-hand-right',
		'icon-hand-left',
		'icon-hand-up',
		'icon-hand-down',
		'icon-circle-arrow-left',
		'icon-circle-arrow-right',
		'icon-circle-arrow-up',
		'icon-circle-arrow-down',
		'icon-globe',
		'icon-wrench',
		'icon-tasks',

		'icon-filter',
		'icon-briefcase',
		'icon-fullscreen',

		'icon-group',
		'icon-link',
		'icon-cloud',
		'icon-beaker',
		'icon-cut',
		'icon-copy',
		'icon-paper-clip',
		'icon-save',
		'icon-sign-blank',
		'icon-reorder',
		'icon-list-ul',
		'icon-list-ol',
		'icon-strikethrough',
		'icon-underline',
		'icon-table',

		'icon-magic',
		'icon-truck',
		'icon-pinterest',
		'icon-pinterest-sign',
		'icon-google-plus-sign',
		'icon-google-plus',
		'icon-money',
		'icon-caret-down',
		'icon-caret-up',
		'icon-caret-left',
		'icon-caret-right',
		'icon-columns',
		'icon-sort',
		'icon-sort-down',
		'icon-sort-up',

		'icon-envelope-alt',
		'icon-linkedin',
		'icon-undo',
		'icon-legal',
		'icon-dashboard',
		'icon-comment-alt',
		'icon-comments-alt',
		'icon-bolt',
		'icon-sitemap',
		'icon-umbrella',
		'icon-paste',
		'icon-lightbulb',
		'icon-exchange',
		'icon-cloud-download',
		'icon-cloud-upload',

		'icon-user-md',
		'icon-stethoscope',
		'icon-suitcase',
		'icon-bell-alt',
		'icon-coffee',
		'icon-food',
		'icon-file-alt',
		'icon-building',
		'icon-hospital',
		'icon-ambulance',
		'icon-medkit',
		'icon-fighter-jet',
		'icon-beer',
		'icon-h-sign',
		'icon-plus-sign-alt',

		'icon-double-angle-left',
		'icon-double-angle-right',
		'icon-double-angle-up',
		'icon-double-angle-down',
		'icon-angle-left',
		'icon-angle-right',
		'icon-angle-up',
		'icon-angle-down',
		'icon-desktop',
		'icon-laptop',
		'icon-tablet',
		'icon-mobile-phone',
		'icon-circle-blank',
		'icon-quote-left',
		'icon-quote-right',

		'icon-spinner',
		'icon-circle',
		'icon-reply',
		'icon-github-alt',
		'icon-folder-close-alt',
		'icon-folder-open-alt',



		// 3.2.1 
		//'icon-expand-alt',
		//'icon-collapse-alt',
		'icon-smile',
		'icon-frown',
		'icon-meh',
		'icon-gamepad',
		'icon-keyboard',
		'icon-flag-alt',
		'icon-flag-checkered',
		'icon-terminal',
		'icon-code',
		'icon-reply-all',
		'icon-mail-reply-all',
		'icon-star-half-full',
		//'icon-star-half-empty',
		'icon-location-arrow',
		'icon-crop',
		'icon-code-fork',
		'icon-unlink',
		'icon-question',
		'icon-info',
		'icon-exclamation',
		'icon-superscript',
		'icon-subscript',
		'icon-eraser',
		'icon-puzzle-piece',
		'icon-microphone',
		'icon-microphone-off',
		'icon-shield',
		'icon-calendar-empty',
		'icon-fire-extinguisher',
		'icon-rocket',
		'icon-maxcdn',
		'icon-chevron-sign-left',
		'icon-chevron-sign-right',
		'icon-chevron-sign-up',
		'icon-chevron-sign-down',
		'icon-html5',
		'icon-css3',
		'icon-anchor',
		'icon-unlock-alt',
		'icon-bullseye',
		'icon-ellipsis-horizontal',
		'icon-ellipsis-vertical',
		'icon-rss-sign',
		'icon-play-sign',
		'icon-ticket',
		'icon-minus-sign-alt',
		'icon-check-minus',
		'icon-level-up',
		'icon-level-down',
		'icon-check-sign',
		'icon-edit-sign',
		'icon-external-link-sign',
		'icon-share-sign',
		'icon-compass',
		'icon-collapse',
		'icon-collapse-top',
		'icon-expand',
		'icon-eur',
		'icon-gbp',
		'icon-usd',
		'icon-inr',
		'icon-jpy',
		'icon-cny',
		'icon-krw',
		'icon-btc',
		'icon-file',
		'icon-file-text',
		'icon-sort-by-alphabet',
		'icon-sort-by-alphabet-alt',
		'icon-sort-by-attributes',
		'icon-sort-by-attributes-alt',
		'icon-sort-by-order',
		'icon-sort-by-order-alt',
		'icon-thumbs-up',
		'icon-thumbs-down',
		'icon-youtube-sign',
		'icon-youtube',
		'icon-xing',
		'icon-xing-sign',
		'icon-youtube-play',
		'icon-dropbox',
		'icon-stackexchange',
		'icon-instagram',
		'icon-flickr',
		'icon-adn',
		'icon-bitbucket',
		'icon-bitbucket-sign',
		'icon-tumblr',
		'icon-tumblr-sign',
		'icon-long-arrow-down',
		'icon-long-arrow-up',
		'icon-long-arrow-left',
		'icon-long-arrow-right',
		'icon-apple',
		'icon-windows',
		'icon-android',
		'icon-linux',
		'icon-dribbble',
		'icon-skype',
		'icon-foursquare',
		'icon-trello',
		'icon-female',
		'icon-male',
		'icon-gittip',
		'icon-sun',
		'icon-moon',
		'icon-archive',
		'icon-bug',
		'icon-vk',
		'icon-weibo',
		'icon-renren',
	);
	return $icon_class_array;
}
*/