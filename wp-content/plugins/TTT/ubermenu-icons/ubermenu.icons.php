<?php
/*
Plugin Name: UberMenu Icons
Plugin URI: http://wpmegamenu.com/icons
Description: Add Font Awesome Icons to your UberMenu menu items
Author: Chris Mavricos, SevenSpark
Author URI: http://sevenspark.com
Version: 1.1
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'UberMenu_Icons' ) ) :


final class UberMenu_Icons {
	/** Singleton *************************************************************/

	private static $instance;
	private $registered_icons;

	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new UberMenu_Icons;
			self::$instance->setup_constants();
			self::$instance->includes();
		}
		return self::$instance;
	}

	/**
	 * Setup plugin constants
	 *
	 * @since 1.0
	 * @access private
	 * @uses plugin_dir_path() To generate plugin path
	 * @uses plugin_dir_url() To generate plugin url
	 */
	private function setup_constants() {
		// Plugin version

		if( ! defined( 'UM_ICONS_VERSION' ) )
			define( 'UM_ICONS_VERSION', '1.1' );

		// Plugin Folder URL
		if( ! defined( 'UM_ICONS_PLUGIN_URL' ) )
			define( 'UM_ICONS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

		// Plugin Folder Path
		if( ! defined( 'UM_ICONS_PLUGIN_DIR' ) )
			define( 'UM_ICONS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

		// Plugin Root File
		if( ! defined( 'UM_ICONS_PLUGIN_FILE' ) )
			define( 'UM_ICONS_PLUGIN_FILE', __FILE__ );
	}

	private function includes() {
		
		require_once UM_ICONS_PLUGIN_DIR . 'includes/functions.php';
		
	}

	public function register_icons( $group , $iconmap ){
		if( !is_array( $this->registered_icons ) ) $this->registered_icons = array();
		$this->registered_icons[$group] = $iconmap;
	}
	public function degister_icons( $group ){
		if( is_array( $this->registered_icons ) && isset( $this->registered_icons[$group] ) ){
			unset( $this->registered_icons[$group] );
		}
	}
	public function get_registered_icons(){ //$group = '' ){
		return $this->registered_icons;
	}

}

endif; // End if class_exists check



function UM_ICONS() {
	return UberMenu_Icons::instance();
}


function umicons_load(){
	UM_ICONS();
}
add_action( 'uberMenu_load_dependents' , 'umicons_load' );


//Let the user know they need to install UberMenu if they haven't already
add_action( 'plugins_loaded' , 'umicons_ubercheck' , 20 );
function umicons_ubercheck(){
	if( !function_exists( 'uberMenu_direct' ) ) add_action( 'admin_notices', 'umicons_admin_notice' );
}
function umicons_admin_notice() {
    ?><div class="error">
        <p><?php _e( '<strong>UberMenu Icons</strong> is an Extension and requires the UberMenu plugin to function.  Please install and activate <a href="http://wpmegamenu.com">UberMenu</a> to use this extension.', 'ubermenu' ); ?></p>
    </div><?php
}

