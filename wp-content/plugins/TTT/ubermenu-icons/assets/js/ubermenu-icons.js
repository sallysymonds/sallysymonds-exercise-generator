jQuery( 'document' ).ready( function( $ ){

	$( '.edit-menu-item-icon' ).each( function( k ){

		var $select = $( this );
		addIconPicker( $select );
		
	});

	function addIconPicker( $select ){

		$select.addClass( 'umicons-primed' );

		var starter_class = $select.val();
		var starter_name = $select.find( ':selected' ).text();
		if( starter_name == '' ) starter_name = 'Select Icon';
		var $iconBox = $( '<div class="umicon-button-container">');
		var $iconButton = $( '<a class="button umicon-button" href="#"><i class="'+starter_class+'"></i> <span>'+starter_name+'</span></a>' );
		$iconButton.appendTo( $iconBox );
		$select.before( $iconBox );

		$select.hide();

		$iconButton.click( function( e ){

			e.preventDefault();

			$iconButton.toggleClass( 'active' );

			var $selectionBox = $( this ).parent().find( '.umicon-select-box' );
			if( $selectionBox.size() === 0 ){
				$selectionBox = $( '<div class="umicon-select-box">' );
				$selectionBox.hide();
				var icontitle;
				$select.find( 'option' ).each( function( k ){
					icontitle = $(this).text(); // this.value.substring( 5 ).replace( /-/g , ' ' );
					$selectionBox.append( '<i class="' + this.value + '" title="'+icontitle+'"></i>')
				});
				$selectionBox.append( '<i class="umicon-remove">&times; Remove</i>' );

				$selectionBox.find( 'i' ).click( function(e){
					e.preventDefault();
					var iconclass = $(this).attr( 'class' );

					if( iconclass == 'umicon-remove' ) iconclass = '';

					$select.val( iconclass ).trigger( 'change' );
					var iconname = $select.find( ':selected' ).text();
					if( iconname == '' ) iconname = 'Select Icon';
					$iconButton.find( 'i' ).attr( 'class' , iconclass );
					$iconButton.find( 'span' ).text( iconname );
					$selectionBox.fadeOut(); 
					$iconButton.removeClass( 'active' );
				} ); 

				$selectionBox.appendTo( $( this ).parent() );

			}

			$selectionBox.fadeToggle();

		});
	}


	//Listen for new menu items
	$( '#menu-management' ).on( 'DOMNodeInserted' , 'ul.menu', function( e ){
		if( e.target.tagName == 'LI' ){
			var $item = $( e.target );
			var $select = $item.find( '.edit-menu-item-icon' );
			if( $select.size() > 0 && !$select.hasClass( 'umicons-primed' ) ){
				//console.log( $select );
				//console.log( 'added menu item ', e.target );
				addIconPicker( $select );
			}
		}
	});

});