<?php
/**
  * Plugin Name: Sally Symonds Custom Video Listing 
  * Plugin URI: http://www.sallysymonds.com
  * Description: Sally Symonds Custom Video Listing Integration
  * Version: 1.0
  * Author: Netseek
  * Author URI:  http://netseek.com.au
  */

add_shortcode( 'embedvideopost', 'sally_embedvideopost_func' );

function sally_embedvideopost_func( $atts ){
	
	extract( shortcode_atts( array(
			'category' 		=> '', // ALL , exercise
			'week' 			=> '' , // ALL, 1 , 2 ...
			'post_status' 	=> 'any',
			'cat_default' 	=> '', 
			'level_default'	=> '', //Beginner 
		), $atts , 'embedvideopost' ) 
	);
	
	
	$params['category_name']  = strtolower( $category );
	$params['post_status']    = $post_status;
	$params['week']    		  = $week;
	
	$params['cat_default']     = $cat_default;
	$params['level_default']   = $level_default;
	
	$exercise				  = esc_sql( $_REQUEST['exercise'] );
	$skill					  = esc_sql( $_REQUEST['skill'] );
	
	if( $_POST ){
		
		$exercise			      = esc_sql( $_POST['exercise'] );
		$skill					  = esc_sql( $_POST['skill'] );
		$week					  = esc_sql( $_POST['week'] );
		$params['week']    		  = esc_sql( $_POST['week'] );
		
	} else {

		$exercise				  = esc_sql( $cat_default );
		$skill					  = esc_sql( $level_default );
		
	}
	
	
		if( is_numeric( $week ) ){
	
			/*
			$e = array(
	            'key' 		=> 'exercise',
	            'value' 	=> $exercise,
	            'compare' 	=> '=='
	         );
	         
	         $s = array(
	            'key' 		=> 'skill',
	            'value' 	=> $skill,
	            'compare' 	=> '=='
	         );
	         
	         $w = array(
	            'week' 		=> 'week',
	            'value' 	=> $week,
	            'compare' 	=> '=='
	         );
	         */
			
			if( $exercise == 'ALL' && $skill == 'ALL' ){
	         
				$params['meta_query'] = array(
		         array(
		            'key' 		=> 'week',
		            'value' 	=> $week,
		            'compare' 	=> '='
		         )
		      );
				
			}elseif ( $exercise == 'ALL' && $skill  != 'ALL' ){
				
				$params['meta_query'] = array(
				'relation' => 'AND',
		         array(
		            'key' 		=> 'skill',
		            'value' 	=> $skill,
		            'compare' 	=> '='
		         ),
		         
		         array(
		            'key' 		=> 'week',
		            'value' 	=> $week,
		            'compare' 	=> '='
		         ),
		      );
				
				
			}else if( $skill == 'ALL' && $exercise != 'ALL' ){
				
				$params['meta_query'] = array(
				'relation' => 'AND',
		         array(
		            'key' 		=> 'exercise',
		            'value' 	=> $exercise,
		            'compare' 	=> '='
		         ),
		         array(
		            'key' 		=> 'week',
		            'value' 	=> $week,
		         	//'type'      => 'NUMERIC',
		            'compare' 	=> '='
		         ),
		      );
				
			}else{
			
			$params['meta_query'] = array(
				'relation' => 'AND',
		         array(
		            'key' 		=> 'exercise',
		            'value' 	=> $exercise,
		            'compare' 	=> '='
		         ),
		         array(
		            'key' 		=> 'skill',
		            'value' 	=> $skill,
		            'compare' 	=> '='
		         ),
		         array(
		            'key' 		=> 'week',
		            'value' 	=> $week,
		            'compare' 	=> '='
		         ),
		      );
			
			}
		}else{
			
			if( $exercise == 'ALL' && $skill == 'ALL' ){
				
			}elseif ( $exercise == 'ALL' && $skill != 'ALL'  ){
			
				$params['meta_query'] = array(
		         array(
		            'key' 		=> 'skill',
		            'value' 	=>  $skill ,
		            'compare' 	=> '='
		         )
		      );
				
			}elseif ($skill == 'ALL' && $exercise != 'ALL'){

				$params['meta_query'] = array(
		         array(
		            'key' 		=> 'exercise',
		            'value' 	=> $exercise,
		            'compare' 	=> '='
		         )
		      );
				
			}else{
				
				$params['meta_query'] = array(
				//'relation' => 'OR',
				'relation' => 'AND',
		         array(
		            'key' 		=> 'exercise',
		            'value' 	=> $exercise,
		            'compare' 	=> '='
		         ),
		         array(
		            'key' 		=> 'skill',
		            'value' 	=>  $skill ,
		            'compare' 	=> '='
		         )
		      );
				
			}
			
			
		}
		
		
	
	return getVideoPost( $params );
}

function getNoVideoPost(){
	
}

function getVideoPost( $params ){

	//add_filter( 'post_thumbnail_html', 'wp_post_thumbnail_class_filter_reset' );
	
	//$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	
	$args = array(
	  'posts_per_page' => -1,	// for pagination
	  //'paged' 		   => $paged, // for pagination
	  'numberposts' => -1,
	  'category_name' => $params['category_name'] ,
      'post_status'   => $params['post_status'],
      'meta_query'    => $params['meta_query'],
	  'orderby'       => 'title',
	  'order' 		  => 'ASC'	
	  //'week' => $params['week'] 
   );
	
  // echo '<pre>'; print_r( $args ); echo '</pre>';
   
   $videoPosts = new WP_Query( $args );
   
   //echo '<pre>'; print_r( $videoPosts ); echo '</pre>';
   
   if( ! $_POST ){
   		
   		$cat_default   = $params['cat_default'];
		$level_default = $params['level_default'];
   	
   		$_REQUEST['exercise'] =  $cat_default;
   		$_REQUEST['skill']    =  $level_default;
   		
   }
   
   
   
    $html = '';
	
	$html .= '<div class="video_wrapper">';
	$html .= '<div class="video_menus">';
	
	/*
	 * commented out by noel
	$html .= '<form action="'. get_permalink() .'" method="post">';
	$html .= '<input type="hidden" name="week" value="'.$params['week'].'" />'; 
	$html .= '<select name="exercise" id="exercise" class="sel_exercise" >';
	$html .= '<option value="">'. __( 'Select Exercise', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="ALL" '. ( $_REQUEST['exercise'] == 'ALL' ?  'selected' : '' ) .'>'. __( 'ALL', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Legs" '. ( $_REQUEST['exercise'] == 'Legs' ?  'selected' : '' ) .'>'. __( 'Legs', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Arms" '. ( $_REQUEST['exercise'] == 'Arms' ?  'selected' : '' ) .'>'. __( 'Arms', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Thighs" '. ( $_REQUEST['exercise'] == 'Thighs' ?  'selected' : '' ) .'>'. __( 'Thighs', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Warm up" '. ( $_REQUEST['exercise'] == 'Warm up' ?  'selected' : '' ) .'>'. __( 'Warm up', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Squat" '. ( $_REQUEST['exercise'] == 'Squat' ?  'selected' : '' ) .'>'. __( 'Squat', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Lunge" '. ( $_REQUEST['exercise'] == 'Lunge' ?  'selected' : '' ) .'>'. __( 'Lunge', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Push-up" '. ( $_REQUEST['exercise'] == 'Push-up' ?  'selected' : '' ) .'>'. __( 'Push-up', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Triceps" '. ( $_REQUEST['exercise'] == 'Triceps' ?  'selected' : '' ) .'>'. __( 'Triceps', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Full Body" '. ( $_REQUEST['exercise'] == 'Full Body' ?  'selected' : '' ) .'>'. __( 'Full Body', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Treadmill" '. ( $_REQUEST['exercise'] == 'Treadmill' ?  'selected' : '' ) .'>'. __( 'Treadmill', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Bike" '. ( $_REQUEST['exercise'] == 'Bike' ?  'selected' : '' ) .'>'. __( 'Bike', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Rowing" '. ( $_REQUEST['exercise'] == 'Rowing' ?  'selected' : '' ) .'>'. __( 'Rowing', 'sallysymonds'  ) .'</option>';
	
	$html .= '<option value="Planking" '. ( $_REQUEST['exercise'] == 'Planking' ?  'selected' : '' ) .'>'. __( 'Planking', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Biceps" '. ( $_REQUEST['exercise'] == 'Biceps' ?  'selected' : '' ) .'>'. __( 'Biceps', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Kitchen Workout" '. ( $_REQUEST['exercise'] == 'Kitchen Workout' ?  'selected' : '' ) .'>'. __( 'Kitchen Workout', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Office Workout" '. ( $_REQUEST['exercise'] == 'Office Workout' ?  'selected' : '' ) .'>'. __( 'Office Workout', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Upper Body" '. ( $_REQUEST['exercise'] == 'Upper Body' ?  'selected' : '' ) .'>'. __( 'Upper Body', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Lower Body" '. ( $_REQUEST['exercise'] == 'Lower Body' ?  'selected' : '' ) .'>'. __( 'Lower Body', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Circuit Training" '. ( $_REQUEST['exercise'] == 'Circuit Training' ?  'selected' : '' ) .'>'. __( 'Circuit Training', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Bosu" '. ( $_REQUEST['exercise'] == 'Bosu' ?  'selected' : '' ) .'>'. __( 'Bosu', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Weights" '. ( $_REQUEST['exercise'] == 'Weights' ?  'selected' : '' ) .'>'. __( 'Weights', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Weight Vest" '. ( $_REQUEST['exercise'] == 'Weight Vest' ?  'selected' : '' ) .'>'. __( 'Weight Vest', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Chest" '. ( $_REQUEST['exercise'] == 'Chest' ?  'selected' : '' ) .'>'. __( 'Chest', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Back" '. ( $_REQUEST['exercise'] == 'Back' ?  'selected' : '' ) .'>'. __( 'Back', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Music" '. ( $_REQUEST['exercise'] == 'Music' ?  'selected' : '' ) .'>'. __( 'Music', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Dance" '. ( $_REQUEST['exercise'] == 'Dance' ?  'selected' : '' ) .'>'. __( 'Dance', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Abdominal" '. ( $_REQUEST['exercise'] == 'Abdominal' ?  'selected' : '' ) .'>'. __( 'Abdominal', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Hamstring" '. ( $_REQUEST['exercise'] == 'Hamstring' ?  'selected' : '' ) .'>'. __( 'Hamstring', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Stretching" '. ( $_REQUEST['exercise'] == 'Stretching' ?  'selected' : '' ) .'>'. __( 'Stretching', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Backside" '. ( $_REQUEST['exercise'] == 'Backside' ?  'selected' : '' ) .'>'. __( 'Backside', 'sallysymonds'  ) .'</option>';
	
	$html .= '</select>';
	
	$html .= '<select name="skill" id="skill" class="sel_skill" >';
	$html .= '<option value="">'. __( 'Select Skill', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="ALL" '. ( $_REQUEST['skill'] == 'ALL' ?  'selected' : '' ) .'>'. __( 'ALL', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Beginner" '. ( $_REQUEST['skill'] == 'Beginner' ?  'selected' : '' ) .'>'. __( 'Beginner', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Intermediate" '. ( $_REQUEST['skill'] == 'Intermediate' ?  'selected' : '' ) .'>'. __( 'Intermediate', 'sallysymonds'  ) .'</option>';
	$html .= '<option value="Advanced" '. ( $_REQUEST['skill'] == 'Advanced' ?  'selected' : '' ) .'>'. __( 'Advanced', 'sallysymonds'  ) .'</option>';
	$html .= '</select>';
	
	$html .= '<input type="submit"  value="Submit" class="filter_button" >';
	$html .= '</form>';
   */
   
	if( $videoPosts->have_posts() ) {
	$html .= '<div class="sally_video_list">';
	$html .= '<ul>';
	
	//echo '<pre>'; print_r( $videoPosts->posts ); echo '</pre>';
	
	 foreach ( $videoPosts->posts as $post ) {
	 	
	 	$args = array(
                        'post_type' => 'attachment',
                        'numberposts' => -1,
                        'post_status' => null,
                        'post_parent' => $post->ID
                    ); 
                    
        $attachments = get_posts( $args );
        $imgsrc = "";
        
        if ( $attachments ){
           foreach ( $attachments as $attachment) {
               $imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               //$imgsrc = wp_get_attachment_image($attachment->ID, 'full'); //thumbnail, medium, large or full)
        		break;
               }
			}
            
	 		$html .= '<li class="">';

	 		$matches = array();
            preg_match('|https://www.youtube.com/watch\?v=([_,a-zA-Z0-9,-]+)|', $post->post_content, $matches);
            //echo '<pre>'; print_r( $matches ); echo '</pre>';
            
            $v = $matches[1];
            	
	 		if( $imgsrc != '' ){
	 			//$html .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			/*
	 			 * Modified by noel
	 			$html .= '<a href="https://www.youtube.com/watch?v='.$v.'" title="' . esc_attr( $post->post_title ) . '" style="position:relative;">';
	 			$html .= '<img src="/files/img/youtube.png" style="position: absolute;left: -174px;top: 50px;" />';
            	$html .= __( get_the_post_thumbnail( $post->ID , 'medium' ), 'sallysymonds' );
            	$html .=  '</a>';
            	* */ 
            	$html .= '<iframe width="600" height="336" src="//www.youtube.com/embed/'.$v.'" frameborder="0" allowfullscreen></iframe>';
				$html .=   __( getExcerptById( $post->ID  ) , 'sallysymonds' );

	 		}

	 			//$html .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			/*
	 			 * modified by noel
	 			$html .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			$html .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$html .=  '</a>';
            	$html .=   __( getExcerptById( $post->ID  ) , 'sallysymonds' );
            	$html .=   '<p><a href="https://www.youtube.com/watch?v='.$v.'" >'. __('Watch Now','sallysymonds') .'</a></p>';
            	* */
            	
        $html .= '</li>';
	 }
	  
	$html .= '</ul>';
	/* pagination
	$big = 999999999;
	$html .=  paginate_links( array(
				'base' 	  => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'   => '?paged=%#%',
				'current'  => max( 1, get_query_var('paged') ),
				'total'    => $videoPosts->max_num_pages,
			    'add_args' => array(
									 'excercise' => esc_sql( $_REQUEST['excercise'] ) ,
			    					 'skill' 	 => esc_sql( $_REQUEST['skill'] ) , 
			    					 'week'		 => esc_sql( $params['week'] ) 
									)
			) );
		*/	
	$html .= '</div><!-- end  -->';	
	
	}else{
		$html .=  '<p>'. __( 'Sorry, no posts matched your criteria.' ). '</p>';
	}
	
	$html .= '</div><!-- end video_menus -->';
	$html .= '</div><!-- end video_wrapper -->';
	
   wp_reset_postdata();
	
	return $html;
}


function getExcerptById( $post_id ){
	
	$the_post 		= get_post($post_id); 
	$the_excerpt 	= $the_post->post_content;
	$excerpt_length = 50; 
	$the_excerpt 	= strip_tags(strip_shortcodes($the_excerpt)); 
	$words 			= explode(' ', $the_excerpt, $excerpt_length + 1);
	
	if( count( $words ) > $excerpt_length ) {

		array_pop($words);
		array_push($words, '...');
		$the_excerpt = implode(' ', $words);
		
	}
	
	$the_excerpt = '<p>' . $the_excerpt . '</p>';
	
	return $the_excerpt;
}


add_action( 'wp_enqueue_scripts', 'video_js_library' );

function video_js_library() {
	
	if( ! is_admin() ){
	
	wp_register_style( 'video-css',  plugins_url( 'assets/css/style.css' , __FILE__ ) );
	wp_enqueue_style( 'video-css' );
	
	wp_register_script( 'jquery-video-script', plugins_url( 'assets/js/scripts.js' , __FILE__ ) , array( 'jquery' )  );
	wp_enqueue_script( 'jquery-video-script' );
	
	}
	
}

function wp_post_thumbnail_class_filter_reset( $output ) {
	 $output = preg_replace('/class=".*?"/', '', $output);
    return $output;
}

function get_first_oembed($id) {

    $meta = get_post_custom($id);

    foreach ($meta as $key => $value)
        if (false !== strpos($key, 'oembed'))
            return $value[0];
}
