jQuery(document).ready(function($) {
	
	if( $( '#abdominals-exercises' ).is(':checked') ){
		$( '#abdominals-exercises-sub' ).show();
		$( '#upsize-abdominals-exercises' ).prop('checked', true );
    }

    $( '#abdominals-exercises' ).click( function(){
    	
    	 if( $(this).is(':checked') ){
    		 $( '#abdominals-exercises-sub' ).show();
    		 $( '#upsize-abdominals-exercises' ).prop('checked', true );
    	 }else{
    		 $( '#abdominals-exercises-sub' ).hide();
    		 $( '#upsize-abdominals-exercises' ).prop('checked', false );
    	 }
    	
    });
    
});
