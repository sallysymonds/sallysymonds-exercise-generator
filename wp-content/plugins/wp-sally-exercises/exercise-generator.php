<?php
/* Exercise Generator 
 * Shortcode [exercise_generator fitness_level="beginner" default="Full Body Exercises,Arms Exercises" ]
 * 
 * The Default Tags & Slugs
 * 
 *  Full Body Exercises - full-body-exercises
 *  Abdominals Exercises - abdominals-exercises
 *  Upsize Abdominals Exercises - upsize-abdominals-exercises
 *  Arms Exercises - arms-exercises
 *  Shoulders Exercises - shoulders-exercises
 *  Legs Exercises - legs-exercises
 *  Chest Exercises - chest-exercises
 *  Biceps Exercises - biceps-exercises
 *  Calves Exercises - calves-exercises
 *  Thighs & Butt Exercises  - thighs-butt-exercises
 *  Back Exercises - back-exercises
 *  Triceps Exercises - triceps-exercises
 *  
 *  The Fitness level Tags & Slugs
 *  Beginner  - beginner-exercises-level
 *  Intermediate - intermediate-exercises-level
 *  Advanced - advanced-exercises-level
 *  
 * */

function exercise_generator_func( $atts ){
	
	extract( shortcode_atts( array(
			'default' 		=> '', 
			'fitness_level'	=> '' // beginner , intermediate , advanced 
		), $atts , 'exercise_egenerator' ) 
	);
	
	$default_tags_slugs = array(
			'Full Body Exercises' 	=> 'full-body-exercises',
			'Abdominals Exercises' 	=> 'abdominals-exercises',
			'Upsize Abdominals Exercises' => 'upsize-abdominals-exercises',
			'Arms Exercises'		=> 'arms-exercises',
			'Shoulders Exercises' 	=> 'shoulders-exercises',
			'Legs Exercises' 		=> 'legs-exercises',
			'Chest Exercises' 		=> 'chest-exercises',
			'Biceps Exercises' 		=> 'biceps-exercises',
			'Calves Exercises' 		=> 'calves-exercises',
			'Thighs & Butt Exercises' => 'thighs-butt-exercises-2',
			'Back Exercises' 		=> 'back-exercises',
			'Triceps Exercises' 	=> 'triceps-exercises'
	);		
	
	if( ! $_POST ){
		$tags_exercises  = explode( ",", $default );
		
	    if( ! empty( $tags_exercises ) ){
	    	foreach ( $tags_exercises as $val ){
	    			$exercises_slug = trim( $val ); 
	    			if( array_key_exists(  $exercises_slug  , $default_tags_slugs)  ){
	    				$tags_in[] = $default_tags_slugs[ $exercises_slug ];
	    			}	
	    	}
	    }
	    
	    $fitness_levels = array(
	    				'Beginner' 		=> 'beginner-exercises-level',
	    				'Intermediate' 	=> 'intermediate-exercises-level',
	    				'Advanced' 		=> 'advanced-exercises-level',	
	    );
	    
	    if( array_key_exists( $fitness_level , $fitness_levels) ){
	    	$tags_fitness_level = $fitness_levels[ $fitness_level ];	
	    }else{
	    	$tags_fitness_level = '';	
	    }
	    
	}else{

		 if( !empty( $_POST['tags_exercises'] )){
		 	$tags_in = array_values( $_POST['tags_exercises'] );	
		 }else{
		 	$tags_in = '';
		 }
		 
		 $tags_fitness_level = $_POST[ '_fitness_level' ];
	}
	
	
    $params[ 'fitness_level' ] = $tags_fitness_level;
    $params[ 'exercises_slugs' ] = $tags_in;
	
    
   
    
    
    return queryExerciseGenerator( $params ); 
    
    
    
	
}

add_shortcode( 'exercise_generator', 'exercise_generator_func' );


function queryExerciseGenerator( $params ){
	
	$args = array(
	  'post_type' 		=> 'exercises',	
	  'posts_per_page' 	=> -1,	
	  'numberposts' 	=> -1,
      'post_status'   	=> 'any',
	  'orderby'       	=> 'title',
	  'order' 		  	=> 'ASC'
    );
   
    if( ! empty( $params[ 'exercises_slugs' ] ) ){
   		$args['tag_slug__in'] = $params[ 'exercises_slugs' ] ;
    }
   
	if( ! empty( $params[ 'fitness_level' ] ) ){
   		$args['meta_query'] = array(
										array(
											'key'     => '_fitness_level',
											'value'   => $params[ 'fitness_level' ] ,
											'compare' => '=',
										), 
									);
    }
 	
    
    $videoPosts = new WP_Query( $args );
   
    $exerciseshtml = '';
    $exerciseshtml .= '<form action="'. get_permalink() .'" method="post">';
    $exerciseshtml .= '<div id="le_body_row_1_col_1_el_2" data-style="" class="element-container cf"><div class="element"> What is your fitness level?  </div></div>';
    
    $beginner = ( 'beginner-exercises-level' == $_REQUEST['_fitness_level'] ? 'checked' : '' );
	$intermediate = ( 'intermediate-exercises-level' == $_REQUEST['_fitness_level'] ? 'checked': '' );
	$advanced = ( 'advanced-exercises-level' == $_REQUEST['_fitness_level'] ? 'checked' : '' );
    
    $exerciseshtml .= '<ul class="tag-fitness-level">';
	$exerciseshtml .= '<li><input type="radio" name="_fitness_level" value="beginner-exercises-level" '.$beginner.'> Beginner</li>';
	$exerciseshtml .= '<li><input type="radio" name="_fitness_level" value="intermediate-exercises-level" '.$intermediate.'> Intermediate</li>';
	$exerciseshtml .= '<li><input type="radio" name="_fitness_level" value="advanced-exercises-level" '.$advanced.'> Advanced</li>';
	$exerciseshtml .= '</ul>';
    
    $exerciseshtml .= '<div id="le_body_row_1_col_1_el_2" data-style="" class="element-container cf"><div class="element"> Which body part/s would you like to exercise today: </div></div>';
    $exerciseshtml .= '<div id="le_body_row_1_col_1_el_3" data-style="" class="element-container cf"><div class="element"> <div style="width:100%;text-align: left;" class="op-text-block">';
    
    
    
    $beginner = ( 'beginner-exercises-level' == $_REQUEST['_fitness_level'] ? 'checked' : '' );
    
    $tags_exercises = $_REQUEST['tags_exercises'];
    
    if( !empty( $tags_exercises )){

	    if( in_array ( 'full-body-exercises' , $tags_exercises ) ){
	    	$full_body_exercises = "checked";
	    }
	    
    	if( in_array ( 'legs-exercises' , $tags_exercises ) ){
	    	$legs_exercises = "checked";
	    }
	    
    	if( in_array ( 'thighs-butt-exercises' , $tags_exercises ) ){
	    	$thighs_butt_exercises = "checked";
	    }
	    
    	if( in_array ( 'abdominals-exercises' , $tags_exercises ) ){
	    	$abdominals_exercises = "checked";
	    }
	    
    	if( in_array ( 'chest-exercises' , $tags_exercises ) ){
	    	$chest_exercises = "checked";
	    }
	    
    	if( in_array ( 'back-exercises' , $tags_exercises ) ){
	    	$back_exercises = "checked";
	    }
	    
    	if( in_array ( 'arms-exercises' , $tags_exercises ) ){
	    	$arms_exercises = "checked";
	    }
	    
    	if( in_array ( 'biceps-exercises' , $tags_exercises ) ){
	    	$biceps_exercises = "checked";
	    }
	    
    	if( in_array ( 'triceps-exercises' , $tags_exercises ) ){
	    	$triceps_exercises = "checked";
	    }
	    
    	if( in_array ( 'shoulders-exercises' , $tags_exercises ) ){
	    	$shoulders_exercises = "checked";
	    }
	    
	    if( in_array ( 'calves-exercises' , $tags_exercises ) ){
	    	$calves_exercises = "checked";
	    }
    }
    
    
    $exerciseshtml .= '<ul class="tag-search">';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="full-body-exercises" '.$full_body_exercises.'> Full Body</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="legs-exercises" '.$legs_exercises.'> Legs</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="thighs-butt-exercises" '.$thighs_butt_exercises.'> Thighs &amp; Butt</li>';
	$exerciseshtml .= '<li><input id="abdominals-exercises" type="checkbox" name="tags_exercises[]" value="abdominals-exercises" '.$abdominals_exercises.'> Abdominals </li>';
	
	$exerciseshtml .= '<li style="display:none" id="abdominals-exercises-sub"> <input id="upsize-abdominals-exercises" type="checkbox" name="tags_exercises[]" value="upsize-abdominals-exercises" '.$abdominals_exercises.'> Upsize Abdominals </li>';
	
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="chest-exercises" '.$chest_exercises.'> Chest</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="back-exercises" '.$back_exercises.'> Back</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="arms-exercises" '.$arms_exercises.'> Arms</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="biceps-exercises" '.$biceps_exercises.'> Biceps</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="triceps-exercises" '.$triceps_exercises.'> Triceps</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="shoulders-exercises" '.$shoulders_exercises.'> Shoulders</li>';
	$exerciseshtml .= '<li><input type="checkbox" name="tags_exercises[]" value="calves-exercises" '.$calves_exercises.'> Calves</li>';
	$exerciseshtml .= '</ul>';
	$exerciseshtml .= '<p><input type="submit" class="search-now" value="Search"></p>';
	$exerciseshtml .= '</div> </div></div>';
	$exerciseshtml .= '</form>';
	    
	
	$exerciseshtml .= '<div class="video_wrapper">';
	$exerciseshtml .= '<div class="video_menus">';
	
	if( $videoPosts->have_posts() ) {
	$exerciseshtml .= '<div class="sally_video_list">';
	$exerciseshtml .= '<ul>';
	
	 foreach ( $videoPosts->posts as $post ) {
	 	
	 	$args = array(
                        'post_type' => 'attachment',
                        'numberposts' => -1,
                        'post_status' => null,
                        'post_parent' => $post->ID
                    ); 
                    
        $attachments = get_posts( $args );
        $imgsrc = "";
        
        if ( $attachments ){
           foreach ( $attachments as $attachment) {
               //$imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               $imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               //$imgsrc = wp_get_attachment_image($attachment->ID, 'full'); //thumbnail, medium, large or full)
        		break;
               }
			}
            
	 		$exerciseshtml .= '<li>';

	 		$matches = array();
            preg_match('|https://www.youtube.com/watch\?v=([_,a-zA-Z0-9,-]+)|', $post->post_content, $matches);
            //echo '<pre>'; print_r( $matches ); echo '</pre>';
            
            $v = $matches[1];
            	
	 		if( $imgsrc != '' ){
	 			//$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			//$exerciseshtml .= '<a href="https://www.youtube.com/watch?v='.$v.'" title="' . esc_attr( $post->post_title ) . '">';
	 			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
            	$exerciseshtml .= __( get_the_post_thumbnail( $post->ID , 'thumbnail' ), 'sallysymonds' );
            	$exerciseshtml .=  '</a>';	
	 		}

	 			//$exerciseshtml .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			$exerciseshtml .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$exerciseshtml .=  '</a>';
            	$exerciseshtml .=   __( getExcerptById( $post->ID  ) , 'sallysymonds' );
            	 
  				
            	//$exerciseshtml .=   '<p><a href="https://www.youtube.com/watch?v='.$v.'" >'. __('Watch Now','sallysymonds') .'</a></p>';
            	
        $exerciseshtml .= '</li>';
	 }
	  
	$exerciseshtml .= '</ul>';
	/* pagination
	$big = 999999999;
	$exerciseshtml .=  paginate_links( array(
				'base' 	  => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'   => '?paged=%#%',
				'current'  => max( 1, get_query_var('paged') ),
				'total'    => $videoPosts->max_num_pages,
			    'add_args' => array(
									 'excercise' => esc_sql( $_REQUEST['excercise'] ) ,
			    					 'skill' 	 => esc_sql( $_REQUEST['skill'] ) , 
			    					 'week'		 => esc_sql( $params['week'] ) 
									)
			) );
		*/	
	$exerciseshtml .= '</div><!-- end  -->';	
	
	}else{
		$exerciseshtml .=  '<p>'. __( 'Sorry, no posts matched your criteria.' ). '</p>';
	}
	
	$exerciseshtml .= '</div><!-- end video_menus -->';
	$exerciseshtml .= '</div><!-- end video_wrapper -->';
	
	
	
	wp_reset_postdata();
	
	return $exerciseshtml;
    
    
}

/* Custom MetaBox*/


add_action( 'add_meta_boxes', 'add_fitness_level_metaboxes' );
	
function add_fitness_level_metaboxes() {
	    add_meta_box('fitness_level_form', 'Fitness Level', 'fitness_level_form', 'exercises', 'side', 'default');
}	

function fitness_level_form(){
	
	global $post;
	
	_e( '<input type="hidden" name="fitness_level_form_meta_noncename" id="fitness_level_form_meta_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />');
	
	$fitness_level	= get_post_meta( $post->ID, '_fitness_level', true );
	
	$beginner = ( 'beginner-exercises-level' == $fitness_level ? 'selected' : '' );
	$intermediate = ( 'intermediate-exercises-level' == $fitness_level ? 'selected': '' );
	$advanced = ( 'advanced-exercises-level' == $fitness_level ? 'selected' : '' );
	
	$table = '<table style="width: 100%;">';
	$table .= '<tr>';
	$table .= '<td>';
	$table .= '<select name="_fitness_level" style="width: 100%;">';
  	$table .= '<option value="beginner-exercises-level" '.$beginner.'>'.__('Beginner','sallysymonds').'</option>';
  	$table .= '<option value="intermediate-exercises-level" '.$intermediate.'>'.__('Intermediate','sallysymonds').'</option>';
  	$table .= '<option value="advanced-exercises-level" '.$advanced.'>'.__('Advanced','sallysymonds').'</option>';
  	$table .= '</select>';
 	$table .= '</td>';
	$table .= '</tr>';
    $table .= '</table>';
    _e( $table );
	
}

function save_fitness_level_form($post_id, $post) {
	
	if ( !wp_verify_nonce( $_POST['fitness_level_form_meta_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}
	
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	
	$fitness_level_meta['_fitness_level'] 		= $_POST['_fitness_level'];
	
	foreach ( $fitness_level_meta as $key => $value ) { 
		
		if( $post->post_type == 'revision' ) return; 
		$value = implode(',', (array)$value); 
		if(get_post_meta($post->ID, $key, FALSE)) { 
			update_post_meta($post->ID, $key, $value);
		} else { 
			add_post_meta($post->ID, $key, $value);
		}
		
		if(!$value) delete_post_meta($post->ID, $key); 
	}

}

add_action('save_post', 'save_fitness_level_form', 1, 2); // save the custom fields

add_action( 'wp_enqueue_scripts', 'exercise_generator_js_library' );

function exercise_generator_js_library() {
	
	if( ! is_admin() ){
	
		//wp_register_style( 'exercise-generator-css',  plugins_url( 'assets/css/style.css' , __FILE__ ) );
		//wp_enqueue_style( 'exercise-generator-css' );
	
		wp_register_script( 'jquery-video-script', plugins_url( 'assets/js/scripts.js' , __FILE__ ) , array( 'jquery' )  );
		wp_enqueue_script( 'exercise-generator-script' );
	
	}
	
}

