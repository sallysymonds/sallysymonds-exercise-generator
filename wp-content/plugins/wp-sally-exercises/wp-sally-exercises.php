<?php
 /**
  * Plugin Name: Sally Symonds Custom  Post Type Exercise Plugin
  * Plugin URI: http://www.sallysymonds.com
  * Description: Sally Symonds Custom  Post Type Excercise Integration
  * Version: 1.0
  * Author: Netseek
  * Author URI:  http://netseek.com.au
  */

include_once 'exercise-generator.php';


function exercises_form() {

	$labels = array(
		'name'                => _x( 'Exercises', 'Post Type General Name', 'sallysymonds' ),
		'singular_name'       => _x( 'Exercises', 'Post Type Singular Name', 'sallysymonds' ),
		'menu_name'           => __( 'Exercises', 'sallysymonds' ),
		'parent_item_colon'   => __( 'Parent Item:', 'sallysymonds' ),
		'all_items'           => __( 'All Items', 'sallysymonds' ),
		'view_item'           => __( 'View Item', 'sallysymonds' ),
		'add_new_item'        => __( 'Add New Item', 'sallysymonds' ),
		'add_new'             => __( 'Add New', 'sallysymonds' ),
		'edit_item'           => __( 'Edit Item', 'sallysymonds' ),
		'update_item'         => __( 'Update Item', 'sallysymonds' ),
		'search_items'        => __( 'Search Item', 'sallysymonds' ),
		'not_found'           => __( 'Exercises Not found', 'sallysymonds' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'sallysymonds' ),
	);
	$args = array(
		'label'               => __( 'exercises', 'sallysymonds' ),
		'description'         => __( 'Exercises', 'sallysymonds' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail','custom-fields' ), 
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'taxonomies'          => array( 'exercises_category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		//'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
	    //'register_meta_box_cb' => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	
	register_post_type( 'exercises', $args );
	//flush_rewrite_rules();

}

// Hook into the 'init' action
add_action( 'init', 'exercises_form', 0 );

function sally_exercises_func( $atts ){
	
	extract( shortcode_atts( array(
			'category' 		=> '',
			'ver'	=> ''
		), $atts , 'exercises' ) 
	);
	
	if($ver == 2){
		return getExercisesPost2( $category );
	}
	
	return getExercisesPost( $category );
	
}

add_shortcode( 'exercises', 'sally_exercises_func' );


function sally_taxonomies_exercise() {
  $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Exercise Categories' ),
    'all_items'         => __( 'All Exercise Categories' ),
    'parent_item'       => __( 'Parent Exercise Category' ),
    'parent_item_colon' => __( 'Parent Exercise Category:' ),
    'edit_item'         => __( 'Edit Exercise Category' ), 
    'update_item'       => __( 'Update Exercise Category' ),
    'add_new_item'      => __( 'Add New Exercise Category' ),
    'new_item_name'     => __( 'New Exercise Category' ),
    'menu_name'         => __( 'Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'public' => true,
    //'show_ui' => true,
  	//'rewrite' => array('slug' => 'exercises_category_test', 'with_front' => true)
  );
  register_taxonomy( 'exercises_category', 'exercises', $args );
  //flush_rewrite_rules();
}
add_action( 'init', 'sally_taxonomies_exercise', 0 );


function getExercisesPost( $category ){
	
	$args = array(
	  'post_type' => 'exercises',	
	  'posts_per_page' => -1,	// for pagination
	  'numberposts' => -1,
	  'exercises_category' => $category ,
      'post_status'   => 'any',
	  'orderby'       => 'title',
	  'order' 		  => 'ASC'
   );
	
   
   $videoPosts = new WP_Query( $args );
   
    $exerciseshtml = '';
	
	$exerciseshtml .= '<div class="video_wrapper">';
	$exerciseshtml .= '<div class="video_menus">';
	
	if( $videoPosts->have_posts() ) {
	$exerciseshtml .= '<div class="sally_video_list">';
	$exerciseshtml .= '<ul>';
	
	 foreach ( $videoPosts->posts as $post ) {
	 	
	 	$args = array(
                        'post_type' => 'attachment',
                        'numberposts' => -1,
                        'post_status' => null,
                        'post_parent' => $post->ID
                    ); 
                    
        $attachments = get_posts( $args );
        $imgsrc = "";
        
        if ( $attachments ){
           foreach ( $attachments as $attachment) {
               //$imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               $imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               //$imgsrc = wp_get_attachment_image($attachment->ID, 'full'); //thumbnail, medium, large or full)
        		break;
               }
			}
            
	 		$exerciseshtml .= '<li style="position:relative;overflow:hidden;">';
			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '" style="position: absolute;display: block;width: 100%;top: 0;left: 0;height: 200px;">';
			$exerciseshtml .=  '</a>';	

	 		$matches = array();
            preg_match('|https://www.youtube.com/watch\?v=([_,a-zA-Z0-9,-]+)|', $post->post_content, $matches);
            //echo '<pre>'; print_r( $matches ); echo '</pre>';
            
            $v = $matches[1];
            	
	 		if( $imgsrc != '' ){
	 			//$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			//$exerciseshtml .= '<a href="https://www.youtube.com/watch?v='.$v.'" title="' . esc_attr( $post->post_title ) . '">';
	 			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
            	$exerciseshtml .= __( get_the_post_thumbnail( $post->ID , 'thumbnail' ), 'sallysymonds' );
            	$exerciseshtml .=  '</a>';	
	 		}

	 			//$exerciseshtml .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			$exerciseshtml .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$exerciseshtml .=  '</a>';
            	$exerciseshtml .=   __( getExcerptById( $post->ID  ) , 'sallysymonds' );
            	 
  				
            	//$exerciseshtml .=   '<p><a href="https://www.youtube.com/watch?v='.$v.'" >'. __('Watch Now','sallysymonds') .'</a></p>';
            	
        $exerciseshtml .= '</li>';
	 }
	  
	$exerciseshtml .= '</ul>';
	/* pagination
	$big = 999999999;
	$exerciseshtml .=  paginate_links( array(
				'base' 	  => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'   => '?paged=%#%',
				'current'  => max( 1, get_query_var('paged') ),
				'total'    => $videoPosts->max_num_pages,
			    'add_args' => array(
									 'excercise' => esc_sql( $_REQUEST['excercise'] ) ,
			    					 'skill' 	 => esc_sql( $_REQUEST['skill'] ) , 
			    					 'week'		 => esc_sql( $params['week'] ) 
									)
			) );
		*/	
	$exerciseshtml .= '</div><!-- end  -->';	
	
	}else{
		$exerciseshtml .=  '<p>'. __( 'Sorry, no posts matched your criteria.' ). '</p>';
	}
	
	$exerciseshtml .= '</div><!-- end video_menus -->';
	$exerciseshtml .= '</div><!-- end video_wrapper -->';
	
	
	
	wp_reset_postdata();
	
	return $exerciseshtml;
	
	
}




function getExercisesPost2( $category ){
	
	$args = array(
	  'post_type' => 'exercises',	
	  'posts_per_page' => -1,	// for pagination
	  'numberposts' => -1,
	  //'exercises_category' => $category ,
      'post_status'   => 'any',
	  'orderby'       => 'title',
	  'order' 		  => 'ASC'
   );
   
   //Added by noel
	$tags_cats = explode(',',$_GET['tags']);
	$tags_cats = array_diff($tags_cats, array(''));
	
	if(empty($tags_cats)){
		$args['tag_slug__in'] = array ('not-found');
	}else{
		$args['tag_slug__in'] = $tags_cats;
	}
	
	//$cats = post_type_tags('exercises_category'); 
	//echo '<pre>';
	//print_r($cats);
	//echo '</pre>';
	
   //Added by noel
   
   $videoPosts = new WP_Query( $args );
   
    $exerciseshtml = '';
	
	$exerciseshtml .= '<div class="video_wrapper">';
	$exerciseshtml .= '<div class="video_menus">';
	
	if( $videoPosts->have_posts() ) {
	$exerciseshtml .= '<div class="sally_video_list">';
	$exerciseshtml .= '<ul>';
	
	 foreach ( $videoPosts->posts as $post ) {
	 	
	 	$args = array(
                        'post_type' => 'attachment',
                        'numberposts' => -1,
                        'post_status' => null,
                        'post_parent' => $post->ID
                    ); 
                    
        $attachments = get_posts( $args );
        $imgsrc = "";
        
        if ( $attachments ){
           foreach ( $attachments as $attachment) {
               //$imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               $imgsrc = wp_get_attachment_image($attachment->ID, array(40,40), $icon = false);
               //$imgsrc = wp_get_attachment_image($attachment->ID, 'full'); //thumbnail, medium, large or full)
        		break;
               }
			}
            
	 		$exerciseshtml .= '<li style="position: relative;">';
			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '" style="position:absolute;display:block;">';
			$exerciseshtml .=  '</a>';

	 		$matches = array();
            preg_match('|https://www.youtube.com/watch\?v=([_,a-zA-Z0-9,-]+)|', $post->post_content, $matches);
            //echo '<pre>'; print_r( $matches ); echo '</pre>';
            
            $v = $matches[1];
            	
	 		if( $imgsrc != '' ){
	 			//$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			//$exerciseshtml .= '<a href="https://www.youtube.com/watch?v='.$v.'" title="' . esc_attr( $post->post_title ) . '">';
	 			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
            	$exerciseshtml .= __( get_the_post_thumbnail( $post->ID , 'thumbnail' ), 'sallysymonds' );
            	$exerciseshtml .=  '</a>';	
	 		}

	 			//$exerciseshtml .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$exerciseshtml .= '<a href="' . get_permalink( $post->ID ). '" title="' . esc_attr( $post->post_title ) . '">';
	 			$exerciseshtml .= '<span>'. __( $post->post_title , 'sallysymonds' ) . '</span>';
	 			$exerciseshtml .=  '</a>';
            	$exerciseshtml .=   __( getExcerptById( $post->ID  ) , 'sallysymonds' );
            	 
  				
            	//$exerciseshtml .=   '<p><a href="https://www.youtube.com/watch?v='.$v.'" >'. __('Watch Now','sallysymonds') .'</a></p>';
            	
        $exerciseshtml .= '</li>';
	 }
	  
	$exerciseshtml .= '</ul>';
	/* pagination
	$big = 999999999;
	$exerciseshtml .=  paginate_links( array(
				'base' 	  => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'   => '?paged=%#%',
				'current'  => max( 1, get_query_var('paged') ),
				'total'    => $videoPosts->max_num_pages,
			    'add_args' => array(
									 'excercise' => esc_sql( $_REQUEST['excercise'] ) ,
			    					 'skill' 	 => esc_sql( $_REQUEST['skill'] ) , 
			    					 'week'		 => esc_sql( $params['week'] ) 
									)
			) );
		*/	
	$exerciseshtml .= '</div><!-- end  -->';	
	
	}else{
		$exerciseshtml .=  '<p>'. __( 'Sorry, no posts matched your criteria.' ). '</p>';
	}
	
	$exerciseshtml .= '</div><!-- end video_menus -->';
	$exerciseshtml .= '</div><!-- end video_wrapper -->';
	
	
	
	wp_reset_postdata();
	
	return $exerciseshtml;
	
}//endfunction 



function post_type_tags( $post_type = '' ) {
    global $wpdb;

    if ( empty( $post_type ) ) {
        $post_type = get_post_type();
    }

    return $wpdb->get_results( $wpdb->prepare( "
        SELECT COUNT( DISTINCT tr.object_id ) 
            AS count, tt.taxonomy, tt.description, tt.term_taxonomy_id, t.name, t.slug, t.term_id 
        FROM {$wpdb->posts} p 
        INNER JOIN {$wpdb->term_relationships} tr 
            ON p.ID=tr.object_id 
        INNER JOIN {$wpdb->term_taxonomy} tt 
            ON tt.term_taxonomy_id=tr.term_taxonomy_id 
        INNER JOIN {$wpdb->terms} t 
            ON t.term_id=tt.term_taxonomy_id 
        WHERE p.post_type=%s 
            AND tt.taxonomy='post_tag' 
        GROUP BY tt.term_taxonomy_id 
        ORDER BY count DESC
    ", $post_type ) );
}

