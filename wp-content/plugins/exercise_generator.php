<?php
/*
Plugin Name: Exercise Generator
*/




function exercises_form_generate() {

	$labels = array(
		'name'                => _x( 'Exercise Gen', 'Post Type General Name', 'sallysymonds' ),
		'singular_name'       => _x( 'Exercise Gen', 'Post Type Singular Name', 'sallysymonds' ),
		'menu_name'           => __( 'Exercise Gen', 'sallysymonds' ),
		'parent_item_colon'   => __( 'Parent Item:', 'sallysymonds' ),
		'all_items'           => __( 'All Items', 'sallysymonds' ),
		'view_item'           => __( 'View Item', 'sallysymonds' ),
		'add_new_item'        => __( 'Add New Item', 'sallysymonds' ),
		'add_new'             => __( 'Add New', 'sallysymonds' ),
		'edit_item'           => __( 'Edit Item', 'sallysymonds' ),
		'update_item'         => __( 'Update Item', 'sallysymonds' ),
		'search_items'        => __( 'Search Item', 'sallysymonds' ),
		'not_found'           => __( 'Exercises Not found', 'sallysymonds' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'sallysymonds' ),
	);
	$args = array(
		'label'               => __( 'exercise gen', 'sallysymonds' ),
		'description'         => __( 'Exercise Gen', 'sallysymonds' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail','custom-fields' ), 
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'taxonomies'          => array( 'exercise-generator', 'bodyparts','eg_category' ),
		'hierarchical'        => false,
		'public'              => true,
		//'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
	    //'register_meta_box_cb' => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	
	//register_post_type( 'exercise_gen', $args );
	register_post_type( 'exercise-generator', $args );
	//flush_rewrite_rules();

}

// Hook into the 'init' action
add_action( 'init', 'exercises_form_generate', 0 );





function eg_taxonomies() {
  $labels = array(
    'name'              => _x( 'EG Category', 'taxonomy general name' ),
    'singular_name'     => _x( 'EG Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search EG Categories' ),
    'all_items'         => __( 'All EG Categories' ),
    'parent_item'       => __( 'Parent EG Category' ),
    'parent_item_colon' => __( 'Parent EG Category:' ),
    'edit_item'         => __( 'Edit EG Category' ), 
    'update_item'       => __( 'Update EG Category' ),
    'add_new_item'      => __( 'Add New EG Category' ),
    'new_item_name'     => __( 'New EG Category' ),
    'menu_name'         => __( 'EG Category' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'public' => true,
  	'show_admin_column' => true
    //'show_ui' => true,
  	//'rewrite' => array('slug' => 'exercises_category_test', 'with_front' => true)
  );
  register_taxonomy( 'eg_category', 'exercise-generator', $args );
  //flush_rewrite_rules(); 
  
  //register_taxonomy_for_object_type('post_tag', 'exercise_gen');
  
}//end
add_action( 'init', 'eg_taxonomies', 0 );



add_action( 'restrict_manage_posts', 'EG_Filter' );
function EG_Filter() {
	//global $typenow;
	$taxonomy = 'eg_category';
	if( $typenow != "page" && $typenow != "post" ){
		$filters = array($taxonomy);
		foreach ($filters as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
			echo "<option value=''>Show All $tax_name</option>";
			foreach ($terms as $term) { echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .'</option>'; }
			echo "</select>";
		}
	}
}//enf Func



function sally_taxonomies_bodypart() {
  $labels = array(
    'name'              => _x( 'Body Parts', 'taxonomy general name' ),
    'singular_name'     => _x( 'Body Part', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Body Parts' ),
    'all_items'         => __( 'All Body Parts' ),
    'parent_item'       => __( 'Parent Body Part' ),
    'parent_item_colon' => __( 'Parent Body Part:' ),
    'edit_item'         => __( 'Edit Body Part' ), 
    'update_item'       => __( 'Update Body Part' ),
    'add_new_item'      => __( 'Add New Body Part' ),
    'new_item_name'     => __( 'New Body Part' ),
    'menu_name'         => __( 'Body Parts' ),
  );
  
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'public' => true,
    'show_ui' => true,
  	'show_admin_column' => true
  	
  );
  register_taxonomy( 'bodyparts', 'exercise-generator', $args );
  //flush_rewrite_rules();
}

add_action( 'init', 'sally_taxonomies_bodypart', 0 );
