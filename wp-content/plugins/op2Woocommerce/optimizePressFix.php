<?php
/*
Plugin Name: OptimizePress 2 fix for Woocommerce plugin
Plugin URI: http://www.optimizepress.com
Description: This plugin adds Woocommerce support to OptimizePress theme
Version: 1.0
Author: Zvonko Biskup
Author URI: http://www.optimizepress.com
*/

add_action('op_init', 'op_woo_compat_init');


function op_woo_compat_init()
{
    remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
    remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

    add_action('woocommerce_before_main_content', 'op_theme_wrapper_start', 10);
    add_action('woocommerce_after_main_content', 'op_theme_wrapper_end', 10);
    add_action('woocommerce_sidebar', 'op_theme_sidebar', 1);

    //declaring woocommerce support
    add_theme_support('woocommerce');
}

function op_theme_wrapper_start()
{
    echo '<div class="page type-page main-content-area">';
}

function op_theme_wrapper_end()
{
    echo '</div>';
}

function op_theme_sidebar()
{
    op_sidebar();
}

