<?php 
 /**
  * Plugin Name: Sally Symonds Custom Plugins
  * Plugin URI: http://www.sallysymonds.com
  * Description: Sally Symonds Integration
  * Version: 1.0
  * Author: Netseek
  * Author URI:  http://netseek.com.au
  */

add_shortcode( 'diary', 'sally_foodDiary' );

function sally_foodDiary(){
	ob_start();
	//require_once  'includes/install.diary.php';
	require_once  'includes/tpl.diary.php';
	
	$rdata = ob_get_contents();
	ob_clean();
	return $rdata;
}

add_action( 'wp_enqueue_scripts', 'diary_js_library' );

function diary_js_library() {
	
	wp_register_script( 'jquery-tools-script', 'http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js' , array( 'jquery' )  );
	wp_enqueue_script( 'jquery-tools-script' );
	
}

add_shortcode( 'progress', 'sally_progress' );

function sally_progress(){
	
	ob_start();
	
	require_once  'includes/tpl.progress.php';
	
	$rdata = ob_get_contents();
	ob_clean();
	return $rdata;
	
	//return html_progress(); 
}





?>