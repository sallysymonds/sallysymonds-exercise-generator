<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'My Diary', 'min-height=100%,min-width=100%');
        mywindow.document.write('<html><head><title>My Diary</title>');
       	mywindow.document.write('</head><body >');
        mywindow.document.write(data);
       	mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();

        return true;
    }


    $(document).ready(function(){
		document.getElementById('right_pane_new').style.display="none";
		document.getElementById('inner_content').style.width="90%";
    });
 
		 function open_popup(x){
			document.getElementById(x).style.display='block';
		  }			 

		 function close_popup(x){
			document.getElementById(x).style.display='none';
		  }			


			$(document).ready(function(){
				$('#ex_amt[title]').tooltip();
				$('#ex_eff[title]').tooltip();
			});
		  
</script>
<style>
    /* tooltip styling. by default the element to be styled is .tooltip  */
  .tooltip {
    display:none;
    background:black;
    font-size:12px;
    height:auto;
    width:auto;
    padding:6px;
    color:#eee;
  }
    /* style the trigger elements */
  #demo img {
    border:0;
    cursor:pointer;
    margin:0 1px;
  }

</style>

<?php

global $wpdb;

$userid = esc_sql( get_current_user_id() );
$userId = esc_sql( get_current_user_id() );
$today = date("j F Y");  

$meal = esc_sql( $_POST['meal'] );
$date = esc_sql( $_POST['date'] );
$day = esc_sql( $_POST['day'] );
$meal_time = esc_sql( $_POST['meal_time'] );
$exercise_amount = esc_sql( $_POST['exercise_amount'] );
$exercise_effort = esc_sql( $_POST['exercise_effort'] );
$id = esc_sql( $_POST['id'] );
$update_food = esc_sql( $_POST['update_food'] );
$update_exercise = esc_sql( $_POST['update_exercise'] );

$url_pager = esc_sql( $_POST['pager'] );

if($userid != 0){

/************************************************************************************/
//calculate the current week for this particular user
        
        $results   	= $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}users WHERE id='{$userId}'" );
		$email_lywl = "";
		
		foreach($results as $row){
			$email_lywl = $row->user_email;
		}
			
//calculate the current week for this particular user
			$week = datediff('ww', $date_started, $today, false) + 1;
			
	/* table not exist into the original DB #__lywl
	$db->setQuery("SELECT * FROM #__lywl WHERE email='".$$email_lywl."'");
			$db->query();
			$results = $db->loadObjectList();
			$week = 0;
				foreach($results as $row)
				{
					$registered_date = $row->registerDate;
					$date_started = date("j F Y ", strtotime($registered_date));
				
					//$week = datediff('ww', $date_started, $today, false) + 1;
					$week = getWeek($registered_date);
			}
			
			*/
			
			$results   	= $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}lywl WHERE email='".$email_lywl."'" );
			$week = 0;
			foreach($results as $row){
					$registered_date = $row->registerDate;
					$date_started = date("j F Y ", strtotime($registered_date));	
					$week = getWeek($registered_date);
			}
			
			

/***************************************SAVE FORM DATA INTO FOOD DIARY TABLE*********************************************/	
	
//if(isset( $update_food ) ){
if( ! empty ( $update_food ) ){
	if($id == ""){
		//$db->setQuery("INSERT INTO food_diary (week, date, day,userID, meal, exercise_amount, exercise_effort, total, meal_time ) VALUES
		//( '".$week."', '".$date."', '".$day."', '".$userId."', '".$meal."', '".$exercise_amount."', '".$exercise_effort."', '".$total."', '".$meal_time."' )");		
		//$db->query();
		
		$query = "INSERT INTO {$wpdb->prefix}food_diary (week, date, day,userID, meal, exercise_amount, exercise_effort, total, meal_time ) VALUES
				( '".$week."', '".$date."', '".$day."', '".$userId."', '".$meal."', '".$exercise_amount."', '".$exercise_effort."', '".$total."', '".$meal_time."' )";
		$sql = $wpdb->query( $query );
		
		echo "<div class='weight_error'>New meal submitted: Meal:".$meal.", for Week:".$date."</div>";
		//avoid resubmit on page refresh
		//header('Location: https://sallysymonds.com.au/index.php?option=com_jumi&view=application&fileid=8'.$url_pager);
		wp_redirect( get_permalink() ); 
		exit;
		
	} else {
		
		//$db->setQuery("UPDATE food_diary set  week='".$week."', date='".$date."', day='".$day."', userID='".$userId."', 
		//meal_time='".$meal_time."', meal='".$meal."' WHERE id='".$id."'");		
		//$db->query();	
		
		$query = "UPDATE {$wpdb->prefix}food_diary set  week='".$week."', date='".$date."', day='".$day."', userID='".$userId."',meal_time='".$meal_time."', meal='".$meal."' WHERE id='".$id."'";
		$sql   = $wpdb->query( $query );
		
		echo "<div class='weight_error'>Update submitted: Meal:".$meal.", for Week:".$date."</div>";
		//avoid resubmit on page refresh
		//header('Location: https://sallysymonds.com.au/index.php?option=com_jumi&view=application&fileid=8'.$url_pager);
		wp_redirect( get_permalink() ); 
		exit;
	}
}	
	
//if(isset($update_exercise)){
if( ! empty ( $update_exercise ) ){
	if($id == ""){
		//$enter_meal = "INSERT INTO food_diary (week, date, day,userID, meal, exercise_amount, exercise_effort, total, meal_time ) VALUES
		//( '".$week."', '".$date."', '".$day."', '".$userId."', '".$meal."', '".$exercise_amount."', '".$exercise_effort."', '".$total."', '".$meal_time."' )";		
		//mysql_query($enter_meal);	
		//echo "<div class='weight_error'>Enter a meal first..</div>";
	//} else {
		
		$found = 0;
		// $db->setQuery("SELECT * FROM food_diary WHERE userID='".$userId."' AND date='".$date."'") ;
		// $db->query();
		// $results = $db->loadObjectList();
		// $meal = "";
		// while($row = mysql_fetch_array($results)){
			// $found  = 1;
			// $meal = $meal . $row['meal'];
		// }
		
		
		    //$db->setQuery("SELECT * FROM food_diary WHERE userID='".$userId."' AND date='".$date."'") ;
			//$db->query();
			//$results = $db->loadObjectList();
			
			$results   	= $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}food_diary WHERE userID='".$userId."' AND date='".$date."'" );
			
		
			$meal = "";
				foreach($results as $row)
				{
					$found  = 1;
			$meal = $meal . $row->meal;
			}
		
		
		if($found){
			//$db->setQuery("UPDATE food_diary set exercise_effort='".$exercise_effort."', exercise_amount='".$exercise_amount."' WHERE userID='".$userId."' AND date='".$date."'") ;
			//$db->query();
			
			$query = "UPDATE {$wpdb->prefix}food_diary set exercise_effort='".$exercise_effort."', exercise_amount='".$exercise_amount."' WHERE userID='".$userId."' AND date='".$date."'";
			$sql   = $wpdb->query( $query );
			
			echo "<div class='weight_error'>Exercise entry updated</div>";
			
		} else {
			//Add dummy row to calculate		
			//$db->setQuery("INSERT INTO food_diary (week, date, day, userID, meal, exercise_amount, exercise_effort, total, meal_time ) VALUES
			//( '".$week."', '".$date."', '".$day."', '".$userId."', '', '".$exercise_amount."', '".$exercise_effort."', '', 'breakfast' )");		
			//$db->query();
			$query = "INSERT INTO {$wpdb->prefix}food_diary (week, date, day, userID, meal, exercise_amount, exercise_effort, total, meal_time ) VALUES( '".$week."', '".$date."', '".$day."', '".$userId."', '', '".$exercise_amount."', '".$exercise_effort."', '', 'breakfast' )";
			$sql   = $wpdb->query( $query );
			wp_redirect( get_permalink() ); exit;
		}
		//header('Location: /members-area/my-diary/');
		//header('Location: https://sallysymonds.com.au/index.php?option=com_jumi&view=application&fileid=8'.$url_pager);
		//wp_redirect( get_permalink() ); 
		//exit;
	}
}
?>

<?php  
$page=0;
$pager = "";
if(isset($_GET['previous'])){ $page = $_GET['previous'];  $sign = "-"; $pager = "&previous=".$_GET['previous'];} //else { $count = 0; }
if(isset($_GET['next'])){ $page = $_GET['next']; $sign = "+"; $pager = "&next=".$_GET['next'];} //else { $count = 0; }


?>
<!-- 
<img src="<?php _e( plugins_url( 'assets/images/food_diary_top.jpg'  ,  dirname( __FILE__ ) ) ); ?>">

	
<div style="height:30px; width: 107%;">
<a href="/members-area" style="float:right;"><img src="<?php _e( plugins_url( 'assets/images/back_to_db.jpg'  ,  dirname( __FILE__ ) ) ); ?>"></a>	
</div>
 -->
<div style="height:45px; width: 100%;">
<a style="background-image:url('<?php _e( plugins_url( 'assets/images/prev.jpg'  ,  dirname( __FILE__ ) ) ); ?>'); display:inline-block; width:77px; height:58px; " href="<?php _e( get_permalink() ); ?>?previous=<?php $page=$page-1; echo $page; ?>"></a>	 
<a style="background-image:url('<?php _e( plugins_url( 'assets/images/next.jpg'  ,  dirname( __FILE__ ) ) ); ?>'); display:inline-block; width:77px; height:58px; " href="<?php _e( get_permalink() ); ?>?next=<?php $page=$page+2; echo $page; ?>"></a>
</div>
		
<?php 
$page = $page-1;
//echo "<br />".$page."-------count value<br />"; ?>	


<?php	
/*************************************CONSTRUCT CALENDER***********************************************/	
//echo date("w") . "------------date <br />";
	date_default_timezone_set('Australia/Melbourne');
	//if(date("w") == 0){
	//$page = 0;
		//$pageVal = 0;
		if($sign == "-"){ $pageVal = substr($page, 1); } else if($sign == "+"){ $pageVal = $page; }
		//echo $pageVal."---------page val<br />";
		
		//echo $sign . "*******sign<br />";
		//$next_week = (6 * $pageVal);
		//if($pageVal == 1){ $next_week = $next_week - 1; }
		//if($next_week == 0){ $next_week = ""; $sign=""; }
		//echo "w".$sign.$next_week."-----------next_week<br />";
		//if($page == 0){ $next_week = 0*$page; }
		//$adjust = date("w".$sign.$next_week); //this is set to start the week on Sunday, to start on a Monday set it to $adjust = date("w") - 1
		//$adjust = date('W'.$sign.$next_week, strtotime('next monday')); 
		//$adjust = date("+7 day");
		//$adjust = date("w");
		//echo $adjust." ------adjust<br />";
	//$weekstart = 	strtotime("-".$adjust." days");
	
	
	//$date = date(); // error
	
	//$oneWeekAgo = strtotime ( '+1 week' , strtotime ( 'monday' ) ) ; 
	//echo $oneWeekAgo."----last monday<br />"; 
	//echo date ( 'Y-m-j G:i:s' , $oneWeekAgo ) . "<br />";
	
	//$weekstart = strtotime('this sunday');
	//$weekstart = strtotime( '+1 week' , strtotime ( 'sunday' ) ) ; 
	//echo $sign.$page."-----sign and page<br />";
	if($sign = "-"){ $sign = "";}
	$weekstart = strtotime( $sign.$page.' week' , strtotime( 'last sunday' )) ; 
	//$weekstart = strtotime( $sign.$page.' week' , strtotime( 'this sunday' )); 
	//echo date("d-m-Y", $weekstart)."<br/>"; //echo out the first date of the week
	//echo date("l", mktime($weekstart))."<br />";
?>
	
	
	
	


	<!--div style="margin-top:15px; display:none;">
<table class="dash_table">
<tr><td style="background:#9C3556;" ><span class="wLabel">Dash board:</span>
</td>
<td><a href="exercise-module">Exercise Plans</a></td>
<td><a href="forum">Member's Forum</a></td>
<td><a href="recipe-module">Recipes</a></td>
<td style="background:orange; color:white;"><a href="food-exercise-diary">Food dairy</a></td>
<td><a href="weekly-events">Events</a></td>
<td><a href="edit-account-details">My Profile</a></td>
</tr>
</table>
</div-->
	
	<br />
<!-- Print -->
<style>
<!--
@page { size : portrait }
/* force background colour printing in chrome browser */
body, span, div, table, tr, tbody, tfoot, td, hr { 
    -webkit-print-color-adjust: exact;
}
@media print {

 .food_div2 {font-family: "Source Sans Pro",sans-serif !important; line-height:20px; font-size: 17px;}
 .food_div2 thead th{background-color:#AA34D0; color:#fff; font-size:100%; height:54px;}
body{margin:0.5cm; color:#ccc;}
div#food_div2{position:relative;}
.food_table h3{color:#444;}
}
-->
</style>
<div class="food_div" id="food_div2" style="width:100%; font-family:'Open Sans', sans-serif; @page { size : portrait } ">
    <img src="/wp-content/uploads/2014/10/sallysymonds-logo-pink.png" alt="Sally Symonds" width="348px" height="146px" />	<br />
    <img src="/wp-content/uploads/2014/10/MyDiary-sliced_02.png" alt="My Diary" width="296px" height="57px" />	
	<table class="food_table" id="food_table" style="color:#444; text-align:center; padding:10px; margin-top: 10px; border:1px solid #ccc; font-family:'Open Sans', sans-serif !important; border-collapse:collapse;font-size:100%; font-weight:bold;">
	<colgroup>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
	</colgroup>
	<?php
	echo "<thead style='background-image:url(/wp-content/uploads/2014/10/print-repeat.png) repeat top; background-color:#AA34D0; color:#fff; font-size:100%; height:54px;'><tr style='border-bottom:1px solid #ccc;background-color:#AA34D0; color:#fff; font-size:100%; height:54px;'>";
	echo "<th class='textLarge' style='background-color:#EA0F6B; color:#fff; font-size:120%; height:54px;'>Week</th>";
	$weekly_total = 0;
	//display days in top rown
	for($i = 1; $i <= 7; $i++){
	
		$current_day = date("l", strtotime("+".$i."days", $weekstart));
		//$day_c = date("l", strtotime($page."1"."days", $weekstart));
		echo "<th>" . $current_day . "<br /><small>". date("d-m-Y", strtotime("+".$i."days", $weekstart)) . "</small></th>";
	}
	echo "</tr></thead>";
	echo "<tbody>";
	for($j = 1; $j <= 9; $j++){
		echo "<tr style='border-bottom:2px solid #AA34D0;padding:10px;'>";
		
		echo "<td class='period' style='border-bottom:2px solid #AA34D0; border-right:1px solid #ccc; background-color:#EA0F6B; font-size:100%; color:#fff;'>";
		if($j == 1){ echo "Breakfast";}
		if($j == 2){ echo "Snack";}
		if($j == 3){ echo "Lunch";}
		if($j == 4){ echo "Snack";}
		if($j == 5){ echo "Dinner";}
		if($j == 6){ echo "Snack";}
		if($j == 7){ echo "Exercise Amount";}
		if($j == 8){ echo "Exercise Effort";}
		if($j == 9){ echo "Daily Exercise effort";}
		
		echo "</td>";
		
		for($i = 1; $i <= 7; $i++){ //echo out the remainder - up to next 6 days
			$m_time = "";
			if($j == 1){ $m_time = "breakfast"; }
			if($j == 2){ $m_time = "snack1"; }
			if($j == 3){ $m_time = "lunch"; }
			if($j == 4){ $m_time = "snack2"; }
			if($j == 5){ $m_time = "dinner"; }
			if($j == 6){ $m_time = "snack3"; }
			if($j == 7){ $m_time = "physical activity"; }
			
			$d_time = "";
			if($i == 1){ $d_time = date("l", strtotime("+"."1"."days", $weekstart));; }
			if($i == 2){ $d_time = date("l", strtotime("+"."2"."days", $weekstart));; }
			if($i == 3){ $d_time = date("l", strtotime("+"."3"."days", $weekstart));; }
			if($i == 4){ $d_time = date("l", strtotime("+"."4"."days", $weekstart));; }
			if($i == 5){ $d_time = date("l", strtotime("+"."5"."days", $weekstart));; }
			if($i == 6){ $d_time = date("l", strtotime("+"."6"."days", $weekstart));; }
			if($i == 7){ $d_time = date("l", strtotime("+"."7"."days", $weekstart));; }
			
			$d_date = "";
			if($i == 1){ $d_date = date("d-m-Y", strtotime("+"."1"."days", $weekstart)); }
			if($i == 2){ $d_date = date("d-m-Y", strtotime("+"."2"."days", $weekstart)); }
			if($i == 3){ $d_date = date("d-m-Y", strtotime("+"."3"."days", $weekstart)); }
			if($i == 4){ $d_date = date("d-m-Y", strtotime("+"."4"."days", $weekstart)); }
			if($i == 5){ $d_date = date("d-m-Y", strtotime("+"."5"."days", $weekstart)); }
			if($i == 6){ $d_date = date("d-m-Y", strtotime("+"."6"."days", $weekstart)); }
			if($i == 7){ $d_date = date("d-m-Y", strtotime("+"."7"."days", $weekstart)); }
			
			/******************************GET SAVED DATA*************************************/
			$saved_meal="";
			$exercise_amount = "";
			$exercise_effort = "";
			$id = "";
			$date = "";
			$day = "";
			//$food_display = "SELECT * FROM food_diary WHERE userId='".$userId."' AND meal_time = '".$m_time."' AND date='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."'  ";
			//$result = mysql_query($food_display);
			$food_display	= "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND meal_time = '".$m_time."' AND date='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."'  ";
			//$food_display	= "SELECT * FROM wp_food_diary WHERE userId='1082' AND meal_time = 'snack1' AND date='19-05-2014' ";
			$result   		= $wpdb->get_results( $food_display );
			
			/*
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_array($result)){
					$saved_meal = $row['meal'];
					$exercise_amount = $row['exercise_amount'];
					$exercise_effort = $row['exercise_effort'];
					$id = $row['id'];
					$date = $row['date'];
					$day = $row['day'];
				}
			}else{
				$exercise_amounts = 0;
				$exercise_efforts = 0;
			}
			*/

			if( ! empty ( $result ) ){
				foreach ($result as $row) {
					$saved_meal = $row->meal;
					$exercise_amount = $row->exercise_amount;
					$exercise_effort = $row->exercise_effort;
					$id = $row->id;
					$date = $row->date;
					$day = $row->day;
				}
				
			}else{
				$exercise_amounts = 0;
				$exercise_efforts = 0;
			}
			
			
			$dots = "";
			if(strlen($saved_meal) > 7){ $dots = ".."; }
			/******************************************************************/
			//date for this column
			//$t;
			if($date != ""){$t = $date;}
			?>
			
			<script>
			$(document).ready(function(){
				$('#t<?php echo $i.$j; ?>[title]').tooltip();
				});
			</script>
			
			<?php echo "<td style='border-right:1px solid #ccc;'>";
			
					
			if(($j != 8) && (($j != 7)) && (($j != 9))){
			echo "<span id='t".$i.$j."' title='".$saved_meal."'>".$saved_meal."</span><br />";
				
				
			} else if($j == 8){

					
					$ex_display	= "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					$result   		= $wpdb->get_results( $ex_display );
					$exercise_amounts = "";
					$exercise_efforts = "";

					if( ! empty ( $result ) ){
						foreach ($result as $row) {
							$exercise_amounts = $row->exercise_amount;
							$exercise_efforts = $row->exercise_effort;
							echo "<span>".$exercise_efforts."</span><br />";
							break;
						}
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
		
				} else if($j == 7){
					//get exercise values
					//$ex_display = "SELECT * FROM food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					//echo $ex_display; 
					//$result = mysql_query($ex_display);
					$ex_display = "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					$result   	= $wpdb->get_results( $ex_display );
					/*
					
					if(mysql_num_rows($result) > 0){
						while($row = mysql_fetch_array($result)){
							$exercise_amounts = $row['exercise_amount'];
							$exercise_efforts = $row['exercise_effort'];
							echo "<span style='font-size:15px;'>".$exercise_amounts."</span>";
							break;
						}
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
					*/
					
					if( ! empty ( $result ) ){
					
						foreach ($result as $row) {
							$exercise_amounts = $row->exercise_amount;
							$exercise_efforts = $row->exercise_effort;
							echo "<span>".$exercise_amounts."</span><br />";
							break;
						}
						
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
					
	
				} else if($j == 9){

					$ex_display = "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					$result   	= $wpdb->get_results( $ex_display );

					if( ! empty ( $result ) ){
						
						foreach ( $result as $row ) {
							$exercise_amounts = $row->exercise_amount;
							$exercise_efforts = $row->exercise_effort;
							$total = $exercise_amounts * $exercise_efforts;
							$weekly_total = $weekly_total + $total;
							echo "<span class='weekly-span'>".$total."</span>";
							break;
						}
						
					}
					
				}				
			echo "</td>";
		
		} 
		echo "</tr>";
		echo "<script>$('.edit9').hide(); </script>"; 
	}
	//echo "</tr><tr>";
	?></tbody>
	</table>
	
	<?php echo "<div class='weekly_total' style='float:left; color:#444; font-family:Open Sans, sans-serif;'><h3>Weekly Exercise Effort Score: ".$weekly_total."</h3></div>"; ?>

	</div>




<!-- End of Print -->

<div class="food_div" id="food_div">
	<table class="food_week" style="display:none;">
	<tr><td style="height: 66px; border-radius:20px 0px 0px 0px">Week </td></tr>
	<tr><td>Breakfast</td></tr>
	<tr><td>Snack</td></tr>
	<tr><td>Lunch</td></tr>
	<tr><td>Snack</td></tr>
	<tr><td>Dinner</td></tr>
	<tr><td>Snack</td></tr>
	<tr><td><span id="ex_amt" title="in mins only.">Exercise Amount</span></td></tr>
	<tr><td><span id="ex_eff" title="score out of 10.">Exercise Effort</span></td></tr>
	<tr><td>Daily Exercise Effort Score</td></tr>
	</table>
	
	
	<table class="food_table" id="food_table">
	<colgroup>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
		<col width="12.5%"></col>
	</colgroup>
	<?php
	echo "<thead><tr>";
	echo "<th class='textLarge'>Week</th>";
	$weekly_total = 0;
	//display days in top rown
	for($i = 1; $i <= 7; $i++){
	
		$current_day = date("l", strtotime("+".$i."days", $weekstart));
		//$day_c = date("l", strtotime($page."1"."days", $weekstart));
		echo "<th>" . $current_day . "<br /><small>". date("d-m-Y", strtotime("+".$i."days", $weekstart)) . "</small></th>";
	}
	echo "</tr></thead>";
	echo "<tbody>";
	for($j = 1; $j <= 9; $j++){
		echo "<tr>";
		
		echo "<td class='period'>";
		if($j == 1){ echo "Breakfast";}
		if($j == 2){ echo "Snack";}
		if($j == 3){ echo "Lunch";}
		if($j == 4){ echo "Snack";}
		if($j == 5){ echo "Dinner";}
		if($j == 6){ echo "Snack";}
		if($j == 7){ echo "Exercise Amount";}
		if($j == 8){ echo "Exercise Effort";}
		if($j == 9){ echo "Daily Exercise effort";}
		
		echo "</td>";
		
		for($i = 1; $i <= 7; $i++){ //echo out the remainder - up to next 6 days
			$m_time = "";
			if($j == 1){ $m_time = "breakfast"; }
			if($j == 2){ $m_time = "snack1"; }
			if($j == 3){ $m_time = "lunch"; }
			if($j == 4){ $m_time = "snack2"; }
			if($j == 5){ $m_time = "dinner"; }
			if($j == 6){ $m_time = "snack3"; }
			if($j == 7){ $m_time = "physical activity"; }
			
			$d_time = "";
			if($i == 1){ $d_time = date("l", strtotime("+"."1"."days", $weekstart));; }
			if($i == 2){ $d_time = date("l", strtotime("+"."2"."days", $weekstart));; }
			if($i == 3){ $d_time = date("l", strtotime("+"."3"."days", $weekstart));; }
			if($i == 4){ $d_time = date("l", strtotime("+"."4"."days", $weekstart));; }
			if($i == 5){ $d_time = date("l", strtotime("+"."5"."days", $weekstart));; }
			if($i == 6){ $d_time = date("l", strtotime("+"."6"."days", $weekstart));; }
			if($i == 7){ $d_time = date("l", strtotime("+"."7"."days", $weekstart));; }
			
			$d_date = "";
			if($i == 1){ $d_date = date("d-m-Y", strtotime("+"."1"."days", $weekstart)); }
			if($i == 2){ $d_date = date("d-m-Y", strtotime("+"."2"."days", $weekstart)); }
			if($i == 3){ $d_date = date("d-m-Y", strtotime("+"."3"."days", $weekstart)); }
			if($i == 4){ $d_date = date("d-m-Y", strtotime("+"."4"."days", $weekstart)); }
			if($i == 5){ $d_date = date("d-m-Y", strtotime("+"."5"."days", $weekstart)); }
			if($i == 6){ $d_date = date("d-m-Y", strtotime("+"."6"."days", $weekstart)); }
			if($i == 7){ $d_date = date("d-m-Y", strtotime("+"."7"."days", $weekstart)); }
			
			/******************************GET SAVED DATA*************************************/
			$saved_meal="";
			$exercise_amount = "";
			$exercise_effort = "";
			$id = "";
			$date = "";
			$day = "";
			//$food_display = "SELECT * FROM food_diary WHERE userId='".$userId."' AND meal_time = '".$m_time."' AND date='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."'  ";
			//$result = mysql_query($food_display);
			$food_display	= "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND meal_time = '".$m_time."' AND date='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."'  ";
			//$food_display	= "SELECT * FROM wp_food_diary WHERE userId='1082' AND meal_time = 'snack1' AND date='19-05-2014' ";
			$result   		= $wpdb->get_results( $food_display );
			
			/*
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_array($result)){
					$saved_meal = $row['meal'];
					$exercise_amount = $row['exercise_amount'];
					$exercise_effort = $row['exercise_effort'];
					$id = $row['id'];
					$date = $row['date'];
					$day = $row['day'];
				}
			}else{
				$exercise_amounts = 0;
				$exercise_efforts = 0;
			}
			*/

			if( ! empty ( $result ) ){
				foreach ($result as $row) {
					$saved_meal = $row->meal;
					$exercise_amount = $row->exercise_amount;
					$exercise_effort = $row->exercise_effort;
					$id = $row->id;
					$date = $row->date;
					$day = $row->day;
				}
				
			}else{
				$exercise_amounts = 0;
				$exercise_efforts = 0;
			}
			
			
			$dots = "";
			if(strlen($saved_meal) > 7){ $dots = ".."; }
			/******************************************************************/
			//date for this column
			//$t;
			if($date != ""){$t = $date;}
			?>
			
			<script>
			$(document).ready(function(){
				$('#t<?php echo $i.$j; ?>[title]').tooltip();
				});
			</script>
			
			<?php echo "<td>";
			
					
			if(($j != 8) && (($j != 7)) && (($j != 9))){
			echo "<span id='t".$i.$j."' title='".$saved_meal."'>".$saved_meal."</span><br />";
				if($saved_meal == ""){
				echo "<span style='float:right; cursor:pointer;' onclick=open_popup('popup".$i.$j."'); style='cursor:pointer;'><img class='edit".$j."' src='". plugins_url( 'assets/images/edit_diary.jpg'  ,  dirname( __FILE__ ) ). "'></span>";			
				} else {
				echo "<span style='display:block; cursor:pointer;' onclick=open_popup('popup".$i.$j."'); style='cursor:pointer;'><br /><img class='edit".$j."' src='". plugins_url( 'assets/images/view_diary.jpg'  ,  dirname( __FILE__ ) ). "'  ></span>";			
				}
				echo "<div class='popup' id='popup".$i.$j."' >
				<form class='p_form' method='post' action='/members-area/my-diary/'>
					<div class='ex_label'><h6 class='pink-h6'>Enter meal:</h6><br /></div>
					<input name='meal' type='text' value='".$saved_meal."' /><br />
					<input name='date' type='hidden' value='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."' />	
					<input name='day' type='hidden' value='".date("l", strtotime("+".$i."days", $weekstart))."' />
					<input name='meal_time' type='hidden' value='".$m_time."' />
					<input name='id' type='hidden' value='".$id."' />
					<input name='update_food' type='hidden' value='update_food' />	
					<input name='pager' type='hidden' value='".$pager."' />
					<br />
					
					<input type='submit' value='Save' class='button' />
				</form>
				<div onclick=close_popup('popup".$i.$j."'); style='cursor:pointer; float: right;' ><img src='/wp-content/plugins/wp-sally-symonds/assets/images/close_icon.gif' ></div>
				</div>";	
					} else if($j == 8){
					//get exercise values
					//$ex_display = "SELECT * FROM food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					//echo $ex_display; 
					//$result = mysql_query($ex_display);
					
					$ex_display	= "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					$result   		= $wpdb->get_results( $ex_display );
					$exercise_amounts = "";
					$exercise_efforts = "";
					/*
					if(mysql_num_rows($result) > 0){
						while($row = mysql_fetch_array($result)){				
							$exercise_amounts = $row['exercise_amount'];
							$exercise_efforts = $row['exercise_effort'];
							echo "<span style='font-size:15px;'>".$exercise_efforts."</span>";
							break;
							}
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
					*/
					
					if( ! empty ( $result ) ){
						foreach ($result as $row) {
							$exercise_amounts = $row->exercise_amount;
							$exercise_efforts = $row->exercise_effort;
							echo "<span>".$exercise_efforts."</span><br />";
							break;
						}
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
				//if($exercise_efforts == "" || $exercise_amounts == ""){	
					echo "<span style='float:right; cursor:pointer;' onclick=open_popup('popup".$i.$j."'); style='cursor:pointer;'><img class='edit".$j."' src='". plugins_url( 'assets/images/edit_diary.jpg'  ,  dirname( __FILE__ ) ). "' ></span>";			
				//} else {
				//	echo "<span style='display:block; cursor:pointer;' onclick=open_popup('popup".$i.$j."'); style='cursor:pointer;'><img class='edit".$j."' src='/images/view_diary.jpg' width='63' ></span>";			
				//}
				echo "<div class='popup' id='popup".$i.$j."'>
				<form class='p_form' method='post' action='/members-area/my-diary/'>
				
				<!--div class='ex_label' >Enter exercise amount for today:<div-->
				<input name='exercise_amount' style='color:grey; display:none;' type='text' value='".$exercise_amounts."'  /><br />
				<div class='ex_label'><h6 class='pink-h6'>Enter Exercise Effort today:</h6><br /></div>
				<input name='exercise_effort' style='color:grey;' type='text' value='".$exercise_efforts."' /><br />
				<input name='date' type='hidden' value='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."' />	
				<input name='day' type='hidden' value='".date("l", strtotime("+".$i."days", $weekstart))."' />
				<input name='meal_time' type='hidden' value='".$m_time."' />
				<input name='meal' type='hidden' value='".$saved_meal."' />
				<input name='id' type='hidden' value='".$id."' />
				<input name='update_exercise' type='hidden' value='update_exercise' />	
				<input name='pager' type='hidden' value='".$pager."' />
				<input type='submit' value='save' class='button' />
				</form>
				<div onclick=close_popup('popup".$i.$j."'); style='cursor:pointer; float: right;' ><img src='http://www.iadvise.co.za/images/close_icon.gif' ></div>
				</div>";			
				} else if($j == 7){
					//get exercise values
					//$ex_display = "SELECT * FROM food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					//echo $ex_display; 
					//$result = mysql_query($ex_display);
					$ex_display = "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					$result   	= $wpdb->get_results( $ex_display );
					/*
					
					if(mysql_num_rows($result) > 0){
						while($row = mysql_fetch_array($result)){
							$exercise_amounts = $row['exercise_amount'];
							$exercise_efforts = $row['exercise_effort'];
							echo "<span style='font-size:15px;'>".$exercise_amounts."</span>";
							break;
						}
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
					*/
					
					if( ! empty ( $result ) ){
					
						foreach ($result as $row) {
							$exercise_amounts = $row->exercise_amount;
							$exercise_efforts = $row->exercise_effort;
							echo "<span>".$exercise_amounts."</span><br />";
							break;
						}
						
					}else{
						$exercise_amounts = '';
						$exercise_efforts = '';
					}
					
					//echo $exercise_efforts . "ef<br />";
					
					//if($exercise_efforts == "" || $exercise_amounts == ""){	
					echo "<span style='float:right; cursor:pointer;' onclick=open_popup('popup".$i.$j."'); style='cursor:pointer;'><img class='edit".$j."' src='". plugins_url( 'assets/images/edit_diary.jpg'  ,  dirname( __FILE__ ) ). "' ></span>";			
				//} //else {
					//echo "<span style='display:block; cursor:pointer;' onclick=open_popup('popup".$i.$j."'); style='cursor:pointer;'><img class='edit".$j."' src='/images/view_diary.jpg' width='63' ></span>";			
				//}
				echo "<div class='popup' id='popup".$i.$j."'>
				<form class='p_form' method='post' action='/members-area/my-diary/'>					
					<div class='ex_label'><h6 class='pink-h6'>Enter exercise amount for today:</h6><br /><div>
					<input name='exercise_amount' style='color:grey;' type='text' value='".$exercise_amounts."'  /><br />
					<!--div class='ex_label'>Enter Exercise Effort today:</div-->
					<input name='exercise_effort' style='color:grey; display:none;' type='text' value='".$exercise_efforts."' /><br />
					<input name='date' type='hidden' value='".date("d-m-Y", strtotime("+".$i."days", $weekstart))."' />	
					<input name='day' type='hidden' value='".date("l", strtotime("+".$i."days", $weekstart))."' />
					<input name='meal_time' type='hidden' value='".$m_time."' />
					<input name='meal' type='hidden' value='".$saved_meal."' />
					<input name='id' type='hidden' value='".$id."' />
					<input name='update_exercise' type='hidden' value='update_exercise' />	
					<input name='pager' type='hidden' value='".$pager."' />
					<input type='submit' value='save' class='button' />
				</form>
				<div onclick=close_popup('popup".$i.$j."'); style='cursor:pointer; float: right;' ><img src='http://www.iadvise.co.za/images/close_icon.gif' ></div>
				</div>";
					
					
					
				
				echo "<div class='popup' id='popup".$i.$j."' >
				</div>";	
				} else if($j == 9){
					//get exercise values
					//$ex_display = "SELECT * FROM food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					//echo $ex_display; 
					//$result = mysql_query($ex_display);
					$ex_display = "SELECT * FROM {$wpdb->prefix}food_diary WHERE userId='".$userId."' AND date='".$d_date."' AND day='".$d_time."' ";
					$result   	= $wpdb->get_results( $ex_display );
					/*
					if($result){
						while($row = mysql_fetch_array($result)){				
							$exercise_amounts = $row['exercise_amount'];
							$exercise_efforts = $row['exercise_effort'];
							$total = $exercise_amounts * $exercise_efforts;
							$weekly_total = $weekly_total + $total;
							echo "<span style='font-size: 15px;'>".$total."</span>";
							break;
							}
					}
					*/
					
					if( ! empty ( $result ) ){
						
						foreach ( $result as $row ) {
							$exercise_amounts = $row->exercise_amount;
							$exercise_efforts = $row->exercise_effort;
							$total = $exercise_amounts * $exercise_efforts;
							$weekly_total = $weekly_total + $total;
							echo "<span class='weekly-span'>".$total."</span>";
							break;
						}
						
					}
					
				}				
			echo "</td>";
		
			//echo date("d-m-Y", strtotime("+".$i."days", $weekstart))."----";
		} 
		echo "</tr>";
		echo "<script>$('.edit9').hide(); </script>"; 
	}
	//echo "</tr><tr>";
	?></tbody>
	</table>
	
	<?php echo "<div class='weekly_total' style='float:left;'><h3>Weekly Exercise Effort Score: ".$weekly_total."</h3></div>"; ?>
	<div style='float:right; margin-top:40px;'><input class="btn-submit" type="button" value="" onclick="PrintElem('#food_div2')" class="button" /></div>
	<div class="both"></div>
	</div>




<?php } else {
		//echo "<h1>Please log back in</h1>";
		_e( sprintf( 'Please <a href="%s" title="Login">log back in</a>' , wp_login_url( get_permalink() ) ) );

} ?>


<?php
function getWeek($timetoday){
 /*******************************************************/
	//$startdate = strtotime("".$row['start_date'].""); 
	$startdate = strtotime($timetoday); 
    $enddate = time();

    $time_passed = $enddate - $startdate;

    // if the first day after startdate is in "Week 1" according to your count
    $weekcount_1 = ceil ( $time_passed / (86400*7));

    // if the first day after startdate is in "Week 0" according to your count
    $weekcount_0 = floor ( $time_passed / (86400*7));
	
	//echo $weekcount_1 . "---<br />";
  if($weekcount_1 == -0){ $weekcount_1 = 1; }
  /******************************************************/
return $weekcount_1;
}



//echo substr($values,0,-1) . " -----values";

function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
    /*
    $interval can be:
    yyyy - Number of full years
    q - Number of full quarters
    m - Number of full months
    y - Difference between day numbers
        (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
    d - Number of full days
    w - Number of full weekdays
    ww - Number of full weeks
    h - Number of full hours
    n - Number of full minutes
    s - Number of full seconds (default)
    */
    
    if (!$using_timestamps) {
        $datefrom = strtotime($datefrom, 0);
        $dateto = strtotime($dateto, 0);
    }
    $difference = $dateto - $datefrom; // Difference in seconds
     
    switch($interval) {
     
    case 'yyyy': // Number of full years

        $years_difference = floor($difference / 31536000);
        if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
            $years_difference--;
        }
        if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
            $years_difference++;
        }
        $datediff = $years_difference;
        break;

    case "q": // Number of full quarters

        $quarters_difference = floor($difference / 8035200);
        while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
            $months_difference++;
        }
        $quarters_difference--;
        $datediff = $quarters_difference;
        break;

    case "m": // Number of full months

        $months_difference = floor($difference / 2678400);
        while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
            $months_difference++;
        }
        $months_difference--;
        $datediff = $months_difference;
        break;

    case 'y': // Difference between day numbers

        $datediff = date("z", $dateto) - date("z", $datefrom);
        break;

    case "d": // Number of full days

        $datediff = floor($difference / 86400);
        break;

    case "w": // Number of full weekdays

        $days_difference = floor($difference / 86400);
        $weeks_difference = floor($days_difference / 7); // Complete weeks
        $first_day = date("w", $datefrom);
        $days_remainder = floor($days_difference % 7);
        $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
        if ($odd_days > 7) { // Sunday
            $days_remainder--;
        }
        if ($odd_days > 6) { // Saturday
            $days_remainder--;
        }
        $datediff = ($weeks_difference * 5) + $days_remainder;
        break;

    case "ww": // Number of full weeks

        $datediff = floor($difference / 604800);
        break;

    case "h": // Number of full hours

        $datediff = floor($difference / 3600);
        break;

    case "n": // Number of full minutes

        $datediff = floor($difference / 60);
        break;

    default: // Number of full seconds (default)

        $datediff = $difference;
        break;
    }    

    return $datediff;

}

/*
class MySQLDatabase{
var $link;
	function connect($user, $password, $database){
		$this->link = mysql_connect('localhost', $user, $password);
		if(!$this->link){
		die('Not connected : ' . mysql_error());
	}
$db = mysql_select_db($database, $this->link);
	if(!$db){
	die ('Cannot use : ' . mysql_error());
	}
	return $this->link;
	}
	function disconnect(){
	mysql_close($this->link);
	}
}



$db->disconnect();
*/
?>
<!-- 

<div class="disclaimer" style="clear:right; padding-top:20px;">
Disclaimer: All information contained within this site is for informational purposes only. It is not intended to diagnose, treat, cure, or prevent any health problem - nor is it intended to replace the advice of a qualified medical practitioner, dietician, or mental health worker. No action should be taken solely on the contents of this website. Always consult your physician or qualified health professional on any matters regarding your health or on any opinions expressed within this website. The information provided within this website is believed to be accurate based on the best judgment of the authors but the reader is responsible for consulting with their own health professional on any matters raised within. Health information changes rapidly. Therefore, some information within this website may be out of date or even possibly inaccurate due to new studies and research that the authors of this website are unaware of. We do not assume any liability for the information contained within this website, be it direct, indirect, consequential, special, exemplary, or other damages. Please see your physician before changing your diet, starting an exercise program, or taking any supplements of any kind. If you have any questions about this disclaimer, please contact us.

</div>
 -->
