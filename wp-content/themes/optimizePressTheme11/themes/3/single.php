<?php
global $post;
global $wp;

get_header();
			$class = op_default_attr('column_layout','option');
			$add_sidebar = true;
			if(defined('OP_SIDEBAR')){
				if(OP_SIDEBAR === FALSE){
					$class = 'no-sidebar';
					$add_sidebar = false;
				} else {
					$class = OP_SIDEBAR;
				}
			}
			?>
			<div class="main-content content-width cf <?php echo $class ?>">
		    	<div class="main-content-area-container cf">
	    	        <?php echo $add_sidebar ? '<div class="sidebar-bg"></div>' : '' ?>

                    <?php while ( have_posts() ) : the_post();
                    $img = '';
                    if(is_singular() && has_post_thumbnail($post->ID)){
                        $img = '<div class="post-image">'.get_the_post_thumbnail($post->ID,'post-thumbnail').'</div>';
                    }
                    ?>
                    <div id="post-<?php the_ID() ?>" <?php post_class('main-content-area'.($img==''?' no-post-image':'')) ?>>
                        <?php op_mod('advertising')->display(array('advertising', 'post_page', 'top')) ?>

                        <div class="latest-post cf">
								<?php 
								if($post->post_type  == "exercises"){
									echo '
										<div style="background:url(/wp-content/uploads/2014/10/exercise_02.png) repeat-x;position:absolute;left: 0;width: 100%;margin-top: -38px;padding-bottom: 2px;">
											<div class="fixed-width-2">
												<img alt="Exercise" src="/wp-content/uploads/2014/10/exercise_03.png" border="0" />
											</div>
										</div>
										<img alt="Exercise" src="/wp-content/uploads/2014/10/exercise_03.png" border="0" style="margin-top: -30px;padding-bottom: 15px;visibility:hidden;" />
									';
																		
									echo exercises_menu();
								}
								?>
							
							
                        <h1 class="the-title"><?php the_title(); ?></h1>
                            <div class="cf post-meta-container">
                                <?php ('post' == get_post_type()) && op_post_meta() ?>
                                <p class="post-meta date-extra"><?php the_time('F j Y') ?></p>
                            </div>
                            <?php echo $img ?>
							<?php op_mod('sharing')->display('sharing') ?>
                            <div class="single-post-content cf">
								
								<?php 
									//echo $post->post_type;
									//$wp->query_vars['post_type'] == "exercises"
									//$exer_contents = get_post_meta( $post->ID, 'exer_content');
									//echo '<pre>';
									//print_r($meta_values);
									//echo '</pre>';
									if($post->post_type == "forum" ||  $post->post_type  == "topic"  ||  $post->post_type  == "exercises" || $post->post_type  == "exercise-generator"){
										//the_excerpt();
									}else{
										the_excerpt();
									}
									 
								?>
                                <?php the_content(); ?>
                                
                                <?php 
                                
									if($post->post_type == "forum" ||  $post->post_type  == "topic"){
										/*
										echo '<div>
										
								Disclaimer: All information contained within this site is for informational purposes only. It is not intended to diagnose, treat, cure, or prevent any 
								health problem - nor is it intended to replace the advice of a qualified medical practitioner, dietician, or mental health worker. No action should be 
								taken solely on the contents of this website. Always consult your physician or qualified health professional on any matters regarding your health or on 
								any opinions expressed within this website. The information provided within this website is believed to be accurate based on the best judgment of the authors 
								but the reader is responsible for consulting with their own health professional on any matters raised within. Health information changes rapidly. Therefore,
								 some information within this website may be out of date or even possibly inaccurate due to new studies and research that the authors of this website are unaware of. 
								 We do not assume any liability for the information contained within this website, be it direct, indirect, consequential, special, exemplary, or other damages. 
								 Please see your physician before changing your diet, starting an exercise program, or taking any supplements of any kind. If you have any questions about this disclaimer, 
								 please contact us.
										
										
										
										</div>';
										* */
									}
									
                                
                                ?>
                                
                                
                                <? /*
                                <table class="exer-tab" width="100%">
                                <?php foreach($exer_contents as $ec): ?>
									<tr>
										<td style="width:50%;"><?php echo nl2br($ec); ?></td>
										<td style="width:50%;">&nbsp;</td>
									</tr>
                                <?php endforeach; ?>
                                </table>
                                */ ?>
                                
                                <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', OP_SN ) . '</span>', 'after' => '</div>' ) ); ?>
                            </div>
                        </div> <!-- end .latest-post -->

                        <?php op_mod('advertising')->display(array('advertising', 'post_page', 'bottom')) ?>

                        <?php 
                        if($post->post_type  != "exercises" && $post->post_type  != "exercise-generator"):
							op_mod('related_posts')->display('related_posts',array('before'=>'<div class="related-posts cf"><h3 class="section-header"><span>'.__('RELATED POSTS',OP_SN).'</span></h3>','after'=>'</div>','ulclass'=>'cf')); 
                        endif;
                        ?>
                        <?php comments_template( '', true ); ?>
                    </div>
                    <?php endwhile ?>
                    <?php op_sidebar() ?>
                    
                </div>
                <div class="clear"></div>
                    <?php 
                    /*$post->post_type == "forum" ||  $post->post_type  == "topic"  ||*/
					if($post->post_type  == "exercises" || $post->post_type  == "exercise-generator" || $post->post_type  == "recipe" || $post->post_type == "forum" ||  $post->post_type  == "topic" ){
					?>
                    
						<div class="fixed-width">
						<p style="font-size:9px;line-height: 140%;">Disclaimer: All information contained within this site is for informational purposes only. It is not intended to diagnose, treat, cure, or prevent any health problem – nor is it intended to replace the advice of a qualified medical practitioner, dietician, or mental health worker. No action should be taken solely on the contents of this website. Always consult your physician or qualified health professional on any matters regarding your health or on any opinions expressed within this website. The information provided within this website is believed to be accurate based on the best judgment of the authors but the reader is responsible for consulting with their own health professional on any matters raised within. Health information changes rapidly. Therefore, some information within this website may be out of date or even possibly inaccurate due to new studies and research that the authors of this website are unaware of. We do not assume any liability for the information contained within this website, be it direct, indirect, consequential, special, exemplary, or other damages. Please see your physician before changing your diet, starting an exercise program, or taking any supplements of any kind. If you have any questions about this disclaimer, please contact us.</p>
						</div>
					<?php
						}
					?>
                
            </div>
            
<style type="text/css">
.continue-reading{display:none;}	
<?php if($post->post_type  == "exercises"|| $post->post_type  == "exercise-generator"): ?>
.single-post-content{position: relative;margin-bottom:20px;font-size: 14px;line-height: 120%;}
.single-post-content iframe{/*margin-top:50px;*/box-shadow: 1px 1px 20px #000;}
.single-post-content i,.single-post-content em {font-size: 16px;text-decoration: underline;color: #EA0F6B !important;}
.single-post-content strong{font-size: 21px;color:#EA0F6B !important;line-height: 120%;}
.single-post-content p{font-size: 14px;line-height: 120%;}
.main-content-area img{max-width: inherit;}
<?php endif; ?>
</style>
<?php if($post->post_type  == "exercise-generator"): ?>
<style>
.exer_cise_cont{}	
.exer_cise{
	border-bottom: 1px solid #EA0F6B;
	padding-bottom: 8px;	
}	

.exer_cise li{
	border: 1px solid #EA0F6B;
	display: inline-block;
	padding: 5px;
	padding-right: 10px;	
	border-radius: 10px;
	margin-right:0px;
	font-size: 14px !important;
	margin-top: -5px;
	box-shadow: 1px 1px 1px #EA0F6B;
}
.exer_cise li a{
	color:#EA0F6B;
}
.exer_cise li.active{
	background:#EA0F6B !important;
	color:#fff !important;
}

.exer_cise li.active a.exer {
	color:#fff !important;
}

#content_area .exer_cise li.active a.exer {
	color:#fff !important;
}

.post-meta-container{display:none;}

.exer_table1 td {border: 1px solid #ccc;padding: 10px;font-size: 14px;}
.exer_table1 td * {font-size: 14px !important;line-height: 120% !important;}
.exer_table2 td{border: 1px solid #ccc;text-align: center;padding: 3px;font-size: 12px;} 
.exer_table2 tr > td:first-child{text-align:left;} 	
.exer_table3 td{border: 1px solid #ccc;text-align: center;padding: 3px;font-size: 12px;} 
.sally_video_list li img{margin:0;float:right;}


.sally_video_list li p{display:none;}
.sally_video_list li{background: #eee;padding:10px;padding-top: 25px;padding-bottom: 25px;}
.sally_video_list li:hover{background: #ccc;box-shadow:1px 1px 20px #000;}

/*.col-right img{width:290px;height:auto;}*/

/**/
.main-sidebar{display:none;}
.main-content .sidebar-bg{display:none;}
.main-content-area{width:100%;}

</style>	
<script type="text/javascript">
jQuery( document ).ready(function() {
	//jQuery("iframe").attr("width", "290");
	//jQuery("iframe").attr("height", "166");
	jQuery("iframe").attr("width", "440");
	jQuery("iframe").attr("height", "250");
});	
</script>
	
<?php endif; ?>



<?php get_footer() ?>
