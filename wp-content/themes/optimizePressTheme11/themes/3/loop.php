				<div class="older-post-list cf">
                    <?php /* Start the Loop */ ?>
                    <?php 
					$categories = get_the_category();
					$cat_id = $categories[0]->cat_ID;
                    
					global $post;
					while ( have_posts() ) : the_post();
					$class = ' no-post-thumbnail'; $image = '';
					if(has_post_thumbnail()){
						$image = '<a href="'.get_permalink().'" title="'.sprintf( esc_attr__( 'Permalink to %s', OP_SN ), the_title_attribute( 'echo=0' ) ).'" rel="bookmark" class="post-image">'.get_the_post_thumbnail( $post->ID ).'</a>';
						$class = '';
					}
					 ?>
					<div class="older-post<?php echo $class ?>">
					<h4 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', OP_SN ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                    	<?php op_post_meta() ?>
                    	<?php 
							if($cat_id != 18){
								echo $image; 
							}
                    	?>
                        <?php the_excerpt(); ?>
                        <p><?php the_tags(); ?></p>
						
					</div> <!-- end .older-post -->
					<?php endwhile ?>
					<?php op_pagination() ?>
				</div>
