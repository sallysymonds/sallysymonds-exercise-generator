<?php
error_reporting(E_ERROR);




require_once( dirname( __FILE__ ) . '/lib/framework.php' );

if (!class_exists('Op_Arrow_Walker_Nav_Menu')) {
	class Op_Arrow_Walker_Nav_Menu extends Walker_Nav_Menu
	{
		public function display_element($el, &$children, $max_depth, $depth = 0, $args, &$output)
		{
			$id = $this->db_fields['id'];    

			if(isset($children[$el->$id])) {
				$el->classes[] = 'has_children';    	
			}			  

			parent::display_element($el, $children, $max_depth, $depth, $args, $output);
		}
	}	
}

/* Custom Shortocde */

/* <!-- Web Form Code --> */

add_shortcode('web_form_code', 'web_form_code'); 

function web_form_code( $atts , $content = null ){
    
    //echo '<pre>'; print_r($atts); echo '</pre>';
    
    if ( wp_is_mobile() ) {
        $html ='<!-- Web Form Code -->';
        $html .='<script language="JavaScript" type="text/javascript" charset="utf-8" src="'.$atts['mobile'].'"></script>';
    }else{
        $html ='<script language="JavaScript" type="text/javascript" charset="utf-8" src="'.$atts['desktop'].'"></script>';
    } 
    
    return $html;
    
} 

add_shortcode('popup_web_form_code', 'popup_web_form_code'); 

function popup_web_form_code( $atts , $content = null ){
    
    //echo '<pre>'; print_r($atts); echo '</pre>';
    add_action( 'wp_enqueue_scripts', 'theme_popup_web_form_code_scripts' );
    
    if ( wp_is_mobile() ) {
        $html ='<!-- Web Form Code -->';
        $html .='<div id="inline-1"  style="width:300px;display:none;">';
        $html .='<script language="JavaScript" type="text/javascript" charset="utf-8" src="'.$atts['mobile'].'"></script>';
        $html .='</div>';
    }else{
        $html .='<div id="inline-1"  style="width:600px;display:none;">';
        $html .='<script language="JavaScript" type="text/javascript" charset="utf-8" src="'.$atts['desktop'].'"></script>';
        $html .='</div>';
    } 
    
    return $html;
    
} 


function theme_popup_web_form_code_scripts() {
	wp_enqueue_style( 'prettyphoto-css', 'http://www.netseek.com.au/wp-content/themes/optimizePressTheme/lib/js/prettyphoto/prettyPhoto.min.css?ver=2.1.6' );
}


add_shortcode( 'get_images_dir', 'get_images_dir' );
function get_images_dir($atts){
	
	extract( shortcode_atts( array(
		'dir' => 'images'
	), $atts ) );	
	

	$image_types = array(
		'gif' => 'image/gif',
		'png' => 'image/png',
		'jpg' => 'image/jpeg',
	);

	$imgs = scandir($dir);
	
	$li  = '';

	foreach($imgs as $i):
	
		if (!is_dir($i)) {
			if (in_array(mime_content_type($dir.'/'. $i), $image_types)) {
				$li.='<li>';
				$li.='<img src="/'.$dir.'/'. $i.'" />';
				$li.='</li>';
			}		
		}
		
	endforeach;
	
	return '<div class="gall"><ul>'.$li.'</ul></div><div style="clear:both;"></div>
	<style>
.gall li{display:inline-block;width: 310px;text-align:center;}
</style>
		';
	
}//get_images_dir


add_shortcode( 'exercises_menu', 'exercises_menu' );
function exercises_menu(){
	ob_start();
	
	global $post;
	
	$exer_menu = array();
	$menu_exercises = wp_get_nav_menu_items(1095);

	$x = 0;
	foreach($menu_exercises as $m_exer){
		
		$exer_menu[$x]['ID']  = $m_exer->object_id;
		$exer_menu[$x]['title']  = $m_exer->title;
		$exer_menu[$x]['url'] 	 = $m_exer->url;
		$exer_menu[$x]['p_name'] = $m_exer->post_name;
		$exer_menu[$x]['cat']    = $m_exer->attr_title;
		
		$x++;
	}//endforeach
	
	$cats = get_the_terms($post->ID, 'exercises_category');
	$cats = current($cats);
	
	?>

	<?php /*added by noel netseek  */ ?>
<!--<div class="exer_cise_cont">
			<ul class="exer_cise">
				<?php// foreach($exer_menu as $e): ?>
					<li class="<?php //echo $post->ID == $e['ID']?'active':'';?> <?php //echo $cats->slug == $e['cat']?'active':'';?> ">
						<a href="<?php //echo $e['url']; ?>" class="exer"><?php //echo $e['title']; ?></a>
					</li>
				<?php //endforeach; ?>
			</ul>
		</div>
-->

			<div class="exercise-nav">
				<?php /*
				<div style="background:url(http://sallysymonds.6bin.com/wp-content/uploads/2014/10/exercise_02.png) repeat-x;position:absolute;left: 0;width: 100%;margin-top: -30px;padding-bottom: 2px;">
					<div class="fixed-width-2">
						<img alt="Exercise" src="http://sallysymonds.6bin.com/wp-content/uploads/2014/10/exercise_03.png" border="0" />
					</div>
				</div>
				<img alt="Exercise" src="http://sallysymonds.6bin.com/wp-content/uploads/2014/10/exercise_03.png" border="0" style="margin-top: -30px;padding-bottom: 15px;visibility:hidden;" />
				* */ ?>
				
				
				<ul>
				<li class="<?php echo $post->ID == '3483'?'act':'';?> <?php echo $cats->slug == 'thighs-butt-exercises'?'act':'';?>">
					<a href="/members-area/exercises/thighs-butt/" title="thigh" >
						<img src="/images/exercise/normal/exercise_03.png" alt="Thighs Butt" class="nor" />
						<img src="/images/exercise/hover/exercise-hover_03.png" alt="Thighs Butt" class="hov"  />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3486'?'act':'';?> <?php echo $cats->slug == 'abdominals-2'?'act':'';?>">
					<a href="/members-area/exercises/abdominals/" title="abs" >
					<img src="/images/exercise/normal/exercise_04.png" alt="Abdominals" class="nor" />
					<img src="/images/exercise/hover/exercise-hover_04.png" alt="Abdominals" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3499'?'act':'';?> <?php echo $cats->slug == 'chest'?'act':'';?>">
					<a href="/members-area/exercises/chest/" title="chest" >
					<img src="/images/exercise/normal/exercise_05.png" alt="" class="nor" />
					<img src="/images/exercise/hover/exercise-hover_05.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3505'?'act':'';?> <?php echo $cats->slug == 'back'?'act':'';?>">
					<a href="/members-area/exercises/back/" title="back" ><img src="/images/exercise/normal/exercise_06.png" alt="" class="nor" />
					<img src="/images/exercise/hover/exercise-hover_06.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3508'?'act':'';?> <?php echo $cats->slug == 'arms'?'act':'';?>">
					<a href="/members-area/exercises/arms/" title="arms" >
						<img src="/images/exercise/normal/exercise_07.png" alt=""class="nor" />
						<img src="/images/exercise/hover/exercise-hover_07.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3514'?'act':'';?> <?php echo $cats->slug == 'lazy-days'?'act':'';?>">
					<a href="/members-area/exercises/lazy-days/" title="lazy" >
						<img src="/images/exercise/normal/exercise_08.png" alt="" class="nor"  />
						<img src="/images/exercise/hover/exercise-hover_08.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3517'?'act':'';?> <?php echo $cats->slug == 'super-challenges'?'act':'';?>">
					<a href="/members-area/exercises/super-challenges/" title="super" >
						<img src="/images/exercise/normal/exercise_11.png" alt="" class="nor" />
						<img src="/images/exercise/hover/exercise-hover_11.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '6111'?'act':'';?> <?php echo $cats->slug == 'super-challenges'?'act':'';?>">
					<a href="/members-area/exercises/warm-ups/" title="warm" >
						<img src="/images/exercise/normal/exercise_12.png" alt="" class="nor" />
						<img src="/images/exercise/hover/exercise-hover_12.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3521'?'act':'';?> <?php echo $cats->slug == 'cardio-2'?'act':'';?>">
					<a href="/members-area/exercises/cardio/" title="cardio">
						<img src="/images/exercise/normal/exercise_13.png" alt="" class="nor" />
						<img src="/images/exercise/hover/exercise-hover_13.png" alt="" class="hov" />
					</a>
				</li>
				<li class="<?php echo $post->ID == '3524'?'act':'';?> <?php echo $cats->slug == 'stretching-flexibility'?'act':'';?>">
					<a href="/members-area/exercises/stretching-flexibility/" title="stretching">
						<img src="/images/exercise/normal/exercise_14.png" alt="" class="nor"  />
						<img src="/images/exercise/hover/exercise-hover_14.png" alt="" class="hov"  />
					</a>
				</li>
				</ul>
			</div>
			<div class="imgLine"></div>
			
	<?php /*added by noel netseek End */ ?>	
<style>
.fixed-width-2{width: 960px !important;margin:0 auto;}
.main-content-area-container{position:inherit;}

.exer_cise_cont{}	
.exer_cise{
	border-bottom: 1px solid #EA0F6B;
	padding-bottom: 8px;	
}	

.exer_cise li{
	border: 1px solid #EA0F6B;
	display: inline-block;
	padding: 5px;
	padding-right: 10px;	
	border-radius: 10px;
	margin-right:0px;
	font-size: 14px !important;
	margin-top: -5px;
	box-shadow: 1px 1px 1px #EA0F6B;
}
.exer_cise li a{
	color:#EA0F6B;
}
.exer_cise li.active{
	background:#EA0F6B !important;
	color:#fff !important;
}

.exer_cise li.active a.exer {
	color:#fff !important;
}

#content_area .exer_cise li.active a.exer {
	color:#fff !important;
}

.post-meta-container{display:none;}

.exer_table1 td {border: 1px solid #ccc;padding: 10px;font-size: 14px;}
.exer_table1 td * {font-size: 14px !important;line-height: 120% !important;}
.exer_table2 td{border: 1px solid #ccc;text-align: center;padding: 3px;font-size: 12px;} 
.exer_table2 tr > td:first-child{text-align:left;} 	
.exer_table3 td{border: 1px solid #ccc;text-align: center;padding: 3px;font-size: 12px;} 
.sally_video_list li img{margin:0;float:right;}


.sally_video_list li p{display:none;}
.sally_video_list li{background: #eee;padding:10px;padding-top: 25px;padding-bottom: 25px;}
.sally_video_list li:hover{background: #ccc;box-shadow:1px 1px 20px #000;}

/*.col-right img{width:290px;height:auto;}*/

/**/
.main-sidebar{display:none;}
.main-content .sidebar-bg{display:none;}
.main-content-area{width:100%;}

</style>	
<script type="text/javascript">
jQuery( document ).ready(function() {
	//jQuery("iframe").attr("width", "290");
	//jQuery("iframe").attr("height", "166");
	jQuery("iframe").attr("width", "440");
	jQuery("iframe").attr("height", "250");
});	
</script>
	
	<?php
	
	$cont_x = ob_get_contents();	
	ob_clean();	
	return $cont_x;
}//endfunction




function recipe_rerr(){
	
	global $wp;
	if($_GET['d'] == 1){
		global $wp;
		echo '<pre>';
		print_r($wp);
		die();
	}
	//?recipe=demo-recipe&post_type=recipe
	//add_rewrite_rule('recipe/([^/]+)/?','index.php?recipe=$matches[1]','top');
	add_rewrite_rule('recipe/([^/]+)(/[0-9]+)?/?$','index.php?recipe=$matches[1]&post_type=recipe&name=$matches[1]','top');
	//add_rewrite_rule('forums/forum/(.+?)(/[0-9]+)?/?$','index.php?','top');

}//recipe_rerr


function recipe_rerr222(){
	global $wp, $post;
	if($_GET['d'] == 1){
		echo '<pre>';
		print_r($post);
		print_r($wp);
		die();
	}
}

add_action('generate_rewrite_rules', 'recipe_rerr');
add_action( 'wp', 'recipe_rerr222' );


if($_GET['d'] == 1){
	/*
	
	$yy = get_option('cron');
	
	echo '<pre>';
	print_r($yy);
	
	die();
	* */
}

//define('WOOCOMMERCE_USE_CSS', false);
add_theme_support( 'woocommerce' );

add_filter( 'woocommerce_payment_complete_order_status', 'virtual_order_payment_complete_order_status', 10, 2);

function virtual_order_payment_complete_order_status( $order_status, $order_id ) {
  $order = new WC_Order( $order_id );
 
  if ( 'processing' == $order_status &&
       ( 'on-hold' == $order->status || 'pending' == $order->status || 'failed' == $order->status ) ) {
 
    $virtual_order = null;
 
    if ( count( $order->get_items() ) > 0 ) {
 
      foreach( $order->get_items() as $item ) {
 
        if ( 'line_item' == $item['type'] ) {
 
          $_product = $order->get_product_from_item( $item );
 
          if ( ! $_product->is_virtual() ) {
            // once we've found one non-virtual product we know we're done, break out of the loop
            $virtual_order = false;
            break;
          } else {
            $virtual_order = true;
          }
        }
      }
    }
 
    // virtual order, mark as completed
    if ( $virtual_order ) {
      return 'completed';
    }
  }
 
  // non-virtual order, return original status
  return $order_status;
}

add_shortcode( 'exercise-quiz', 'exercise_quiz_survey' );

function exercise_quiz_survey()
{
	//echo '<pre>'; print_r($_REQUEST); echo '<pre>';
	$token = session_id(); 
	$query_string = 'name=' . urlencode($_REQUEST['name']) . '&email=' . urlencode($_REQUEST['email']);
	return '<div >
<iframe id="ssiframe" src="http://sallysymonds.6bin.com/exercise-quiz-form/index.php?'. htmlentities($query_string) .'&'.$token.'" width="100%" scrolling="no"></iframe>
</div>
<p id="callback"> </p>';
}


