<?php
global $post;
global $wp;

if($wp->request == "forums"){
	header('Location:/members-area/forums/');
	die();
}

get_header();
			$class = op_default_attr('column_layout','option');
			$add_sidebar = true;
			if(defined('OP_SIDEBAR')){
				if(OP_SIDEBAR === FALSE){
					$class = 'no-sidebar';
					$add_sidebar = false;
				} else {
					$class = OP_SIDEBAR;
				}
			}
			?>
			<div class="op-page-header cf">
            	<h2 class="the-title"><?php single_post_title() ?></h2>
            </div>
            <span class="wpsr_floatbts_anchor" data-offset="50" ></span>
			<div class="main-content content-width cf <?php echo $class ?>">
		    	<div class="main-content-area-container cf">
                    <?php while ( have_posts() ) : the_post();
                    $img = '';
                    if(is_singular() && has_post_thumbnail($post->ID)){
                        $img = '<div class="post-image">'.get_the_post_thumbnail($post->ID,'post-thumbnail').'</div>';
                    }
                    ?>
                    <div id="post-<?php the_ID() ?>" <?php post_class('main-content-area'.($img==''?' no-post-image':'')) ?>>
                        <?php op_mod('advertising')->display(array('advertising', 'pages', 'top')) ?>
                        <div class="latest-post cf">
                            <?php echo $img ?>
                            <div class="single-post-content cf">
                                <?php the_content(); ?>
                                <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', OP_SN ) . '</span>', 'after' => '</div>' ) ); ?>
                            </div>
                        </div>
                        <?php op_mod('advertising')->display(array('advertising', 'pages', 'bottom')) ?>
                        <?php comments_template( '', true ); ?>
                    </div>
                    <?php endwhile ?>
                    <?php echo $add_sidebar ? '<div class="sidebar-bg"></div>' : '' ?>
                    <?php op_sidebar() ?>
                </div>
                <div class="clear"></div>
                
                    <?php 
                    /*$post->post_type == "forum" ||  $post->post_type  == "topic"  ||*/
					if($post->post_type  == "exercises" || $post->post_type  == "exercise-generator" || $post->post_type  == "recipe" || $post->post_type == "forum" ||  $post->post_type  == "topic" ){
					?>
                    
						<div class="fixed-width">
						<p style="font-size:9px;line-height: 140%;">Disclaimer: All information contained within this site is for informational purposes only. It is not intended to diagnose, treat, cure, or prevent any health problem – nor is it intended to replace the advice of a qualified medical practitioner, dietician, or mental health worker. No action should be taken solely on the contents of this website. Always consult your physician or qualified health professional on any matters regarding your health or on any opinions expressed within this website. The information provided within this website is believed to be accurate based on the best judgment of the authors but the reader is responsible for consulting with their own health professional on any matters raised within. Health information changes rapidly. Therefore, some information within this website may be out of date or even possibly inaccurate due to new studies and research that the authors of this website are unaware of. We do not assume any liability for the information contained within this website, be it direct, indirect, consequential, special, exemplary, or other damages. Please see your physician before changing your diet, starting an exercise program, or taking any supplements of any kind. If you have any questions about this disclaimer, please contact us.</p>
						</div>
					<?php
						}
					?>
                
            </div>
<?php get_footer() ?>
